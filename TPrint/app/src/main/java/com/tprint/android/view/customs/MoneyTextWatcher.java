package com.tprint.android.view.customs;

import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.widget.EditText;

import com.tprint.android.etc.DataUtils;

import java.lang.ref.WeakReference;


/**
 * Created by aboot on 10/4/16.
 */
public class MoneyTextWatcher implements TextWatcher {
    private final WeakReference<EditText> editTextWeakReference;

    public MoneyTextWatcher(EditText editText) {
        editText.setFilters(new InputFilter[] { new InputFilter.LengthFilter(16) });
        editTextWeakReference = new WeakReference<>(editText);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    @Override
    public void afterTextChanged(Editable editable) {
        if (editable.length() > 0) {
            EditText editText = editTextWeakReference.get();
            if (editText == null) return;
            String s = editable.toString();
            editText.removeTextChangedListener(this);
            String cleanString = s.replaceAll("[Rp$,. ]", "");
            String formatted = DataUtils.getNumber(Long.parseLong(DataUtils.isNotEmpty(cleanString) ? cleanString : "0"));
            editText.setTag(DataUtils.isNotEmpty(cleanString) ? Long.parseLong(cleanString) : 0);
            editText.setText(DataUtils.isNotEmpty(formatted) ? (formatted.equals("0") ? null : "Rp "+formatted) : null);
            editText.setSelection(editText.length());
            editText.addTextChangedListener(this);
        }
    }
}