package com.tprint.android.presenter.Storage;


import com.tprint.android.services.API;
import com.tprint.android.services.APIResponseData;

/**
 * Created by aboot on 7/13/17.
 */

public interface StorageViewImpl {

    void showLoading(API api);
    void hideLoading();
    void UploadSuccess();
    void ListSuccess(APIResponseData response);
    void PrintSuccess();
    void RemoveSuccess();
    void RequestFailed(API api, String errorMessage);
}
