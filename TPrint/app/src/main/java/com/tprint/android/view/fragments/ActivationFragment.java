package com.tprint.android.view.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.tprint.android.R;
import com.tprint.android.database.SessionApps;
import com.tprint.android.etc.DataUtils;
import com.tprint.android.etc.GilaPrint;
import com.tprint.android.models.ProfileModel;
import com.tprint.android.presenter.Credential.CredentialPageImpl;
import com.tprint.android.presenter.Credential.CredentialPresenter;
import com.tprint.android.presenter.Credential.CredentialViewImpl;
import com.tprint.android.services.API;
import com.tprint.android.services.APIResponseData;
import com.tprint.android.view.BaseFragment;

import java.util.concurrent.TimeUnit;
import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by aboot on 7/4/17.
 */

public class ActivationFragment extends BaseFragment implements CredentialViewImpl {

    private CredentialPageImpl credentialPage;
    private CredentialPresenter credentialPresenter;

    @BindView(R.id.textNumber) TextView textNumber;
    @BindView(R.id.inputCode1) EditText inputCode1;
    @BindView(R.id.inputCode2) EditText inputCode2;
    @BindView(R.id.inputCode3) EditText inputCode3;
    @BindView(R.id.inputCode4) EditText inputCode4;
    @BindView(R.id.btnResendCode) View btnResendCode;
    @BindView(R.id.textCount) TextView textCount;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (!(context instanceof CredentialPageImpl)) {
            throw new ClassCastException("Activity must implement CredentialPageImpl");
        }
        this.credentialPage = (CredentialPageImpl) context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activation_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getToolbar().setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        setToolbarTitle(GilaPrint.ActivationPage);

        credentialPresenter = new CredentialPresenter(this);

        ProfileModel profileModel = SessionApps.getProfile();
        if (profileModel != null) {
            textNumber.setText(profileModel.getPhone());
        }

        CodeEngine();
        ResendCodeCountDown();
    }

    @OnClick(R.id.btnChangeNumber) void ChangeNumber() {
        credentialPage.ChangePage(GilaPrint.ChangePhonePage);
    }


    @OnClick(R.id.btnResendCode) void onResendCode() {
        credentialPresenter.MeResendVerification();
        SessionApps.setActivationTime();
        ResendCodeCountDown();
    }

    private void CodeEngine() {
        inputCode1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    inputCode2.requestFocus();
                    checkVerificationCode();
                }
            }
        });

        inputCode2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    inputCode3.requestFocus();
                    checkVerificationCode();
                }
            }
        });

        inputCode3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    inputCode4.requestFocus();
                    checkVerificationCode();
                }
            }
        });

        inputCode4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    checkVerificationCode();
                }
            }
        });

        inputCode4.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_DEL) {
                    if (inputCode4.length() == 0) {
                        inputCode3.requestFocus();
                        inputCode3.setText(null);
                        return true;
                    }else {
                        return false;
                    }
                }else {
                    return false;
                }
            }
        });

        inputCode3.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_DEL) {
                    if (inputCode3.length() == 0) {
                        inputCode2.requestFocus();
                        inputCode2.setText(null);
                        return true;
                    }else {
                        return false;
                    }
                }else {
                    return false;
                }
            }
        });

        inputCode2.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_DEL) {
                    if (inputCode2.length() == 0) {
                        inputCode1.requestFocus();
                        inputCode1.setText(null);
                        return true;
                    }else {
                        return false;
                    }
                }else {
                    return false;
                }
            }
        });
    }

    private void ResendCodeCountDown() {
        long start = System.currentTimeMillis();
        long datetime = SessionApps.getActivationTime();
        long timeLeft = (datetime) - start;
        CountDownTimer countDownTimer = new CountDownTimer(timeLeft, 100) {
            @Override
            public void onTick(long millisUntilFinished) {
                long minute = TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) -
                        TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millisUntilFinished));
                @SuppressLint("DefaultLocale")
                String minutes = minute > 0 ? String.format("%02d", minute) : "00";
                long second = TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished));
                @SuppressLint("DefaultLocale")
                String seconds = second > 0 ? String.format("%02d", second) : "00";

                textCount.setText(minutes+":"+seconds);
                btnResendCode.setAlpha(0.5f);
                btnResendCode.setClickable(false);
            }


            @Override
            public void onFinish() {
                textCount.setText("00:00");
                btnResendCode.setAlpha(1f);
                btnResendCode.setClickable(true);
            }
        };
        countDownTimer.start();
    }

    private void checkVerificationCode() {
        if (DataUtils.isNotEmpty(inputCode1) && DataUtils.isNotEmpty(inputCode2) &&
                DataUtils.isNotEmpty(inputCode3) && DataUtils.isNotEmpty(inputCode4)) {
            String Code = inputCode1.getText().toString()+inputCode2.getText().toString()+
                    inputCode3.getText().toString()+inputCode4.getText().toString();
            credentialPresenter.MeVerification(Code);
        }
    }

    @Override
    public void showLoading(API api) {
        showLoadingDialog(api);
    }

    @Override
    public void hideLoading() {
        dismissLoadingDialog();
    }

    @Override
    public void LoginSuccess(APIResponseData response) {}

    @Override
    public void RegisterSuccess(APIResponseData response) {}

    @Override
    public void ProfileSuccess(String ChangePage) {
        credentialPage.ChangePage(ChangePage);
    }

    @Override
    public void VerificationSuccess() {
        credentialPresenter.MeProfile();
    }

    @Override
    public void ResendVerificationSuccess(String Message) {
        Toast.makeText(getActivity(), Message, Toast.LENGTH_SHORT).show();
        inputCode1.setText(null);
        inputCode2.setText(null);
        inputCode3.setText(null);
        inputCode4.setText(null);
    }

    @Override
    public void ForgotPasswordSuccess() {}

    @Override
    public void ResetPasswordCodeSuccess() {}

    @Override
    public void ResetPasswordSuccess() {}

    @Override
    public void ChangePhoneNumber() {}

    @Override
    public void RequestFailed(API api, String errorMessage) {
        Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT).show();
    }
}
