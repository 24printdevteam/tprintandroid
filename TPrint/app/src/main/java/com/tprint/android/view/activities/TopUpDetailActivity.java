package com.tprint.android.view.activities;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.text.Spanned;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.tprint.android.R;
import com.tprint.android.etc.DataUtils;
import com.tprint.android.etc.GilaPrint;
import com.tprint.android.models.BankModel;
import com.tprint.android.models.TopUpModel;
import com.tprint.android.presenter.Wallet.WalletPresenter;
import com.tprint.android.presenter.Wallet.WalletViewImpl;
import com.tprint.android.services.API;
import com.tprint.android.services.APIListener;
import com.tprint.android.services.APIRequest;
import com.tprint.android.services.APIResponseData;
import com.tprint.android.view.BaseActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by aboot on 5/24/17.
 */

public class TopUpDetailActivity extends BaseActivity implements WalletViewImpl {

    private WalletPresenter walletPresenter;

    @BindView(R.id.textAmount) TextView textAmount;
    @BindView(R.id.textExpiredDate) TextView textExpiredDate;
    @BindView(R.id.textMethod) TextView textMethod;
    @BindView(R.id.layBank) LinearLayout layBank;
    @BindView(R.id.layBankList) LinearLayout layBankList;
    @BindView(R.id.textDescription) TextView textDescription;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.topup_detail_activity);
        getToolbar().setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        setToolbarTitle(GilaPrint.TopUpDetailPage);

        walletPresenter = new WalletPresenter(this);

        TopUpModel topUpModel = getIntent().getParcelableExtra("topUpModel");
        if (topUpModel != null) {
            textAmount.setText(DataUtils.getCurrency((long) topUpModel.getAmountSystem()));
            textExpiredDate.setText(DataUtils.getDateServer(topUpModel.getExpired()));
            textMethod.setText(topUpModel.getType());
            textDescription.setText(getDescription((long) topUpModel.getAmountSystem()));
            if (topUpModel.getType().equals(GilaPrint.PaymentTransfer)) {
                getBankList();
            }
        }else {
            finish();
        }
    }

    @OnClick(R.id.btnCancel) void onCancel() {
        walletPresenter.WalletTopUpCancel(textMethod.getText().toString());
    }


    @Override
    public void showLoading(API api) {
        showLoadingDialog(api);
    }

    @Override
    public void hideLoading() {
        dismissLoadingDialog();
    }

    @Override
    public void ListSuccess(APIResponseData response) {}

    @Override
    public void TopUpListSuccess(APIResponseData response) {}

    @Override
    public void TopUpSuccess() {}

    @Override
    public void TopUpCancelSuccess() {
        finish();
    }

    @Override
    public void RequestFailed(API api, String errorMessage) {
        Toast.makeText(getApplicationContext(), errorMessage, Toast.LENGTH_SHORT).show();
    }

    private Spanned getDescription(long amount) {
        String desc1 = "Anda tidak perlu konfirmasi setelah transfer, sistem kami akan otomatis " +
                "mendeteksi nilai transfer anda. Pastikan jumlah yang ditransfer <b>sama persis</b> dengan " +
                "jumlah diatas, anda juga akan mendapatkan saldo yang sama yaitu "+
                "<b>"+DataUtils.getCurrency(amount)+"</b> setelah melakukan transfer.";
        String desc2 = "Biasanya, proses ini memakan waktu kurang dari 5 menit setelah " +
                "anda melakukan transfer, namun terkadang bisa sampai 24 jam " +
                "(tergantung dari email notifikasi yang dikirimkan ke bank kami).";
        String desc3 = "Jika anda tidak mentransfer jumlah yang persis diatas, " +
                "harap beritahukan admin outlet kami, proses ini bisa memakan waktu 7 hari.";

        String desc4 = "Klik batal transfer dibawah ini jika anda ingin memasukan jumlah yang lain.";

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(desc1+"<br/><br/>"+desc2+"<br/><br/>"+desc3+"<br/><br/>"+desc4+"<br/>",
                    Html.FROM_HTML_MODE_LEGACY);
        }else {
            return Html.fromHtml(desc1+"<br/><br/>"+desc2+"<br/><br/>"+desc3+"<br/><br/>"+desc4+"<br/>");
        }
    }

    private void getBankList() {
        new APIRequest().ServerBankList(new APIListener() {
            @Override
            public void onSuccess(APIResponseData response) {
                if (response.getBankList() != null) {
                    SetUpBankView(response.getBankList());
                }
            }

            @Override
            public void onFailure(API api, String errorMessage) {
                Toast.makeText(getApplicationContext(), errorMessage, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void SetUpBankView(List<BankModel> list) {
        layBank.setVisibility(View.VISIBLE);
        layBankList.removeAllViews();
        for (int i = 0; i < list.size(); i++) {
            @SuppressLint("InflateParams")
            View view = getLayoutInflater().inflate(R.layout.bank_item_viewholder, null);
            TextView textBank = (TextView) view.findViewById(R.id.textBank);
            TextView textBankAccount = (TextView) view.findViewById(R.id.textBankAccount);
            TextView textBankNumber = (TextView) view.findViewById(R.id.textBankNumber);

            BankModel model = list.get(i);

            textBank.setText(model.getBank()+", "+model.getBankBranch());
            textBankAccount.setText(model.getBankName());
            textBankNumber.setText(model.getBankNumber());

            layBankList.addView(view);
        }
    }
}
