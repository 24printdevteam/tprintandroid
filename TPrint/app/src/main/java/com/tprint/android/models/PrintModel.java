package com.tprint.android.models;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.List;

/**
 * Created by aboot on 7/5/17.
 */

public class PrintModel implements Parcelable {

    private String ID;
    private String Filename;
    private String Extension;
    private int Pages;
    private String Size;
    private String Orientation;
    private int OPageFrom;
    private int OPageTo;
    private int OCopies;
    private int OColor;
    private int ODoubleSided;
    private String Step;
    private long Timestamp;
    private long PrintReceipt;
    private long TotalPerFile;
    private List<PageDetails> PageDetails;
    private PriceDetails PriceDetails;

    protected PrintModel(Parcel in) {
        ID = in.readString();
        Filename = in.readString();
        Extension = in.readString();
        Pages = in.readInt();
        Size = in.readString();
        Orientation = in.readString();
        OPageFrom = in.readInt();
        OPageTo = in.readInt();
        OCopies = in.readInt();
        OColor = in.readInt();
        ODoubleSided = in.readInt();
        Step = in.readString();
        Timestamp = in.readLong();
        PrintReceipt = in.readLong();
        TotalPerFile = in.readLong();
        PageDetails = in.createTypedArrayList(com.tprint.android.models.PageDetails.CREATOR);
        PriceDetails = in.readParcelable(com.tprint.android.models.PriceDetails.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(ID);
        dest.writeString(Filename);
        dest.writeString(Extension);
        dest.writeInt(Pages);
        dest.writeString(Size);
        dest.writeString(Orientation);
        dest.writeInt(OPageFrom);
        dest.writeInt(OPageTo);
        dest.writeInt(OCopies);
        dest.writeInt(OColor);
        dest.writeInt(ODoubleSided);
        dest.writeString(Step);
        dest.writeLong(Timestamp);
        dest.writeLong(PrintReceipt);
        dest.writeLong(TotalPerFile);
        dest.writeTypedList(PageDetails);
        dest.writeParcelable(PriceDetails, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<PrintModel> CREATOR = new Creator<PrintModel>() {
        @Override
        public PrintModel createFromParcel(Parcel in) {
            return new PrintModel(in);
        }

        @Override
        public PrintModel[] newArray(int size) {
            return new PrintModel[size];
        }
    };

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getFilename() {
        return Filename;
    }

    public void setFilename(String filename) {
        Filename = filename;
    }

    public String getExtension() {
        return Extension;
    }

    public void setExtension(String extension) {
        Extension = extension;
    }

    public int getPages() {
        return Pages;
    }

    public void setPages(int pages) {
        Pages = pages;
    }

    public String getSize() {
        return Size;
    }

    public void setSize(String size) {
        Size = size;
    }

    public String getOrientation() {
        return Orientation;
    }

    public void setOrientation(String orientation) {
        Orientation = orientation;
    }

    public int getOPageFrom() {
        return OPageFrom;
    }

    public void setOPageFrom(int OPageFrom) {
        this.OPageFrom = OPageFrom;
    }

    public int getOPageTo() {
        return OPageTo;
    }

    public void setOPageTo(int OPageTo) {
        this.OPageTo = OPageTo;
    }

    public int getOCopies() {
        return OCopies;
    }

    public void setOCopies(int OCopies) {
        this.OCopies = OCopies;
    }

    public int getOColor() {
        return OColor;
    }

    public void setOColor(int OColor) {
        this.OColor = OColor;
    }

    public int getODoubleSided() {
        return ODoubleSided;
    }

    public void setODoubleSided(int ODoubleSided) {
        this.ODoubleSided = ODoubleSided;
    }

    public String getStep() {
        return Step;
    }

    public void setStep(String step) {
        Step = step;
    }

    public long getTimestamp() {
        return Timestamp;
    }

    public void setTimestamp(long timestamp) {
        Timestamp = timestamp;
    }

    public long getPrintReceipt() {
        return PrintReceipt;
    }

    public void setPrintReceipt(long printReceipt) {
        PrintReceipt = printReceipt;
    }

    public long getTotalPerFile() {
        return TotalPerFile;
    }

    public void setTotalPerFile(long totalPerFile) {
        TotalPerFile = totalPerFile;
    }

    public List<com.tprint.android.models.PageDetails> getPageDetails() {
        return PageDetails;
    }

    public void setPageDetails(List<com.tprint.android.models.PageDetails> pageDetails) {
        PageDetails = pageDetails;
    }

    public com.tprint.android.models.PriceDetails getPriceDetails() {
        return PriceDetails;
    }

    public void setPriceDetails(com.tprint.android.models.PriceDetails priceDetails) {
        PriceDetails = priceDetails;
    }
}
