package com.tprint.android.etc;

/**
 * Created by aboot on 5/29/17.
 */

public class GilaPrint {

    public static final String Type = "Android";

    // User Status
    public static final String Unverified = "Unverified";
    public static final String Reset = "Reset";

    //Credential
    public static final String CredentialPage = "CredentialPage";
    public static final String LoginPage = "Login";
    public static final String RegisterPage = "Daftar";
    public static final String ActivationPage = "Verifikasi";
    public static final String ChangePhonePage = "Ganti Nomor?";
    public static final String ForgotPasswordPage = "Lupa Password?";
    public static final String ForgotPasswordCodePage = "Verifikasi Password";
    public static final String SetPasswordPage = "Set Password";
    public static final String MainMenuPage = "MainMenu";
    public static final String TopUpPage = "Top Up";
    public static final String TopUpDetailPage = "Top Up Detail";

    //Menu Page
    public static final String HomePage = "home";
    public static final String WalletPage = "Wallet";
    public static final String HistoryPage = "History";
    public static final String ProfilePage = "Profile";

    public static final String PrintPage = "PrintPage";
    public static final String PrintList = "Pengaturan Print";
    public static final String PrintUpload = "Upload File";
    public static final String PrintPayment = "Pembayaran";
    public static final String PrintTransactionDetail = "Transaksi Detail";

    public static final String PaymentBalance = "Balance";
    public static final String PaymentTransfer = "Bank Transfer";
    public static final String PaymentOnSite = "On Site";

    public static final String HistoryPending = "Pending";
    public static final String HistoryWaiting = "Waiting";
    public static final String HistoryExpired = "Expired";
    public static final String HistoryCompleted = "Completed";


    public static final String CoverageQuarter = "Quarter";
    public static final String CoverageHalf = "Half";
    public static final String CoverageFull = "Full";

    public static final String PaperA4 = "A4";
    public static final String PaperA3 = "A3";

    public static final String PrintModel = "PrintModel";



    public static final int REQUEST_PAYMENT = 11;
    public static final int REQUEST_UPLOADFILE = 12;
    public static final int REQUEST_DROPBOX = 13;
    public static final int REQUEST_GOOGLEDRIVE = 14;
    public static final int REQUEST_STORAGE = 15;
    public static final int REQUEST_SCAN = 16;
    public static final int REQUEST_CHANGE_NUMBER = 17;
    public static final int REQUEST_CREDENTIAL = 18;
    public static final int REQUEST_NOTIFICATION = 19;
    public static final int REQUEST_PERMISSION = 20;
    public static final int REQUEST_APPLICATION_SETTINGS = 21;

    public static final String UPLOAD_FILE = "UploadFile";
    public static final String STORAGE = "Storage";
    public static final String GOOGLEDRIVE = "GoogleDrive";
    public static final String DROPBOX = "Dropbox";
    public static final String SCANDOCUMENT = "ScanDocument";

    public static final String mime_pdf = "application/pdf";
    public static final String mime_msword = "application/msword";
    public static final String mime_msexcel = "application/vnd.ms-excel";
    public static final String mime_mspowerpoint = "application/vnd.ms-powerpoint";
    public static final String mime_google_document = "application/vnd.google-apps.document";
    public static final String mime_google_spreadsheet = "application/vnd.google-apps.spreadsheet";
    public static final String mime_google_presentation = "application/vnd.google-apps.presentation";

    public static final String[] mimeTypes = {
            mime_pdf,
            mime_msword,
            mime_msexcel,
            mime_mspowerpoint,
            mime_google_document,
            mime_google_spreadsheet,
            mime_google_presentation
    };


}
