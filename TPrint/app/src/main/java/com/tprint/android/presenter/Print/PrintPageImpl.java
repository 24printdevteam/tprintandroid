package com.tprint.android.presenter.Print;


import com.tprint.android.services.APIResponseData;

/**
 * Created by aboot on 7/7/17.
 */

public interface PrintPageImpl {

    void ChangePage(String Page, APIResponseData response);
    void ChangePage(long GrandTotal);
}
