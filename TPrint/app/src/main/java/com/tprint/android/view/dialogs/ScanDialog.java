package com.tprint.android.view.dialogs;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.tprint.android.R;
import com.tprint.android.view.BaseDialog;
import butterknife.OnClick;

/**
 * Created by aboot on 7/26/17.
 */

public class ScanDialog extends BaseDialog {

    private OnClickCameraListener onClickCameraListener;

    public void setOnClickCameraListener(OnClickCameraListener onClickCameraListener) {
        this.onClickCameraListener = onClickCameraListener;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.scan_dialog, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @OnClick(R.id.btnCamera) void onCamera() {
        if (onClickCameraListener != null) onClickCameraListener.onCamera(true);
        dismissDialog();
    }

    @OnClick(R.id.btnGallery) void onGallery() {
        if (onClickCameraListener != null) onClickCameraListener.onCamera(false);
        dismissDialog();
    }

    public interface OnClickCameraListener {
        void onCamera(boolean isCamera);
    }
}
