package com.tprint.android.services;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;
import com.tprint.android.etc.GilaPrint;
import com.tprint.android.etc.Tools;
import com.tprint.android.models.BankModel;
import com.tprint.android.models.CheckOutModel;
import com.tprint.android.models.HistoryModel;
import com.tprint.android.models.LocationModel;
import com.tprint.android.models.PriceListModel;
import com.tprint.android.models.PrintModel;
import com.tprint.android.models.ProfileModel;
import com.tprint.android.models.TopUpModel;
import com.tprint.android.models.WalletModel;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by aboot on 6/28/17.
 */
class APIDeserializer implements JsonDeserializer<APIResponseData> {

    private API api;

    APIDeserializer(API api) {
        this.api = api;
    }

    @Override
    public APIResponseData deserialize(JsonElement json, Type typeOfT,
                                       JsonDeserializationContext context) throws JsonParseException {
        APIResponseData apiResponseData = new APIResponseData();
        JsonObject jsonObject = (JsonObject) json;
        if (jsonObject.has("Code")) {
            apiResponseData.setCode(jsonObject.get("Code").getAsInt());
        }
        if (jsonObject.has("Status")) {
            apiResponseData.setStatus(jsonObject.get("Status").getAsString());
        }
        if (jsonObject.has("Message")) {
            apiResponseData.setMessage(jsonObject.get("Message").getAsString());
        }
        if (jsonObject.has("Data") && jsonObject.get("Data").isJsonObject()) {
            JsonObject data = jsonObject.getAsJsonObject("Data");
            switch (api) {
                case Me_Register:
                case Me_Login:
                case Me_ResetPasswordCode:
                    if (data.has("Token")) {
                        apiResponseData.setToken(data.get("Token").getAsString());
                    }
                    if (data.has("Status")) {
                        apiResponseData.setLoginStatus(data.get("Status").getAsString());
                    }
                    break;
                case Me_Profile:
                    ProfileModel profileModel = new Gson().
                            fromJson(data, ProfileModel.class);
                    apiResponseData.setProfileModel(profileModel);
                    break;
                case Print_List:
                    if (data.has("Files") && data.get("Files").isJsonArray()) {
                        JsonArray Files = data.getAsJsonArray("Files");
                        Type type = new TypeToken<List<PrintModel>>() {}.getType();
                        List<PrintModel> printList = new Gson().fromJson(Files, type);
                        apiResponseData.setPrintList(printList);
                    }
                    break;
                case Print_CheckOut:
                    CheckOutModel checkOutModel = new Gson().
                            fromJson(data, CheckOutModel.class);
                    apiResponseData.setCheckOutModel(checkOutModel);
                    break;
                case History_List:
                    if (data.has("Pending") && data.get("Pending").isJsonArray() ||
                            data.has("Waiting") && data.get("Waiting").isJsonArray()) {
                        List<HistoryModel> inProgressList = new ArrayList<>();
                        if (data.has("Waiting")) {
                            JsonArray Waiting = data.getAsJsonArray("Waiting");
                            if (Waiting.size() > 0) {
                                Type type = new TypeToken<List<HistoryModel>>() {}.getType();
                                List<HistoryModel> waitingList = new Gson().fromJson(Waiting, type);
                                inProgressList.addAll(getList(GilaPrint.HistoryWaiting, waitingList));
                            }
                        }
                        if (data.has("Pending")) {
                            JsonArray Pending = data.getAsJsonArray("Pending");
                            if (Pending.size() > 0) {
                                Type type = new TypeToken<List<HistoryModel>>() {}.getType();
                                List<HistoryModel> pendingList = new Gson().fromJson(Pending, type);
                                inProgressList.addAll(getList(GilaPrint.HistoryPending, pendingList));
                            }
                        }
                        apiResponseData.setInProgressList(inProgressList);
                    }

                    if (data.has("Expired") && data.get("Expired").isJsonArray() ||
                            data.has("Completed") && data.get("Completed").isJsonArray()) {
                        List<HistoryModel> completedList = new ArrayList<>();
                        if (data.has("Completed")) {
                            JsonArray Completed = data.getAsJsonArray("Completed");
                            if (Completed.size() > 0) {
                                Type type = new TypeToken<List<HistoryModel>>() {}.getType();
                                List<HistoryModel> completeList = new Gson().fromJson(Completed, type);
                                completedList.addAll(getList(GilaPrint.HistoryCompleted, completeList));
                            }
                        }
                        if (data.has("Expired")) {
                            JsonArray Expired = data.getAsJsonArray("Expired");
                            if (Expired.size() > 0) {
                                Type type = new TypeToken<List<HistoryModel>>() {}.getType();
                                List<HistoryModel> expiredList = new Gson().fromJson(Expired, type);
                                completedList.addAll(getList(GilaPrint.HistoryExpired, expiredList));
                            }
                        }
                        apiResponseData.setCompletedList(completedList);
                    }
                    break;
                case History_Details:
                    HistoryModel historyModel = new Gson().
                            fromJson(data, HistoryModel.class);
                    apiResponseData.setHistoryModel(historyModel);
                    break;
                case Server_PriceList:
                    apiResponseData.setPriceListModel(getPriceList(data));
                    break;
                case Print_Preview:
                    if (data.has("ID")) {
                        apiResponseData.setPreviewUrl(Tools.getPreviewUrl()+""+data.get("ID").getAsString());
                    }
                    break;
            }
        }else if (jsonObject.has("Data") && jsonObject.get("Data").isJsonArray()) {
            JsonArray data = jsonObject.getAsJsonArray("Data");
            switch (api) {
                case Storage_List:
                    Type type = new TypeToken<List<PrintModel>>() {}.getType();
                    List<PrintModel> printList = new Gson().fromJson(data, type);
                    apiResponseData.setPrintList(printList);
                    break;
                case Server_LocationList:
                    Type locType = new TypeToken<List<LocationModel>>() {}.getType();
                    List<LocationModel> locationList = new Gson().fromJson(data, locType);
                    apiResponseData.setLocationList(locationList);
                    break;
                case Wallet_List:
                    Type walletType = new TypeToken<List<WalletModel>>() {}.getType();
                    List<WalletModel> walletList = new Gson().fromJson(data, walletType);
                    apiResponseData.setWalletList(walletList);
                    break;
                case Wallet_TopUpList:
                    Type topUpType = new TypeToken<List<TopUpModel>>() {}.getType();
                    List<TopUpModel> topUpList = new Gson().fromJson(data, topUpType);
                    apiResponseData.setTopUpList(topUpList);
                    break;
                case Server_BankList:
                    Type bankType = new TypeToken<List<BankModel>>() {}.getType();
                    List<BankModel> bankList = new Gson().fromJson(data, bankType);
                    apiResponseData.setBankList(bankList);
                    break;
            }
        }
        return apiResponseData;
    }

    private List<HistoryModel> getList(String Status, List<HistoryModel> list) {
        List<HistoryModel> statusList = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            HistoryModel model = list.get(i);
            model.setStatus(Status);
            statusList.add(model);
        }
        return statusList;
    }

    private PriceListModel getPriceList(JsonObject data) {
        PriceListModel model = new PriceListModel();
        if (data.has("PRICE_A4_QUARTER_MONO")) {
            model.setA4MonoLow(data.get("PRICE_A4_QUARTER_MONO").getAsLong());
        }
        if (data.has("PRICE_A4_HALF_MONO")) {
            model.setA4MonoMedium(data.get("PRICE_A4_HALF_MONO").getAsLong());
        }
        if (data.has("PRICE_A4_FULL_MONO")) {
            model.setA4MonoHigh(data.get("PRICE_A4_FULL_MONO").getAsLong());
        }

        if (data.has("PRICE_A4_PHOTO")) {
            model.setA4Photo(data.get("PRICE_A4_PHOTO").getAsLong());
        }

        if (data.has("PRICE_A4_QUARTER_COLOR")) {
            model.setA4ColorLow(data.get("PRICE_A4_QUARTER_COLOR").getAsLong());
        }
        if (data.has("PRICE_A4_HALF_COLOR")) {
            model.setA4ColorMedium(data.get("PRICE_A4_HALF_COLOR").getAsLong());
        }
        if (data.has("PRICE_A4_FULL_COLOR")) {
            model.setA4ColorHigh(data.get("PRICE_A4_FULL_COLOR").getAsLong());
        }

        if (data.has("PRICE_A3_QUARTER_MONO")) {
            model.setA3MonoLow(data.get("PRICE_A3_QUARTER_MONO").getAsLong());
        }
        if (data.has("PRICE_A3_HALF_MONO")) {
            model.setA3MonoMedium(data.get("PRICE_A3_HALF_MONO").getAsLong());
        }
        if (data.has("PRICE_A3_FULL_MONO")) {
            model.setA3MonoHigh(data.get("PRICE_A3_FULL_MONO").getAsLong());
        }

        if (data.has("PRICE_A3_QUARTER_COLOR")) {
            model.setA3ColorLow(data.get("PRICE_A3_QUARTER_COLOR").getAsLong());
        }
        if (data.has("PRICE_A3_HALF_COLOR")) {
            model.setA3ColorMedium(data.get("PRICE_A3_HALF_COLOR").getAsLong());
        }
        if (data.has("PRICE_A3_FULL_COLOR")) {
            model.setA3ColorHigh(data.get("PRICE_A3_FULL_COLOR").getAsLong());
        }
        if (data.has("PRICE_PACKAGING")) {
            model.setPricePackaging(data.get("PRICE_PACKAGING").getAsLong());
        }

        return model;
    }

    private HistoryModel parseHistory(JsonObject object) {
        HistoryModel model = new HistoryModel();
        model.setID(object.get("ID").getAsString());
        model.setOutletID(object.get("OutletID").getAsString());
        model.setPrinterID(object.get("PrinterID").getAsString());
        model.setKioskID(object.get("KioskID").getAsString());
        model.setInquiryID(object.get("InquiryID").getAsString());
        model.setUsername(object.get("Username").getAsString());
        model.setTotalPages(object.get("TotalPages").getAsInt());
        model.setFiles(object.get("Files").getAsString());
        model.setPages(object.get("Pages").getAsString());
        model.setPageFroms(object.get("PageFroms").getAsString());
        model.setPageTos(object.get("PageTos").getAsString());
        model.setDuplex(object.get("Duplex").getAsString());
        model.setQuarter(object.get("Quarter").getAsString());
        model.setHalf(object.get("Half").getAsString());
        model.setFull(object.get("Full").getAsString());
        model.setType(object.get("Type").getAsString());
        model.setCopies(object.get("Copies").getAsString());
        model.setPrices(object.get("Prices").getAsString());
        model.setPrintReceipt(object.get("PrintReceipt").getAsLong());
        model.setSubTotal(object.get("SubTotal").getAsLong());
        model.setReceipt(object.get("Receipt").getAsLong());
        model.setDelivery(object.get("Delivery").getAsLong());
        model.setDiscount(object.get("Discount").getAsLong());
        model.setTotal(object.get("Total").getAsLong());
        model.setTotalSystem(object.get("TotalSystem").getAsLong());
        model.setPaymentType(object.get("PaymentType").getAsString());
        model.setPaymentMethod(object.get("PaymentMethod").getAsString());
        model.setPaymentStatus(object.get("PaymentStatus").getAsString());
        model.setStatus(object.get("Status").getAsString());
        model.setExpired(object.get("Expired").getAsLong());
        model.setTCreated(object.get("TCreated").getAsLong());
        model.setTSend(object.get("TSent").getAsLong());
        model.setTReceived(object.get("TReceived").getAsLong());
        model.setTProcessed(object.get("TProcessed").getAsLong());
        model.setTProcessed(object.get("TProcessed").getAsLong());
        model.setTCompleted(object.get("TCompleted").getAsLong());
        model.setLocation(object.get("Location").getAsString());
        return model;
    }
}
