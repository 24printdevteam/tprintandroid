package com.tprint.android.presenter.Credential;

/**
 * Created by aboot on 7/4/17.
 */

public interface CredentialPresenterImpl {

    void MeLogin(String Username, String Password);
    void MeRegister(String Phone, String Email, String Name, String Password);
    void MeProfile();
    void MeVerification(String Code);
    void MeResendVerification();
    void MeForgotPassword(boolean isEmail, String EmailPhone);
    void MeResetPasswordCode(boolean isEmail, String EmailPhone, String Code);
    void MeResetPassword(String Password);
    void MeChangePhoneNumber(String Phone);
}
