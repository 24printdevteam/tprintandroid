package com.tprint.android.view.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;

import com.tprint.android.R;
import com.tprint.android.etc.GilaPrint;
import com.tprint.android.presenter.SlideAdapter;
import com.tprint.android.view.BaseActivity;
import com.viewpagerindicator.CirclePageIndicator;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by aboot on 5/5/17.
 */

public class SlideActivity extends BaseActivity {

    @BindView(R.id.viewPager) ViewPager viewPager;
    @BindView(R.id.circePageIndicator) CirclePageIndicator circlePageIndicator;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.slide_activity);
        ButterKnife.bind(this);
        viewPager.setAdapter(new SlideAdapter(getApplicationContext()));
        circlePageIndicator.setViewPager(viewPager);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GilaPrint.REQUEST_CREDENTIAL && resultCode == RESULT_OK) {
            finish();
        }
    }

    @OnClick(R.id.btnLogin) void onLogin() {
        Intent intent = new Intent(getApplicationContext(), CredentialActivity.class);
        intent.putExtra(GilaPrint.CredentialPage, GilaPrint.LoginPage);
        startActivityForResult(intent, GilaPrint.REQUEST_CREDENTIAL);
    }

    @OnClick(R.id.btnRegister) void onRegister() {
        Intent intent = new Intent(getApplicationContext(), CredentialActivity.class);
        intent.putExtra(GilaPrint.CredentialPage, GilaPrint.RegisterPage);
        startActivityForResult(intent, GilaPrint.REQUEST_CREDENTIAL);
    }


}
