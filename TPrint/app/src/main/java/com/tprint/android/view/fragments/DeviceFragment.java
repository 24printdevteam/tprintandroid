package com.tprint.android.view.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.tprint.android.R;
import com.tprint.android.presenter.DeviceAdapter;
import com.tprint.android.view.BaseFragment;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import butterknife.BindView;

/**
 * Created by aboot on 6/8/17.
 */

public class DeviceFragment extends BaseFragment implements
        SwipeRefreshLayout.OnRefreshListener,
        DeviceAdapter.OnDeviceSelectedListener {

    DeviceAdapter deviceAdapter;
    OnFolderSelectedListener onFolderSelectedListener;

    @BindView(R.id.refreshLayout) SwipeRefreshLayout refreshLayout;
    @BindView(R.id.recyclerView) RecyclerView recyclerView;

    public static DeviceFragment newInstance(String directory) {
        Bundle args = new Bundle();
        args.putString("directoryName", directory);
        DeviceFragment fragment = new DeviceFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            onFolderSelectedListener = (OnFolderSelectedListener) context;
        } catch (ClassCastException e) {
            e.printStackTrace();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.device_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        deviceAdapter = new DeviceAdapter();
        deviceAdapter.setOnDeviceSelectedListener(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(deviceAdapter);
        refreshLayout.setOnRefreshListener(this);

        if (getListFiles().size() > 0) {
            deviceAdapter.addNewList(getListFiles());
            hideAllState();
        }else {
            showEmpty("No Items");
        }
    }

    @Override
    public void onRefresh() {
        refreshLayout.setRefreshing(false);
        if (getListFiles().size() > 0) {
            deviceAdapter.addNewList(getListFiles());
            hideAllState();
        }else {
            showEmpty("No Items");
        }
    }

    @Override
    public void onFolder(File path) {
        onFolderSelectedListener.onFolder(path);
    }

    @Override
    public void onFile(File file) {
        if (onFolderSelectedListener != null) onFolderSelectedListener.onFile(file);
    }

    private String getDirectory() {
        return getArguments().getString("directoryName");
    }

    private List<File> getListFiles() {
        File parentDir = new File(getDirectory());
        List<File> list = new ArrayList<>();

        File[] folders = parentDir.listFiles(new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                return pathname.isDirectory();
            }
        });

        if (folders != null) {
            Arrays.sort(folders, new Comparator<File>() {
                public int compare(File f1, File f2) {
                    return f1.getName().compareTo(f2.getName());
                }
            });
            list.addAll(Arrays.asList(folders));
        }



        File[] files = parentDir.listFiles(new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                return !pathname.isDirectory();
            }
        });

        if (files != null) {
            Arrays.sort(files, new Comparator<File>() {
                public int compare(File f1, File f2) {
                    return f1.getName().compareTo(f2.getName());
                }
            });

            list.addAll(Arrays.asList(files));
        }

        return list;
    }

    public interface OnFolderSelectedListener {
        void onFolder(File directory);
        void onFile(File file);
    }
}
