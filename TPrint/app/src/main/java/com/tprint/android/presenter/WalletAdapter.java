package com.tprint.android.presenter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.tprint.android.R;
import com.tprint.android.etc.DataUtils;
import com.tprint.android.models.WalletModel;
import com.tprint.android.view.BaseViewHolder;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by aboot on 5/22/17.
 */

public class WalletAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<WalletModel> list = new ArrayList<>();

    public void addNewList(List<WalletModel> list) {
        this.list.clear();
        if (list != null) {
            this.list.addAll(list);
        }
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.wallet_item_viewholder, parent, false);
        return new ItemHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ItemHolder) {
            ItemHolder itemHolder = (ItemHolder) holder;
            WalletModel model = list.get(position);
            itemHolder.textID.setText("ID: "+model.getID() +"");
            itemHolder.textDescription.setText(model.getDescription());
            itemHolder.textDates.setText(DataUtils.getDateServer(model.getTimestamp()));
            itemHolder.textValue.setText(DataUtils.getCurrency(model.getValue()));
            itemHolder.textBalance.setText(DataUtils.getCurrency(model.getBalance()));
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ItemHolder extends BaseViewHolder {

        @BindView(R.id.textID) TextView textID;
        @BindView(R.id.textDescription) TextView textDescription;
        @BindView(R.id.textDates) TextView textDates;
        @BindView(R.id.textValue) TextView textValue;
        @BindView(R.id.textBalance) TextView textBalance;

        private ItemHolder(View itemView) {
            super(itemView);
        }
    }
}
