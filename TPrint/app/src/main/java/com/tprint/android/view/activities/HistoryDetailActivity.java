package com.tprint.android.view.activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.tprint.android.R;
import com.tprint.android.etc.DataUtils;
import com.tprint.android.etc.GilaPrint;
import com.tprint.android.etc.Tools;
import com.tprint.android.models.BankModel;
import com.tprint.android.models.HistoryModel;
import com.tprint.android.models.LocationModel;
import com.tprint.android.presenter.History.HistoryPresenter;
import com.tprint.android.presenter.History.HistoryViewImpl;
import com.tprint.android.services.API;
import com.tprint.android.services.APIListener;
import com.tprint.android.services.APIRequest;
import com.tprint.android.services.APIResponseData;
import com.tprint.android.view.BaseActivity;
import java.util.List;
import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by aboot on 7/15/17.
 */

public class HistoryDetailActivity extends BaseActivity implements HistoryViewImpl {

    @BindView(R.id.layContent) LinearLayout layContent;
    @BindView(R.id.imageMethod) ImageView imageMethod;
    @BindView(R.id.textMethodTitle) TextView textMethodTitle;
    @BindView(R.id.textMethodDesc) TextView textMethodDesc;
    @BindView(R.id.textCode) TextView textCode;

    @BindView(R.id.textStatus) TextView textStatus;
    @BindView(R.id.textDateHeader) TextView textDateHeader;
    @BindView(R.id.textDate) TextView textDate;
    @BindView(R.id.layPickUp) LinearLayout layPickUp;
    @BindView(R.id.textPickUp) TextView textPickUp;
    @BindView(R.id.textSubtotal) TextView textSubtotal;
    @BindView(R.id.textReceipt) TextView textReceipt;
    @BindView(R.id.textTotal) TextView textTotal;

    @BindView(R.id.layBank) LinearLayout layBank;
    @BindView(R.id.layBankList) LinearLayout layBankList;
    @BindView(R.id.layFileList) LinearLayout layFileList;
    @BindView(R.id.locationText) TextView locationText;
    @BindView(R.id.layLocationList) LinearLayout layLocationList;
    @BindView(R.id.btnLocationList) View btnLocationList;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.order_detail_activity);
        getToolbar().setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        setToolbarTitle(GilaPrint.PrintTransactionDetail);

        String ID = getIntent().getStringExtra("ID");

        HistoryPresenter historyPresenter = new HistoryPresenter(this);
        historyPresenter.HistoryDetails(ID);
    }

    @Override
    public void showLoading(API api) {
        showLoadingDialog(api);
    }

    @Override
    public void hideLoading() {
        dismissLoadingDialog();
    }

    @Override
    public void ListSuccess(APIResponseData response) {}

    @Override
    public void DetailsSuccess(APIResponseData response) {
        if (response.getHistoryModel() != null) {
            layContent.setVisibility(View.VISIBLE);
            setUpView(response.getHistoryModel());
        }else {
            finish();
        }
    }

    @Override
    public void RequestFailed(API api, String errorMessage) {
        Toast.makeText(getApplicationContext(), errorMessage, Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.btnCopy) void onCopy() {
        int sdk = android.os.Build.VERSION.SDK_INT;
        if (sdk < android.os.Build.VERSION_CODES.HONEYCOMB) {
            android.text.ClipboardManager clipboard = (android.text.ClipboardManager)
                    getSystemService(Context.CLIPBOARD_SERVICE);
            clipboard.setText(textCode.getText().toString());
        } else {
            android.content.ClipboardManager clipboard = (android.content.ClipboardManager)
                    getSystemService(Context.CLIPBOARD_SERVICE);
            android.content.ClipData clip = android.content.ClipData
                    .newPlainText("Kode Print", textCode.getText().toString());
            clipboard.setPrimaryClip(clip);
        }
        Toast.makeText(getApplicationContext(),
                "Kode Print berhasil disalin", Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.btnLocationList) void onLocationList() {
        Intent intent = new Intent(getApplicationContext(), LocationActivity.class);
        startActivity(intent);
    }

    private void setUpView(HistoryModel model) {
        switch (model.getStatus()) {
            case GilaPrint.HistoryPending:
                imageMethod.setImageResource(R.drawable.ico_order_vending);
                textMethodTitle.setText(R.string.order_berhasil);
                textMethodDesc.setText(R.string.gunakan_kode_print);
                textDateHeader.setText(R.string.batas_pembayaran);
                textStatus.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary));
                textDate.setText(DataUtils.getDateServer(model.getExpired()));
                break;
            case GilaPrint.HistoryWaiting:
                imageMethod.setImageResource(R.drawable.ico_order_now);
                textMethodTitle.setText(R.string.pembayaran_berhasil);
                textMethodDesc.setText(R.string.terima_kasih_telah_melakukan_pembayaran);
                textDateHeader.setText(R.string.batas_pembayaran);
                textStatus.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.waiting));
                textDate.setText(DataUtils.getDateServer(model.getExpired()));
                getLocation();
                break;
            case GilaPrint.HistoryCompleted:
                textStatus.setText(model.getStatus());
                imageMethod.setImageResource(R.drawable.ico_order_now);
                textMethodTitle.setText("Pengambilan Berahsil");
                textMethodDesc.setText("Terima kasih, Dokumen telah diambil");
                textDateHeader.setText(R.string.tanggal_pengambilan);
                textStatus.setTextColor(Color.BLACK);
                textDate.setText(DataUtils.getDateServer(model.getTCompleted()));
                if (DataUtils.isNotEmpty(model.getLocation())) {
                    layPickUp.setVisibility(View.VISIBLE);
                    textPickUp.setText(model.getLocation());
                }
                break;
            case GilaPrint.HistoryExpired:
                imageMethod.setImageResource(R.drawable.ico_order_now);
                textMethodTitle.setText("Transaksi Gagal");
                textMethodDesc.setText("Dikarenakan Anda tidak melakukan pembayaran atau tidak mengambil dokumen.");
                textStatus.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary));
                break;
        }

        textCode.setText(model.getID());
        textStatus.setText(model.getStatus());
        textSubtotal.setText(DataUtils.getCurrency(model.getSubTotal()));
        textReceipt.setText(DataUtils.getCurrency(model.getReceipt()));
        textTotal.setText(DataUtils.getCurrency(model.getTotal()));

        setUpFileList(model);
        if (model.getStatus().equals(GilaPrint.HistoryPending) && model.getPaymentMethod().equals(GilaPrint.PaymentTransfer)) {
            getBankList();
        }
    }

    private void getBankList() {
        new APIRequest().ServerBankList(new APIListener() {
            @Override
            public void onSuccess(APIResponseData response) {
                if (response.getBankList() != null) {
                    setUpBankList(response.getBankList());
                }
            }

            @Override
            public void onFailure(API api, String errorMessage) {
                Toast.makeText(getApplicationContext(), errorMessage, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setUpBankList(List<BankModel> list) {
        layBank.setVisibility(View.VISIBLE);
        layBankList.removeAllViews();
        for (int i = 0; i < list.size(); i++) {
            @SuppressLint("InflateParams")
            View view = getLayoutInflater().inflate(R.layout.bank_item_viewholder, null);
            TextView textBank = (TextView) view.findViewById(R.id.textBank);
            TextView textBankAccount = (TextView) view.findViewById(R.id.textBankAccount);
            TextView textBankNumber = (TextView) view.findViewById(R.id.textBankNumber);

            BankModel model = list.get(i);

            textBank.setText(model.getBank()+", "+model.getBankBranch());
            textBankAccount.setText(model.getBankName());
            textBankNumber.setText(model.getBankNumber());

            layBankList.addView(view);
        }
    }

    private void setUpFileList(HistoryModel model) {
        layFileList.removeAllViews();
        
        String[] fileList = model.getFiles().split("/");
        String[] pageFromList = model.getPageFroms().split("\\|");
        String[] pageToList = model.getPageTos().split("\\|");
        String[] priceList = model.getPrices().split("\\|");
        String[] pagesList = model.getPages().split("\\|");
        String[] typeList = model.getType().split("\\|");
        int fileSize = fileList.length;
        if (pageFromList.length == fileSize && pageToList.length == fileSize && priceList.length == fileSize) {
            for (int i = 0; i < fileList.length; i++) {
                @SuppressLint("InflateParams")
                View view = getLayoutInflater().inflate(R.layout.transaction_files_viewholder, null);
                TextView textFileName = (TextView) view.findViewById(R.id.textFileName);
                TextView textPrintInfo = (TextView) view.findViewById(R.id.textPrintInfo);
                TextView textPrintSummary = (TextView) view.findViewById(R.id.textPrintSummary);
                TextView textPricePerFile = (TextView) view.findViewById(R.id.textPricePerFile);
                View theLine = view.findViewById(R.id.theLine);
                textFileName.setText(fileList[i]);
                String Page = "Pages: " + pageFromList[i] + " s/d " + pageToList[i];
                textPrintInfo.setText(Page);
                String summary = pagesList[i]+" Pages, "+typeList[i];

                textPricePerFile.setText(DataUtils.getCurrency(Long.parseLong(priceList[i])));
                textPrintSummary.setText(summary);

                theLine.setVisibility(i == (fileList.length - 1) ? View.GONE : View.VISIBLE);

                layFileList.addView(view);
            }
        }else {
            Toast.makeText(getApplicationContext(), "Gagal Parsing File", Toast.LENGTH_SHORT).show();
            Log.e("files", ""+fileList.length);
            Log.e("pageFrom", ""+pageFromList.length);
            Log.e("pageto", ""+pageToList.length);
            Log.e("prices", ""+priceList.length);
        }
    }

    private void getLocation() {
        new APIRequest().ServerLocationList(new APIListener() {
            @Override
            public void onSuccess(APIResponseData response) {
                setUpLocation(response.getLocationList());
            }

            @Override
            public void onFailure(API api, String errorMessage) {
                Toast.makeText(getApplicationContext(), errorMessage, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setUpLocation(List<LocationModel> list) {
        btnLocationList.setVisibility(View.VISIBLE);
        locationText.setVisibility(View.VISIBLE);
        layLocationList.removeAllViews();
        int size = list.size() > 3 ? 3 : list.size();
        for (int i = 0; i < size; i++) {
            @SuppressLint("InflateParams")
            View view = getLayoutInflater().inflate(R.layout.location_item_viewholder, null);

            TextView textLocationName = (TextView) view.findViewById(R.id.textLocationName);
            final LinearLayout layDayHeader = (LinearLayout) view.findViewById(R.id.layDayHeader);
            TextView textOpenClose = (TextView) view.findViewById(R.id.textOpenClose);

            final LinearLayout layDayDetail = (LinearLayout) view.findViewById(R.id.layDayDetail);
            TextView textDay0 = (TextView) view.findViewById(R.id.textDay0);
            TextView textHours0 = (TextView) view.findViewById(R.id.textHours0);
            TextView textDay1 = (TextView) view.findViewById(R.id.textDay1);
            TextView textHours1 = (TextView) view.findViewById(R.id.textHours1);
            TextView textDay2 = (TextView) view.findViewById(R.id.textDay2);
            TextView textHours2 = (TextView) view.findViewById(R.id.textHours2);
            TextView textDay3 = (TextView) view.findViewById(R.id.textDay3);
            TextView textHours3 = (TextView) view.findViewById(R.id.textHours3);
            TextView textDay4 = (TextView) view.findViewById(R.id.textDay4);
            TextView textHours4 = (TextView) view.findViewById(R.id.textHours4);
            TextView textDay5 = (TextView) view.findViewById(R.id.textDay5);
            TextView textHours5 = (TextView) view.findViewById(R.id.textHours5);
            TextView textDay6 = (TextView) view.findViewById(R.id.textDay6);
            TextView textHours6 = (TextView) view.findViewById(R.id.textHours6);

            TextView textLocationDetail = (TextView) view.findViewById(R.id.textLocationDetail);
            TextView textLocationAddress = (TextView) view.findViewById(R.id.textLocationAddress);
            TextView btnDirections = (TextView) view.findViewById(R.id.btnDirections);

            final LocationModel model = list.get(i);

            textLocationName.setText(model.getName());
            textLocationDetail.setText(model.getDetails());
            textLocationAddress.setText(model.getAddress());

            textOpenClose.setText(Tools.getOpenClose(model));

            textDay0.setText(Tools.getDay(0));
            textHours0.setText(Tools.getHours(model, 0));
            textDay1.setText(Tools.getDay(1));
            textHours1.setText(Tools.getHours(model, 1));
            textDay2.setText(Tools.getDay(2));
            textHours2.setText(Tools.getHours(model, 2));
            textDay3.setText(Tools.getDay(3));
            textHours3.setText(Tools.getHours(model, 3));
            textDay4.setText(Tools.getDay(4));
            textHours4.setText(Tools.getHours(model, 4));
            textDay5.setText(Tools.getDay(5));
            textHours5.setText(Tools.getHours(model, 5));
            textDay6.setText(Tools.getDay(6));
            textHours6.setText(Tools.getHours(model, 6));

            layDayHeader.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    layDayHeader.setVisibility(View.GONE);
                    layDayDetail.setVisibility(View.VISIBLE);
                }
            });
            layDayDetail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    layDayHeader.setVisibility(View.VISIBLE);
                    layDayDetail.setVisibility(View.GONE);
                }
            });
            btnDirections.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:"+model.getLatitude()+","+model.getLongitude()+"?q="+
                            model.getLatitude()+","+model.getLongitude()+"("+model.getName()+")"));
                    startActivity(intent);
                }
            });

            layLocationList.addView(view);
        }
    }
}
