package com.tprint.android.view.dialogs;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import com.tprint.android.R;
import com.tprint.android.etc.DataUtils;
import com.tprint.android.view.BaseDialog;
import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by aboot on 7/26/17.
 */

public class PDFRenameDialog extends BaseDialog {

    private OnClickRenameListener onClickRenameListener;

    @BindView(R.id.inputDocName) EditText inputDocName;

    public void setOnClickRenameListener(OnClickRenameListener onClickRenameListener) {
        this.onClickRenameListener = onClickRenameListener;
    }

    public static PDFRenameDialog newInstance(String documentName) {
        Bundle args = new Bundle();
        args.putString("documentName", documentName);
        PDFRenameDialog fragment = new PDFRenameDialog();
        fragment.setCancelable(false);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.pdf_rename_dialog, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (DataUtils.isNotEmpty(getDocumentName())) {
            inputDocName.setText(getDocumentName());
            inputDocName.setSelection(getDocumentName().length());
        }
    }

    private String getDocumentName() {
        return getArguments().getString("documentName");
    }

    @OnClick(R.id.btnCancel) void onCancel() {
        dismissDialog();
    }

    @OnClick(R.id.btnRename) void onGallery() {
        if (DataUtils.isNotEmpty(inputDocName)) {
            if (onClickRenameListener != null) onClickRenameListener.onRename(inputDocName.getText().toString());
            dismissDialog();
        }
    }

    public interface OnClickRenameListener {
        void onRename(String text);
    }
}
