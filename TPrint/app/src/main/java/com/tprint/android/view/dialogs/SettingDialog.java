package com.tprint.android.view.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.PopupMenu;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import com.norbsoft.typefacehelper.TypefaceHelper;
import com.tprint.android.R;
import com.tprint.android.etc.DataUtils;
import com.tprint.android.etc.GilaPrint;
import com.tprint.android.models.PrintModel;
import com.tprint.android.presenter.Print.PrintPresenter;
import com.tprint.android.presenter.Print.PrintViewImpl;
import com.tprint.android.services.API;
import com.tprint.android.services.APIResponseData;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import static android.support.design.widget.BottomSheetBehavior.STATE_HIDDEN;

/**
 * Created by aboot on 7/6/17.
 */

public class SettingDialog extends BottomSheetDialogFragment implements PrintViewImpl {

    private PrintModel printModel;
    private int printReceipt;
    private PrintPresenter printPresenter;
    private LoadingDialog loadingDialog;
    private OnSettingListener onSettingListener;

    @BindView(R.id.groupColor) RadioGroup groupColor;
    @BindView(R.id.radioColor) RadioButton radioColor;
    @BindView(R.id.radioMono) RadioButton radioMono;

    @BindView(R.id.groupSide) RadioGroup groupSide;
    @BindView(R.id.radioOneSide) RadioButton radioOneSide;
    @BindView(R.id.radioTwoSide) RadioButton radioTwoSide;

    @BindView(R.id.groupRange) RadioGroup groupRange;
    @BindView(R.id.radioAll) RadioButton radioAll;
    @BindView(R.id.radioSelection) RadioButton radioSelection;
    @BindView(R.id.textPageFrom) TextView textPageFrom;
    @BindView(R.id.textPageTo) TextView textPageTo;

    @BindView(R.id.inputCopies) EditText inputCopies;
    @BindView(R.id.btnClose) ImageButton btnClose;

    public static SettingDialog newInstance(PrintModel model, OnSettingListener onSettingListener) {
        Bundle args = new Bundle();
        args.putParcelable(GilaPrint.PrintModel, model);
        SettingDialog fragment = new SettingDialog();
        fragment.setArguments(args);
        fragment.setOnSettingListener(onSettingListener);
        return fragment;
    }

    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        View contentView = View.inflate(getContext(), R.layout.setting_dialog, null);
        dialog.setContentView(contentView);
        ButterKnife.bind(this, contentView);
        TypefaceHelper.typeface(contentView);

        printPresenter = new PrintPresenter(this);

        printModel = getArguments().getParcelable(GilaPrint.PrintModel);
        printReceipt = getArguments().getInt("printReceipt", 0);
        if (printModel == null) {
            Toast.makeText(getActivity(), R.string.data_tidak_ditemukan, Toast.LENGTH_SHORT).show();
            dismiss();
        }

        groupColor.check(printModel.getOColor() == 0 ? radioMono.getId() : radioColor.getId());
        groupSide.check(printModel.getODoubleSided() == 0 ? radioOneSide.getId() : radioTwoSide.getId());
        groupRange.check(isAllPages() ? radioAll.getId() : radioSelection.getId());
        textPageFrom.setText(String.valueOf(printModel.getOPageFrom()));
        textPageTo.setText(String.valueOf(printModel.getOPageTo()));
        inputCopies.setText(String.valueOf(printModel.getOCopies()));
        inputCopies.setSelection(inputCopies.getText().length());

        CoordinatorLayout.LayoutParams layoutParams =
                (CoordinatorLayout.LayoutParams) ((View) contentView.getParent()).getLayoutParams();
        CoordinatorLayout.Behavior behavior = layoutParams.getBehavior();
        if (behavior != null && behavior instanceof BottomSheetBehavior) {
            ((BottomSheetBehavior) behavior).setBottomSheetCallback(bottomSheetCallback);
        }

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                if (bottomSheet != null) {
                    BottomSheetBehavior.from(bottomSheet)
                            .setState(BottomSheetBehavior.STATE_EXPANDED);
                }
            }
        });

        Init();
    }

    private void Init() {
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        groupColor.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                printModel.setOColor(radioMono.isChecked() ? 0 : 1);
            }
        });

        groupSide.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                printModel.setODoubleSided(radioOneSide.isChecked() ? 0 : 1);
            }
        });

        groupRange.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                if (checkedId == radioAll.getId() && radioAll.isChecked()) {
                    printModel.setOPageFrom(1);
                    printModel.setOPageTo(printModel.getPages());
                }
                textPageFrom.setText(String.valueOf(printModel.getOPageFrom()));
                textPageTo.setText(String.valueOf(printModel.getOPageTo()));
            }
        });

        textPageFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setPopUpMenu(v);
            }
        });

        textPageTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setPopUpMenu(v);
            }
        });

        inputCopies.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    printModel.setOCopies(Integer.parseInt(s.toString()));
                }
            }
        });
    }

    @OnClick(R.id.btnDelete) void onDelete() {
        printPresenter.PrintRemove(printModel.getID());
    }

    @OnClick(R.id.btnSetting) void onSetting() {
        if (!DataUtils.isNotEmpty(inputCopies)) {
            Toast.makeText(getActivity(), "Jumlah Copy tidak boleh kosong", Toast.LENGTH_SHORT).show();
            return;
        }
        printPresenter.PrintSetting(printModel, printReceipt);
    }

    private BottomSheetBehavior.BottomSheetCallback bottomSheetCallback =
            new BottomSheetBehavior.BottomSheetCallback() {
        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            if (newState == STATE_HIDDEN){
                dismiss();
            }

        }
        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {}
    };

    private boolean isAllPages() {
        return printModel.getOPageFrom() == 1 && printModel.getOPageTo() == printModel.getPages();
    }

    private void setPopUpMenu(final View view) {
        PopupMenu popupMenu = new PopupMenu(getActivity(), view);
        for (int i = 0; i < printModel.getPages(); i++) {
            popupMenu.getMenu().add(String.valueOf(i + 1));
        }
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                int Page = Integer.parseInt(item.getTitle().toString());
                if (view.getId() == textPageFrom.getId()) {
                    if (Page <= printModel.getOPageTo()) {
                        printModel.setOPageFrom(Page);
                        textPageFrom.setText(String.valueOf(printModel.getOPageFrom()));
                        groupRange.check(isAllPages() ? radioAll.getId() : radioSelection.getId());
                    }
                }else if (view.getId() == textPageTo.getId()) {
                    if (Page >= printModel.getOPageFrom()) {
                        printModel.setOPageTo(Page);
                        textPageTo.setText(String.valueOf(printModel.getOPageTo()));
                        groupRange.check(isAllPages() ? radioAll.getId() : radioSelection.getId());
                    }
                }
                return true;
            }
        });
        popupMenu.show();
    }

    protected void showLoadingDialog(API api) {
        String Message;
        if (api == null) {
            Message = null;
        }else if (api == API.Print_Setting) {
            Message = getString(R.string.loading_file_update);
        }else if (api == API.Print_Remove) {
            Message = getString(R.string.loading_file_deleting);
        }else {
            Message = getString(R.string.loadingText);
        }
        dismissLoadingDialog();
        loadingDialog = LoadingDialog.newInstance(Message);
        loadingDialog.show(getChildFragmentManager(), Message);
    }

    protected void dismissLoadingDialog() {
        if (loadingDialog != null) {
            loadingDialog.dismissDialog();
            loadingDialog = null;
        }
    }

    @Override
    public void showLoading(API api) {
        showLoadingDialog(api);
    }

    @Override
    public void hideLoading() {
        dismissLoadingDialog();
    }

    @Override
    public void UploadSuccess() {}

    @Override
    public void ListSuccess(APIResponseData response) {}

    @Override
    public void SettingSuccess() {
        dismiss();
        if (onSettingListener != null) onSettingListener.onChange(true);
    }

    @Override
    public void PreviewSuccess(String Url) {}

    @Override
    public void RemoveSuccess() {
        dismiss();
        if (onSettingListener != null) onSettingListener.onChange(true);
    }

    @Override
    public void RefreshAllSuccess() {}

    @Override
    public void RemoveAllSuccess() {}

    @Override
    public void ConfirmationSuccess() {}

    @Override
    public void CheckOutSuccess(APIResponseData response) {}

    @Override
    public void RequestFailed(API api, String errorMessage) {
        Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT).show();
    }

    public interface OnSettingListener {
        void onChange(boolean isChange);
    }

    public void setOnSettingListener(OnSettingListener onSettingListener) {
        this.onSettingListener = onSettingListener;
    }
}
