package com.tprint.android.presenter.Wallet;

import com.tprint.android.services.API;
import com.tprint.android.services.APIListener;
import com.tprint.android.services.APIRequest;
import com.tprint.android.services.APIResponseData;

/**
 * Created by aboot on 7/17/17.
 */

public class WalletPresenter implements WalletPresenterImpl {

    private WalletViewImpl walletView;

    public WalletPresenter(WalletViewImpl walletView) {
        this.walletView = walletView;
    }

    @Override
    public void WalletList() {
        walletView.showLoading(API.Wallet_List);
        new APIRequest().WalletList(new APIListener() {
            @Override
            public void onSuccess(APIResponseData response) {
                walletView.hideLoading();
                walletView.ListSuccess(response);
            }

            @Override
            public void onFailure(API api, String errorMessage) {
                walletView.hideLoading();
                walletView.RequestFailed(api, errorMessage);
            }
        });
    }

    @Override
    public void WalletTopUpList() {
        walletView.showLoading(API.Wallet_TopUpList);
        new APIRequest().WalletTopUpList(new APIListener() {
            @Override
            public void onSuccess(APIResponseData response) {
                walletView.hideLoading();
                walletView.TopUpListSuccess(response);
            }

            @Override
            public void onFailure(API api, String errorMessage) {
                walletView.hideLoading();
                walletView.RequestFailed(api, errorMessage);
            }
        });

    }

    @Override
    public void WalletTopUp(long Amount, String Type) {
        walletView.showLoading(API.Wallet_TopUp);
        new APIRequest().WalletTopUp(Amount, Type, new APIListener() {
            @Override
            public void onSuccess(APIResponseData response) {
                walletView.hideLoading();
                walletView.TopUpSuccess();
            }

            @Override
            public void onFailure(API api, String errorMessage) {
                walletView.hideLoading();
                walletView.RequestFailed(api, errorMessage);
            }
        });
    }

    @Override
    public void WalletTopUpCancel(String Type) {
        walletView.showLoading(API.Wallet_TopUpCancel);
        new APIRequest().WalletTopUpCancel(Type, new APIListener() {
            @Override
            public void onSuccess(APIResponseData response) {
                walletView.hideLoading();
                walletView.TopUpCancelSuccess();
            }

            @Override
            public void onFailure(API api, String errorMessage) {
                walletView.hideLoading();
                walletView.RequestFailed(api, errorMessage);
            }
        });
    }
}
