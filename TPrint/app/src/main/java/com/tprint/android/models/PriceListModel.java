package com.tprint.android.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by aboot on 7/6/17.
 */

public class PriceListModel implements Parcelable {

    private long A4MonoLow;
    private long A4MonoMedium;
    private long A4MonoHigh;
    private long A4ColorLow;
    private long A4ColorMedium;
    private long A4ColorHigh;
    private long A4Photo;
    private long A3MonoLow;
    private long A3MonoMedium;
    private long A3MonoHigh;
    private long A3ColorLow;
    private long A3ColorMedium;
    private long A3ColorHigh;
    private long pricePackaging;

    public PriceListModel() {}

    protected PriceListModel(Parcel in) {
        A4MonoLow = in.readLong();
        A4MonoMedium = in.readLong();
        A4MonoHigh = in.readLong();
        A4ColorLow = in.readLong();
        A4ColorMedium = in.readLong();
        A4ColorHigh = in.readLong();
        A4Photo = in.readLong();
        A3MonoLow = in.readLong();
        A3MonoMedium = in.readLong();
        A3MonoHigh = in.readLong();
        A3ColorLow = in.readLong();
        A3ColorMedium = in.readLong();
        A3ColorHigh = in.readLong();
        pricePackaging = in.readLong();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(A4MonoLow);
        dest.writeLong(A4MonoMedium);
        dest.writeLong(A4MonoHigh);
        dest.writeLong(A4ColorLow);
        dest.writeLong(A4ColorMedium);
        dest.writeLong(A4ColorHigh);
        dest.writeLong(A4Photo);
        dest.writeLong(A3MonoLow);
        dest.writeLong(A3MonoMedium);
        dest.writeLong(A3MonoHigh);
        dest.writeLong(A3ColorLow);
        dest.writeLong(A3ColorMedium);
        dest.writeLong(A3ColorHigh);
        dest.writeLong(pricePackaging);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<PriceListModel> CREATOR = new Creator<PriceListModel>() {
        @Override
        public PriceListModel createFromParcel(Parcel in) {
            return new PriceListModel(in);
        }

        @Override
        public PriceListModel[] newArray(int size) {
            return new PriceListModel[size];
        }
    };

    public long getA4MonoLow() {
        return A4MonoLow;
    }

    public void setA4MonoLow(long a4MonoLow) {
        A4MonoLow = a4MonoLow;
    }

    public long getA4MonoMedium() {
        return A4MonoMedium;
    }

    public void setA4MonoMedium(long a4MonoMedium) {
        A4MonoMedium = a4MonoMedium;
    }

    public long getA4MonoHigh() {
        return A4MonoHigh;
    }

    public void setA4MonoHigh(long a4MonoHigh) {
        A4MonoHigh = a4MonoHigh;
    }

    public long getA4ColorLow() {
        return A4ColorLow;
    }

    public void setA4ColorLow(long a4ColorLow) {
        A4ColorLow = a4ColorLow;
    }

    public long getA4ColorMedium() {
        return A4ColorMedium;
    }

    public void setA4ColorMedium(long a4ColorMedium) {
        A4ColorMedium = a4ColorMedium;
    }

    public long getA4ColorHigh() {
        return A4ColorHigh;
    }

    public void setA4ColorHigh(long a4ColorHigh) {
        A4ColorHigh = a4ColorHigh;
    }

    public long getA4Photo() {
        return A4Photo;
    }

    public void setA4Photo(long a4Photo) {
        A4Photo = a4Photo;
    }

    public long getA3MonoLow() {
        return A3MonoLow;
    }

    public void setA3MonoLow(long a3MonoLow) {
        A3MonoLow = a3MonoLow;
    }

    public long getA3MonoMedium() {
        return A3MonoMedium;
    }

    public void setA3MonoMedium(long a3MonoMedium) {
        A3MonoMedium = a3MonoMedium;
    }

    public long getA3MonoHigh() {
        return A3MonoHigh;
    }

    public void setA3MonoHigh(long a3MonoHigh) {
        A3MonoHigh = a3MonoHigh;
    }

    public long getA3ColorLow() {
        return A3ColorLow;
    }

    public void setA3ColorLow(long a3ColorLow) {
        A3ColorLow = a3ColorLow;
    }

    public long getA3ColorMedium() {
        return A3ColorMedium;
    }

    public void setA3ColorMedium(long a3ColorMedium) {
        A3ColorMedium = a3ColorMedium;
    }

    public long getA3ColorHigh() {
        return A3ColorHigh;
    }

    public void setA3ColorHigh(long a3ColorHigh) {
        A3ColorHigh = a3ColorHigh;
    }

    public long getPricePackaging() {
        return pricePackaging;
    }

    public void setPricePackaging(long pricePackaging) {
        this.pricePackaging = pricePackaging;
    }
}
