package com.tprint.android.notification;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.tprint.android.database.SessionApps;
import com.tprint.android.services.API;
import com.tprint.android.services.APIListener;
import com.tprint.android.services.APIRequest;
import com.tprint.android.services.APIResponseData;

/**
 * Created by aboot on 7/5/17.
 */

public class PrintGilaInstanceID extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        SessionApps.setTokenFCM(refreshedToken);
        new APIRequest().MeAddPushID(refreshedToken, new APIListener() {
            @Override
            public void onSuccess(APIResponseData response) {}

            @Override
            public void onFailure(API api, String errorMessage) {}
        });
    }
}
