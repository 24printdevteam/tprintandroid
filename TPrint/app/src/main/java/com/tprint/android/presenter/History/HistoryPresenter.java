package com.tprint.android.presenter.History;

import com.tprint.android.services.API;
import com.tprint.android.services.APIListener;
import com.tprint.android.services.APIRequest;
import com.tprint.android.services.APIResponseData;

/**
 * Created by aboot on 7/15/17.
 */

public class HistoryPresenter implements HistoryPresenterImpl {

    HistoryViewImpl historyView;

    public HistoryPresenter(HistoryViewImpl historyView) {
        this.historyView = historyView;
    }

    @Override
    public void HistoryList() {
        historyView.showLoading(API.History_List);
        new APIRequest().HistoryList(new APIListener() {
            @Override
            public void onSuccess(APIResponseData response) {
                historyView.hideLoading();
                historyView.ListSuccess(response);
            }

            @Override
            public void onFailure(API api, String errorMessage) {
                historyView.hideLoading();
                historyView.RequestFailed(api, errorMessage);
            }
        });
    }

    @Override
    public void HistoryDetails(String ID) {
        historyView.showLoading(API.History_Details);
        new APIRequest().HistoryDetails(ID, new APIListener() {
            @Override
            public void onSuccess(APIResponseData response) {
                historyView.hideLoading();
                historyView.DetailsSuccess(response);
            }

            @Override
            public void onFailure(API api, String errorMessage) {
                historyView.hideLoading();
                historyView.RequestFailed(api, errorMessage);
            }
        });
    }
}
