package com.tprint.android.presenter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.tprint.android.R;
import com.tprint.android.models.PrintModel;
import com.tprint.android.view.BaseViewHolder;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class StorageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<PrintModel> list = new ArrayList<>();
    private OnClickListener onClickListener;

    public void addNewList(List<PrintModel> list) {
        if (list != null) {
            this.list.clear();
            this.list.addAll(list);
            notifyDataSetChanged();
        }
    }

    public void setOnClickListener(OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.storage_item_viewholder, parent, false);
        return new ItemHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ItemHolder) {
            ItemHolder itemHolder = (ItemHolder) holder;
            final PrintModel model = list.get(position);
            itemHolder.textNumberList.setText("File "+(position+1)+"/"+list.size());
            itemHolder.textFileName.setText(model.getFilename()+"."+model.getExtension());
            itemHolder.textFileFormat.setText(model.getSize()+", "+model.getPages()+" Halaman");

            itemHolder.btnPrint.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onClickListener != null) onClickListener.onPrint(model.getID());
                }
            });
            itemHolder.btnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onClickListener != null) onClickListener.onRemove(model.getID());
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ItemHolder extends BaseViewHolder {

        @BindView(R.id.textNumberList) TextView textNumberList;
        @BindView(R.id.textFileName) TextView textFileName;
        @BindView(R.id.textFileFormat) TextView textFileFormat;

        @BindView(R.id.btnPrint) ImageButton btnPrint;
        @BindView(R.id.btnDelete) ImageButton btnDelete;

        public ItemHolder(View itemView) {
            super(itemView);
        }
    }

    public interface OnClickListener {
        void onPrint(String ID);
        void onRemove(String ID);
    }
}
