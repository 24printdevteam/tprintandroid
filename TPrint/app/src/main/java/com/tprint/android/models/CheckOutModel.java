package com.tprint.android.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by aboot on 7/15/17.
 */

public class CheckOutModel implements Parcelable {

    private String ID;
    private long Expired;

    protected CheckOutModel(Parcel in) {
        ID = in.readString();
        Expired = in.readLong();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(ID);
        dest.writeLong(Expired);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CheckOutModel> CREATOR = new Creator<CheckOutModel>() {
        @Override
        public CheckOutModel createFromParcel(Parcel in) {
            return new CheckOutModel(in);
        }

        @Override
        public CheckOutModel[] newArray(int size) {
            return new CheckOutModel[size];
        }
    };

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public long getExpired() {
        return Expired;
    }

    public void setExpired(long expired) {
        Expired = expired;
    }
}
