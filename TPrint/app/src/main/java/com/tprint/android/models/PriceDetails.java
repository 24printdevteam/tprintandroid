package com.tprint.android.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Map;

/**
 * Created by aboot on 7/17/17.
 */

public class PriceDetails implements Parcelable {

    private Map<String, Long> Price;
    private long SubTotal;
    private int Receipt;
    private long TotalPrice;
    private int TotalPages;
    private Map<String, String> SQ;
    private Map<String, String> SH;
    private Map<String, String> SF;

    protected PriceDetails(Parcel in) {
        SubTotal = in.readLong();
        Receipt = in.readInt();
        TotalPrice = in.readLong();
        TotalPages = in.readInt();

        final int PriceSize = in.readInt();
        for (int i=0; i<PriceSize; i++) {
            Price.put(in.readString(), in.readLong());
        }

        final int SQSize = in.readInt();
        for (int i=0; i<SQSize; i++) {
            SQ.put(in.readString(), in.readString());
        }

        final int SHSize = in.readInt();
        for (int i=0; i<SHSize; i++) {
            SH.put(in.readString(), in.readString());
        }

        final int SFSize = in.readInt();
        for (int i=0; i<SFSize; i++) {
            SF.put(in.readString(), in.readString());
        }
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(SubTotal);
        dest.writeInt(Receipt);
        dest.writeLong(TotalPrice);
        dest.writeInt(TotalPages);

        final int priceSize = Price.size();
        dest.writeInt(priceSize);
        if (priceSize > 0) {
            for (Map.Entry<String, Long> entry : Price.entrySet()) {
                dest.writeString(entry.getKey());
                dest.writeLong(entry.getValue());
            }
        }

        final int SQSize = SQ.size();
        dest.writeInt(SQSize);
        if (SQSize > 0) {
            for (Map.Entry<String, String> entry : SQ.entrySet()) {
                dest.writeString(entry.getKey());
                dest.writeString(entry.getValue());
            }
        }

        final int SHSize = SH.size();
        dest.writeInt(SHSize);
        if (SHSize > 0) {
            for (Map.Entry<String, String> entry : SH.entrySet()) {
                dest.writeString(entry.getKey());
                dest.writeString(entry.getValue());
            }
        }

        final int SFSize = SF.size();
        dest.writeInt(SFSize);
        if (SFSize > 0) {
            for (Map.Entry<String, String> entry : SF.entrySet()) {
                dest.writeString(entry.getKey());
                dest.writeString(entry.getValue());
            }
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<PriceDetails> CREATOR = new Creator<PriceDetails>() {
        @Override
        public PriceDetails createFromParcel(Parcel in) {
            return new PriceDetails(in);
        }

        @Override
        public PriceDetails[] newArray(int size) {
            return new PriceDetails[size];
        }
    };

    public Map<String, Long> getPrice() {
        return Price;
    }

    public void setPrice(Map<String, Long> price) {
        Price = price;
    }

    public long getSubTotal() {
        return SubTotal;
    }

    public void setSubTotal(long subTotal) {
        SubTotal = subTotal;
    }

    public int getReceipt() {
        return Receipt;
    }

    public void setReceipt(int receipt) {
        Receipt = receipt;
    }

    public long getTotalPrice() {
        return TotalPrice;
    }

    public void setTotalPrice(long totalPrice) {
        TotalPrice = totalPrice;
    }

    public int getTotalPages() {
        return TotalPages;
    }

    public void setTotalPages(int totalPages) {
        TotalPages = totalPages;
    }

    public Map<String, String> getSQ() {
        return SQ;
    }

    public void setSQ(Map<String, String> SQ) {
        this.SQ = SQ;
    }

    public Map<String, String> getSH() {
        return SH;
    }

    public void setSH(Map<String, String> SH) {
        this.SH = SH;
    }

    public Map<String, String> getSF() {
        return SF;
    }

    public void setSF(Map<String, String> SF) {
        this.SF = SF;
    }

    @Override
    public String toString() {
        return "PriceDetails{" +
                "Price=" + Price +
                ", SubTotal=" + SubTotal +
                ", Receipt=" + Receipt +
                ", TotalPrice=" + TotalPrice +
                ", TotalPages=" + TotalPages +
                ", SQ=" + SQ +
                ", SH=" + SH +
                ", SF=" + SF +
                '}';
    }
}
