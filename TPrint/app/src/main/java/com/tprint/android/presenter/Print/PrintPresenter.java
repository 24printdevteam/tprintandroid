package com.tprint.android.presenter.Print;

import com.tprint.android.database.SessionApps;
import com.tprint.android.models.PrintModel;
import com.tprint.android.services.API;
import com.tprint.android.services.APIListener;
import com.tprint.android.services.APIRequest;
import com.tprint.android.services.APIResponseData;

import java.io.File;
import java.util.List;

/**
 * Created by aboot on 7/5/17.
 */

public class PrintPresenter implements PrintPresenterImpl {

    private PrintViewImpl printView;

    public PrintPresenter(PrintViewImpl printView) {
        this.printView = printView;
    }

    @Override
    public void PrintUpload(List<File> list) {
        printView.showLoading(API.Print_Upload);
        new APIRequest().PrintUpload(list, new APIListener() {
            @Override
            public void onSuccess(APIResponseData response) {
                printView.hideLoading();
                printView.UploadSuccess();
            }

            @Override
            public void onFailure(API api, String errorMessage) {
                printView.hideLoading();
                printView.RequestFailed(api, errorMessage);
            }
        });
    }

    @Override
    public void PrintList() {
        printView.showLoading(API.Print_List);
        new APIRequest().PrintList(new APIListener() {
            @Override
            public void onSuccess(APIResponseData response) {
                if (response.getCode() == 101) {
                    SessionApps.setUploadList(null);
                    SessionApps.setUploadStatus(0);
                }
                printView.hideLoading();
                printView.ListSuccess(response);
            }

            @Override
            public void onFailure(API api, String errorMessage) {
                printView.hideLoading();
                printView.RequestFailed(api, errorMessage);
            }
        });
    }

    @Override
    public void PrintSetting(PrintModel printModel, int PrintReceipt) {
        printView.showLoading(API.Print_Setting);
        new APIRequest().PrintSetting(printModel, PrintReceipt, new APIListener() {
            @Override
            public void onSuccess(APIResponseData response) {
                printView.hideLoading();
                printView.SettingSuccess();
            }

            @Override
            public void onFailure(API api, String errorMessage) {
                printView.hideLoading();
                printView.RequestFailed(api, errorMessage);
            }
        });
    }

    @Override
    public void PrintPreview(String ID, int Color, int PageFrom, int PageTo) {
        printView.showLoading(API.Print_Preview);
        new APIRequest().PrintPreview(ID, Color, PageFrom, PageTo, new APIListener() {
            @Override
            public void onSuccess(APIResponseData response) {
                printView.hideLoading();
                printView.PreviewSuccess(response.getPreviewUrl());
            }

            @Override
            public void onFailure(API api, String errorMessage) {
                printView.hideLoading();
                printView.RequestFailed(api, errorMessage);
            }
        });
    }

    @Override
    public void PrintRemove(String ID) {
        printView.showLoading(API.Print_Remove);
        new APIRequest().PrintRemove(ID, new APIListener() {
            @Override
            public void onSuccess(APIResponseData response) {
                printView.hideLoading();
                printView.RemoveSuccess();
            }

            @Override
            public void onFailure(API api, String errorMessage) {
                printView.hideLoading();
                printView.RequestFailed(api, errorMessage);
            }
        });
    }

    @Override
    public void PrintRefreshAll() {
        printView.showLoading(API.Print_RefreshAll);
        new APIRequest().PrintRefreshAll(new APIListener() {
            @Override
            public void onSuccess(APIResponseData response) {
                printView.hideLoading();
                printView.RefreshAllSuccess();
            }

            @Override
            public void onFailure(API api, String errorMessage) {
                printView.hideLoading();
                printView.RequestFailed(api, errorMessage);
            }
        });
    }

    @Override
    public void PrintRemoveAll() {
        printView.showLoading(API.Print_RemoveAll);
        new APIRequest().PrintRemoveAll(new APIListener() {
            @Override
            public void onSuccess(APIResponseData response) {
                printView.hideLoading();
                printView.RemoveAllSuccess();
            }

            @Override
            public void onFailure(API api, String errorMessage) {
                printView.hideLoading();
                printView.RequestFailed(api, errorMessage);
            }
        });
    }

    @Override
    public void PrintConfirmation() {
        printView.showLoading(API.Print_Confirmation);
        new APIRequest().PrintConfirmation(new APIListener() {
            @Override
            public void onSuccess(APIResponseData response) {
                printView.hideLoading();
                printView.ConfirmationSuccess();
            }

            @Override
            public void onFailure(API api, String errorMessage) {
                printView.hideLoading();
                printView.RequestFailed(api, errorMessage);
            }
        });
    }

    @Override
    public void PrintCheckOut(String Method) {
        printView.showLoading(API.Print_CheckOut);
        new APIRequest().PrintCheckOut(Method, new APIListener() {
            @Override
            public void onSuccess(APIResponseData response) {
                printView.hideLoading();
                printView.CheckOutSuccess(response);
            }

            @Override
            public void onFailure(API api, String errorMessage) {
                printView.hideLoading();
                printView.RequestFailed(api, errorMessage);
            }
        });
    }
}
