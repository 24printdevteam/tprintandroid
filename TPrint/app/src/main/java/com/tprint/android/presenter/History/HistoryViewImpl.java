package com.tprint.android.presenter.History;

import com.tprint.android.services.API;
import com.tprint.android.services.APIResponseData;

/**
 * Created by aboot on 7/15/17.
 */

public interface HistoryViewImpl {

    void showLoading(API api);
    void hideLoading();
    void ListSuccess(APIResponseData response);
    void DetailsSuccess(APIResponseData response);
    void RequestFailed(API api, String errorMessage);
}
