package com.tprint.android.services;

/**
 * Created by aboot on 6/28/17.
 */

public enum  API {

    Me_Login,
    Me_Register,
    Me_Profile,
    Me_ResendVerification,
    Me_Verification,
    Me_ChangePhoneNumber,
    Me_ForgotPassword,
    Me_ResetPasswordCode,
    Me_ResetPassword,
    Me_AddPushID,
    Print_Upload,
    Print_List,
    Print_Setting,
    Print_Preview,
    Print_Remove,
    Print_RefreshAll,
    Print_RemoveAll,
    Print_Confirmation,
    Print_CheckOut,
    History_List,
    History_Details,
    Wallet_List,
    Wallet_TopUpList,
    Wallet_TopUp,
    Wallet_TopUpCancel,
    Storage_Upload,
    Storage_List,
    Storage_Print,
    Storage_Remove,
    Server_PriceList,
    Server_LocationList,
    Server_BankList


}
