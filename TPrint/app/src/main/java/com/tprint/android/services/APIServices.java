package com.tprint.android.services;

import java.util.Map;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PartMap;

/**
 * Created by aboot on 6/28/17.
 */

public interface APIServices {

    @FormUrlEncoded
    @POST("aP1/1.0/Me/Register")
    Call<APIResponseData> meRegister(
            @Field("DeviceID") String DeviceID,
            @Field("Type") String Type,
            @Field("Username") String Username,
            @Field("Email") String Email,
            @Field("Name") String Name,
            @Field("Password") String Password
    );

    @FormUrlEncoded
    @POST("aP1/1.0/Me/Login")
    Call<APIResponseData> meLogin(
            @Field("DeviceID") String DeviceID,
            @Field("Type") String Type,
            @Field("Username") String Username,
            @Field("Password") String Password
    );

    @FormUrlEncoded
    @POST("aP1/1.0/Me/Profile")
    Call<APIResponseData> meProfile(
            @Field("Token") String Token
    );

    @FormUrlEncoded
    @POST("aP1/1.0/Me/ResendVerification")
    Call<APIResponseData> meResendVerification(
            @Field("Token") String Token
    );

    @FormUrlEncoded
    @POST("aP1/1.0/Me/Verification")
    Call<APIResponseData> meVerification(
            @Field("Token") String Token,
            @Field("Code") String Code
    );

    @FormUrlEncoded
    @POST("aP1/1.0/Me/ChangePhoneNumber")
    Call<APIResponseData> meChangePhoneNumber(
            @Field("Token") String Token,
            @Field("Phone") String Phone
    );

    @Multipart
    @POST("aP1/1.0/Me/ForgotPassword")
    Call<APIResponseData> meForgotPassword(
            @PartMap Map<String, RequestBody> emailPhone
    );

    @Multipart
    @POST("aP1/1.0/Me/ResetPasswordCode")
    Call<APIResponseData> meResetPasswordCode(
            @PartMap Map<String, RequestBody> emailPhone
    );

    @FormUrlEncoded
    @POST("aP1/1.0/Me/ResetPassword")
    Call<APIResponseData> meResetPassword(
            @Field("Token") String Token,
            @Field("Password") String Code
    );

    @FormUrlEncoded
    @POST("aP1/1.0/Me/AddPushID")
    Call<APIResponseData> meAddPushID(
            @Field("DeviceID") String DeviceID,
            @Field("PushID") String PushID
    );

    @Multipart
    @POST("aP1/1.0/Print/Upload")
    Call<APIResponseData> printUpload(
            @PartMap Map<String, RequestBody> file
    );

    @FormUrlEncoded
    @POST("aP1/1.0/Print/List")
    Call<APIResponseData> printList(
            @Field("Token") String Token
    );

    @Multipart
    @POST("aP1/1.0/Print/Setting")
    Call<APIResponseData> printSetting(
            @PartMap Map<String, RequestBody> maps
    );

    @FormUrlEncoded
    @POST("aP1/1.0/Print/Preview")
    Call<APIResponseData> printPreview(
            @Field("Token") String Token,
            @Field("ID") String ID,
            @Field("Color") int Color,
            @Field("PageFrom") int PageFrom,
            @Field("PageTo") int PageTo

    );

    @FormUrlEncoded
    @POST("aP1/1.0/Print/Remove")
    Call<APIResponseData> printRemove(
            @Field("Token") String Token,
            @Field("ID") String ID
    );

    @FormUrlEncoded
    @POST("aP1/1.0/Print/RefreshAll")
    Call<APIResponseData> printRefreshAll(
            @Field("Token") String Token
    );

    @FormUrlEncoded
    @POST("aP1/1.0/Print/RemoveAll")
    Call<APIResponseData> printRemoveAll(
            @Field("Token") String Token
    );

    @FormUrlEncoded
    @POST("aP1/1.0/Print/Confirmation")
    Call<APIResponseData> printConfirmation(
            @Field("Token") String Token
    );

    @FormUrlEncoded
    @POST("aP1/1.0/Print/CheckOut")
    Call<APIResponseData> printCheckOut(
            @Field("Token") String Token,
            @Field("Type") String Type

    );

    @FormUrlEncoded
    @POST("aP1/1.0/History/List")
    Call<APIResponseData> historyList(
            @Field("Token") String Token

    );

    @FormUrlEncoded
    @POST("aP1/1.0/History/Details")
    Call<APIResponseData> historyDetails(
            @Field("Token") String Token,
            @Field("ID") String ID
    );

    @FormUrlEncoded
    @POST("aP1/1.0/Wallet/List")
    Call<APIResponseData> walletList(
            @Field("Token") String Token
    );

    @FormUrlEncoded
    @POST("aP1/1.0/Wallet/TopUpList")
    Call<APIResponseData> walletTopUpList(
            @Field("Token") String Token
    );

    @FormUrlEncoded
    @POST("aP1/1.0/Wallet/TopUp")
    Call<APIResponseData> walletTopUp(
            @Field("Token") String Token,
            @Field("Amount") long Amount,
            @Field("Type") String Type
    );

    @FormUrlEncoded
    @POST("aP1/1.0/Wallet/TopUpCancel")
    Call<APIResponseData> walletTopUpCancel(
            @Field("Token") String Token,
            @Field("Type") String Type
    );

    @Multipart
    @POST("aP1/1.0/Storage/Upload")
    Call<APIResponseData> storageUpload(
            @PartMap Map<String, RequestBody> file
    );

    @FormUrlEncoded
    @POST("aP1/1.0/Storage/List")
    Call<APIResponseData> storageList(
            @Field("Token") String Token
    );

    @FormUrlEncoded
    @POST("aP1/1.0/Storage/Remove")
    Call<APIResponseData> storageRemove(
            @Field("Token") String Token,
            @Field("ID") String ID
    );

    @FormUrlEncoded
    @POST("aP1/1.0/Storage/Print")
    Call<APIResponseData> storagePrint(
            @Field("Token") String Token,
            @Field("IDs") String IDs
    );

    @FormUrlEncoded
    @POST("aP1/1.0/Server/PriceList")
    Call<APIResponseData> serverPriceList(
            @Field("Token") String Token
    );

    @FormUrlEncoded
    @POST("aP1/1.0/Server/LocationList")
    Call<APIResponseData> serverLocationList(
            @Field("Token") String Token
    );

    @FormUrlEncoded
    @POST("aP1/1.0/Server/BankList")
    Call<APIResponseData> serverBankList(
            @Field("Token") String Token
    );
}
