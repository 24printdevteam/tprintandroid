package com.tprint.android.view.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.tprint.android.R;
import com.tprint.android.database.SessionApps;
import com.tprint.android.etc.DataUtils;
import com.tprint.android.etc.GilaPrint;
import com.tprint.android.models.PrintModel;
import com.tprint.android.presenter.Print.PrintPageImpl;
import com.tprint.android.presenter.Print.PrintPresenter;
import com.tprint.android.presenter.Print.PrintViewImpl;
import com.tprint.android.presenter.PrintAdapter;
import com.tprint.android.services.API;
import com.tprint.android.services.APIResponseData;
import com.tprint.android.view.BaseFragment;
import com.tprint.android.view.activities.PreviewActivity;
import com.tprint.android.view.dialogs.SettingDialog;
import java.util.List;
import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by aboot on 7/5/17.
 */

public class PrintListFragment extends BaseFragment implements
        PrintAdapter.OnClickListener,
        SwipeRefreshLayout.OnRefreshListener, PrintViewImpl {

    private long TotalPrice;

    private PrintPageImpl printPage;
    private PrintAdapter printAdapter;
    private PrintPresenter printPresenter;

    @BindView(R.id.refreshLayout) SwipeRefreshLayout refreshLayout;
    @BindView(R.id.recyclerView) RecyclerView recyclerView;
    @BindView(R.id.checkboxInvoice) CheckBox checkboxInvoice;
    @BindView(R.id.layProcess) LinearLayout layProcess;
    @BindView(R.id.textReceipt) TextView textReceipt;
    @BindView(R.id.textGrandTotal) TextView textGrandTotal;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (!(context instanceof PrintPageImpl)) {
            throw new ClassCastException("Activity must implement PrintPageImpl");
        }
        this.printPage = (PrintPageImpl) context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.print_list_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getToolbar().setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        setToolbarTitle(GilaPrint.PrintList);

        printPresenter = new PrintPresenter(this);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        printAdapter = new PrintAdapter(getActivity());
        recyclerView.setAdapter(printAdapter);
        printAdapter.setOnClickListener(this);
        refreshLayout.setOnRefreshListener(this);

        List<PrintModel> list = getArguments().getParcelableArrayList("printList");
        if (list != null) {
            if (list.size() == 0) {
                showEmpty(getString(R.string.empty_print_list));
            }else {
                printAdapter.addNewList(list);
            }
        }else {
            printPresenter.PrintList();
        }


        checkboxInvoice.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                setUpTotal();
            }
        });
    }

    @OnClick(R.id.btnProcess) void onProcess() {
        PrintModel printModel = printAdapter.getList().get(0);
        printPresenter.PrintSetting(printModel, checkboxInvoice.isChecked() ? 1 : 0);
    }

    private void setUpTotal() {
        List<PrintModel> list = printAdapter.getList();
        long Prices = 0;
        for (int i = 0; i < list.size(); i++) {
            Prices += list.get(i).getTotalPerFile();
        }
        long receipt = SessionApps.getPriceList().getPricePackaging();
        TotalPrice = (checkboxInvoice.isChecked() ? receipt : 0) + Prices;

        textReceipt.setText(DataUtils.getCurrency(receipt));
        textGrandTotal.setText(DataUtils.getCurrency(TotalPrice));
    }

    @Override
    public void onRefresh() {
        printPresenter.PrintList();
    }


    @Override
    public void onLoaded() {
        setUpTotal();
    }

    @Override
    public void onPreview(PrintModel model) {
        printPresenter.PrintPreview(model.getID(), model.getOColor(), model.getOPageFrom(), model.getOPageTo());
    }

    @Override
    public void onSetting(PrintModel model) {
        SettingDialog.newInstance(model, new SettingDialog.OnSettingListener() {
            @Override
            public void onChange(boolean isChange) {
                if (isChange) {
                    printPresenter.PrintList();
                }
            }
        }).show(getChildFragmentManager(), "Setting");
    }

    @Override
    public void onAddNewItem() {
        if (isAdded()) {
            printPage.ChangePage(GilaPrint.PrintUpload, null);
        }
    }

    @Override
    public void showLoading(API api) {
        showLoadingDialog(api);
    }

    @Override
    public void hideLoading() {
        dismissLoadingDialog();
    }

    @Override
    public void UploadSuccess() {}

    @Override
    public void ListSuccess(APIResponseData response) {
        hideAllState();
        layProcess.setVisibility(View.VISIBLE);
        if (response.getCode() == 101) {
            refreshLayout.setRefreshing(false);
            if (isAdded()) {
                printAdapter.addNewList(response.getPrintList());
            }
        }else if (response.getCode() == 200 || response.getCode() == 300) {
            if (isAdded()) {
                printPage.ChangePage(GilaPrint.PrintUpload, null);
            }
        }
    }

    @Override
    public void SettingSuccess() {
        printPresenter.PrintConfirmation();
    }

    @Override
    public void PreviewSuccess(String Url) {
        Log.e("asdasdasdasdadsasd", Url);
        Intent intent = new Intent(getActivity(), PreviewActivity.class);
        intent.putExtra("pdfPreview", Url);
        startActivity(intent);
    }

    @Override
    public void RemoveSuccess() {}

    @Override
    public void RefreshAllSuccess() {}

    @Override
    public void RemoveAllSuccess() {}

    @Override
    public void ConfirmationSuccess() {
        printPage.ChangePage(TotalPrice);
    }

    @Override
    public void CheckOutSuccess(APIResponseData response) {}

    @Override
    public void RequestFailed(API api, String errorMessage) {
        Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT).show();
    }
}
