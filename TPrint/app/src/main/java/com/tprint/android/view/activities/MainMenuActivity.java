package com.tprint.android.view.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.MenuItem;
import com.tprint.android.R;
import com.tprint.android.database.SessionApps;
import com.tprint.android.etc.GilaPrint;
import com.tprint.android.services.API;
import com.tprint.android.services.APIListener;
import com.tprint.android.services.APIRequest;
import com.tprint.android.services.APIResponseData;
import com.tprint.android.view.BaseActivity;
import com.tprint.android.view.customs.BottomNavigationViewHelper;
import com.tprint.android.view.fragments.HistoryMenuFragment;
import com.tprint.android.view.fragments.HomeFragment;
import com.tprint.android.view.fragments.ProfileFragment;
import com.tprint.android.view.fragments.WalletFragment;
import butterknife.BindView;

/**
 * Created by aboot on 5/12/17.
 */

public class MainMenuActivity extends BaseActivity {

    int oldPosition = -1;
    Fragment homeFragment;
    Fragment walletFragment;
    Fragment historyFragment;
    Fragment profileFragment;

    @BindView(R.id.bottomNavigationView) BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_menu_activity);

        replaceFragment(0);
        BottomNavigationViewHelper.disableShiftMode(bottomNavigationView);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_home:
                        replaceFragment(0);
                        break;
                    case R.id.action_wallet:
                        replaceFragment(1);
                        break;
                    case R.id.action_history:
                        replaceFragment(2);
                        break;
                    case R.id.action_profile:
                        replaceFragment(3);
                        break;
                }
                return true;
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        ServerPrice();
    }

    @Override
    public void onBackPressed(){
        if (isHome()) {
            finish();
        }else {
            bottomNavigationView.setSelectedItemId(R.id.action_home);
        }
    }

    private boolean isHome() {
        HomeFragment homeFragment = (HomeFragment) getSupportFragmentManager().findFragmentByTag(GilaPrint.HomePage);
        return homeFragment != null && homeFragment.isVisible();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (homeFragment != null) homeFragment.onActivityResult(requestCode, resultCode, data);
    }

    public void replaceFragment(int newPosition) {
        if (oldPosition != newPosition) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            if (homeFragment != null) transaction.hide(homeFragment);
            if (walletFragment != null) transaction.hide(walletFragment);
            if (historyFragment != null) transaction.hide(historyFragment);
            if (profileFragment != null) transaction.hide(profileFragment);
            if (newPosition == 0) {
                if (homeFragment == null) {
                    homeFragment = HomeFragment.newInstance();
                    transaction.add(R.id.content, homeFragment, GilaPrint.HomePage);
                    transaction.addToBackStack(GilaPrint.HomePage);
                }else {
                    transaction.show(homeFragment);
                }
                transaction.commit();
            } else if (newPosition == 1) {
                if (walletFragment == null) {
                    walletFragment = WalletFragment.newInstance();
                    transaction.add(R.id.content, walletFragment, GilaPrint.WalletPage);
                    transaction.addToBackStack(GilaPrint.WalletPage);
                }else {
                    transaction.show(walletFragment);
                }
                transaction.commit();
            } else if (newPosition == 2) {
                if (historyFragment == null) {
                    historyFragment = HistoryMenuFragment.newInstance();
                    transaction.add(R.id.content, historyFragment, GilaPrint.HistoryPage);
                    transaction.addToBackStack(GilaPrint.HistoryPage);
                }else {
                    transaction.show(historyFragment);
                }
                transaction.commit();
            } else if (newPosition == 3) {
                if (profileFragment == null) {
                    profileFragment = ProfileFragment.newInstance();
                    transaction.add(R.id.content, profileFragment, GilaPrint.ProfilePage);
                    transaction.addToBackStack(GilaPrint.ProfilePage);
                }else {
                    transaction.show(profileFragment);
                }
                transaction.commit();
            }
            oldPosition = newPosition;
        }
    }

    private void ServerPrice() {
        new APIRequest().ServerPriceList(new APIListener() {
            @Override
            public void onSuccess(APIResponseData response) {
                SessionApps.setPriceList(response.getPriceListModel());
            }

            @Override
            public void onFailure(API api, String errorMessage) {}
        });
    }
}
