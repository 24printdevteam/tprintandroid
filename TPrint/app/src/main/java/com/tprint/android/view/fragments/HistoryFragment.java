package com.tprint.android.view.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.tprint.android.R;
import com.tprint.android.models.HistoryModel;
import com.tprint.android.presenter.History.HistoryPresenter;
import com.tprint.android.presenter.History.HistoryViewImpl;
import com.tprint.android.presenter.HistoryAdapter;
import com.tprint.android.services.API;
import com.tprint.android.services.APIResponseData;
import com.tprint.android.view.BaseFragment;
import com.tprint.android.view.activities.HistoryDetailActivity;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import butterknife.BindView;

/**
 * Created by aboot on 5/23/17.
 */

public class HistoryFragment extends BaseFragment implements
        SwipeRefreshLayout.OnRefreshListener, HistoryAdapter.OnClickListener, HistoryViewImpl {

    private boolean isCompleted;
    private HistoryAdapter historyAdapter;
    private HistoryPresenter historyPresenter;

    @BindView(R.id.refreshLayout) SwipeRefreshLayout refreshLayout;
    @BindView(R.id.recyclerView) RecyclerView recyclerView;


    public static HistoryFragment newInstance(boolean isCompleted) {
        Bundle args = new Bundle();
        args.putBoolean("isCompleted", isCompleted);
        HistoryFragment fragment = new HistoryFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.history_list_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        isCompleted = getArguments().getBoolean("isCompleted", false);

        historyPresenter = new HistoryPresenter(this);


        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        historyAdapter = new HistoryAdapter(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(historyAdapter);
        historyAdapter.setOnClickListener(this);
        refreshLayout.setOnRefreshListener(this);

        historyPresenter.HistoryList();
    }

    @Override
    public void onRefresh() {
        historyPresenter.HistoryList();
    }

    @Override
    public void onClick(String orderID) {
        Intent intent = new Intent(getActivity(), HistoryDetailActivity.class);
        intent.putExtra("ID", orderID);
        startActivity(intent);
    }


    @Override
    public void showLoading(API api) {
        refreshLayout.setRefreshing(true);
    }

    @Override
    public void hideLoading() {
        refreshLayout.setRefreshing(false);
    }

    @Override
    public void ListSuccess(APIResponseData response) {
        refreshLayout.setRefreshing(false);
        List<HistoryModel> list = isCompleted ? response.getCompletedList() : response.getInProgressList();
        if (list != null) {
            Collections.sort(list, new Comparator<HistoryModel>() {
                @Override
                public int compare(HistoryModel o1, HistoryModel o2) {
                    return (o2.getTCreated() < o1.getTCreated()) ? -1 : ((o2.getTCreated() == o1.getTCreated()) ? 0 : 1);
                }
            });

            historyAdapter.addNewList(list);
        }
    }

    @Override
    public void DetailsSuccess(APIResponseData response) {}

    @Override
    public void RequestFailed(API api, String errorMessage) {}
}
