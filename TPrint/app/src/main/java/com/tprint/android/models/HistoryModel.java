package com.tprint.android.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by aboot on 7/15/17.
 */

public class HistoryModel implements Parcelable {

    private String ID;
    private String OutletID;
    private String PrinterID;
    private String KioskID;
    private String InquiryID;
    private String Username;
    private int TotalPages;
    private String Files;
    private String Pages;
    private String PageFroms;
    private String PageTos;
    private String Duplex;
    private String Quarter;
    private String Half;
    private String Full;
    private String Type;
    private String Copies;
    private String Prices;
    private long PrintReceipt;
    private long SubTotal;
    private long Receipt;
    private long Delivery;
    private long Discount;
    private long Total;
    private long TotalSystem;
    private String PaymentType;
    private String PaymentMethod;
    private String PaymentStatus;
    private String Status;
    private long Expired;
    private long TCreated;
    private long TSend;
    private long TReceived;
    private long TProcessed;
    private long TCompleted;
    private String Location;

    public HistoryModel() {
    }

    protected HistoryModel(Parcel in) {
        ID = in.readString();
        OutletID = in.readString();
        PrinterID = in.readString();
        KioskID = in.readString();
        InquiryID = in.readString();
        Username = in.readString();
        TotalPages = in.readInt();
        Files = in.readString();
        Pages = in.readString();
        PageFroms = in.readString();
        PageTos = in.readString();
        Duplex = in.readString();
        Quarter = in.readString();
        Half = in.readString();
        Full = in.readString();
        Type = in.readString();
        Copies = in.readString();
        Prices = in.readString();
        PrintReceipt = in.readLong();
        SubTotal = in.readLong();
        Receipt = in.readLong();
        Delivery = in.readLong();
        Discount = in.readLong();
        Total = in.readLong();
        TotalSystem = in.readLong();
        PaymentType = in.readString();
        PaymentMethod = in.readString();
        PaymentStatus = in.readString();
        Status = in.readString();
        Expired = in.readLong();
        TCreated = in.readLong();
        TSend = in.readLong();
        TReceived = in.readLong();
        TProcessed = in.readLong();
        TCompleted = in.readLong();
        Location = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(ID);
        dest.writeString(OutletID);
        dest.writeString(PrinterID);
        dest.writeString(KioskID);
        dest.writeString(InquiryID);
        dest.writeString(Username);
        dest.writeInt(TotalPages);
        dest.writeString(Files);
        dest.writeString(Pages);
        dest.writeString(PageFroms);
        dest.writeString(PageTos);
        dest.writeString(Duplex);
        dest.writeString(Quarter);
        dest.writeString(Half);
        dest.writeString(Full);
        dest.writeString(Type);
        dest.writeString(Copies);
        dest.writeString(Prices);
        dest.writeLong(PrintReceipt);
        dest.writeLong(SubTotal);
        dest.writeLong(Receipt);
        dest.writeLong(Delivery);
        dest.writeLong(Discount);
        dest.writeLong(Total);
        dest.writeLong(TotalSystem);
        dest.writeString(PaymentType);
        dest.writeString(PaymentMethod);
        dest.writeString(PaymentStatus);
        dest.writeString(Status);
        dest.writeLong(Expired);
        dest.writeLong(TCreated);
        dest.writeLong(TSend);
        dest.writeLong(TReceived);
        dest.writeLong(TProcessed);
        dest.writeLong(TCompleted);
        dest.writeString(Location);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<HistoryModel> CREATOR = new Creator<HistoryModel>() {
        @Override
        public HistoryModel createFromParcel(Parcel in) {
            return new HistoryModel(in);
        }

        @Override
        public HistoryModel[] newArray(int size) {
            return new HistoryModel[size];
        }
    };

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getOutletID() {
        return OutletID;
    }

    public void setOutletID(String outletID) {
        OutletID = outletID;
    }

    public String getPrinterID() {
        return PrinterID;
    }

    public void setPrinterID(String printerID) {
        PrinterID = printerID;
    }

    public String getKioskID() {
        return KioskID;
    }

    public void setKioskID(String kioskID) {
        KioskID = kioskID;
    }

    public String getInquiryID() {
        return InquiryID;
    }

    public void setInquiryID(String inquiryID) {
        InquiryID = inquiryID;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String username) {
        Username = username;
    }

    public int getTotalPages() {
        return TotalPages;
    }

    public void setTotalPages(int totalPages) {
        TotalPages = totalPages;
    }

    public String getFiles() {
        return Files;
    }

    public void setFiles(String files) {
        Files = files;
    }

    public String getPages() {
        return Pages;
    }

    public void setPages(String pages) {
        Pages = pages;
    }

    public String getPageFroms() {
        return PageFroms;
    }

    public void setPageFroms(String pageFroms) {
        PageFroms = pageFroms;
    }

    public String getPageTos() {
        return PageTos;
    }

    public void setPageTos(String pageTos) {
        PageTos = pageTos;
    }

    public String getDuplex() {
        return Duplex;
    }

    public void setDuplex(String duplex) {
        Duplex = duplex;
    }

    public String getQuarter() {
        return Quarter;
    }

    public void setQuarter(String quarter) {
        Quarter = quarter;
    }

    public String getHalf() {
        return Half;
    }

    public void setHalf(String half) {
        Half = half;
    }

    public String getFull() {
        return Full;
    }

    public void setFull(String full) {
        Full = full;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getCopies() {
        return Copies;
    }

    public void setCopies(String copies) {
        Copies = copies;
    }

    public String getPrices() {
        return Prices;
    }

    public void setPrices(String prices) {
        Prices = prices;
    }

    public long getPrintReceipt() {
        return PrintReceipt;
    }

    public void setPrintReceipt(long printReceipt) {
        PrintReceipt = printReceipt;
    }

    public long getSubTotal() {
        return SubTotal;
    }

    public void setSubTotal(long subTotal) {
        SubTotal = subTotal;
    }

    public long getReceipt() {
        return Receipt;
    }

    public void setReceipt(long receipt) {
        Receipt = receipt;
    }

    public long getDelivery() {
        return Delivery;
    }

    public void setDelivery(long delivery) {
        Delivery = delivery;
    }

    public long getDiscount() {
        return Discount;
    }

    public void setDiscount(long discount) {
        Discount = discount;
    }

    public long getTotal() {
        return Total;
    }

    public void setTotal(long total) {
        Total = total;
    }

    public long getTotalSystem() {
        return TotalSystem;
    }

    public void setTotalSystem(long totalSystem) {
        TotalSystem = totalSystem;
    }

    public String getPaymentType() {
        return PaymentType;
    }

    public void setPaymentType(String paymentType) {
        PaymentType = paymentType;
    }

    public String getPaymentMethod() {
        return PaymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        PaymentMethod = paymentMethod;
    }

    public String getPaymentStatus() {
        return PaymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        PaymentStatus = paymentStatus;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public long getExpired() {
        return Expired;
    }

    public void setExpired(long expired) {
        Expired = expired;
    }

    public long getTCreated() {
        return TCreated;
    }

    public void setTCreated(long TCreated) {
        this.TCreated = TCreated;
    }

    public long getTSend() {
        return TSend;
    }

    public void setTSend(long TSend) {
        this.TSend = TSend;
    }

    public long getTReceived() {
        return TReceived;
    }

    public void setTReceived(long TReceived) {
        this.TReceived = TReceived;
    }

    public long getTProcessed() {
        return TProcessed;
    }

    public void setTProcessed(long TProcessed) {
        this.TProcessed = TProcessed;
    }

    public long getTCompleted() {
        return TCompleted;
    }

    public void setTCompleted(long TCompleted) {
        this.TCompleted = TCompleted;
    }

    public String getLocation() {
        return Location;
    }

    public void setLocation(String location) {
        Location = location;
    }
}
