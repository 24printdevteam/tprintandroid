package com.tprint.android.database;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tprint.android.etc.DataUtils;
import com.tprint.android.models.PriceListModel;
import com.tprint.android.models.ProfileModel;

import java.io.File;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by aboot on 9/13/16.
 */
public class SessionApps {

    private static SharedPreferences preferences;
    private static SharedPreferences.Editor editor;

    public static void initialize(Context context) {
        preferences = context.getSharedPreferences("24Print_Session", Context.MODE_PRIVATE);
    }

    @SuppressLint("CommitPrefEdits")
    public static void setLogin(String Token, String loginStatus) {
        editor = preferences.edit();
        editor.putString("TokenApps", DataUtils.isNotEmpty(Token) ? Token : "");
        editor.putString("loginStatus", DataUtils.isNotEmpty(loginStatus) ? loginStatus : "");
        editor.apply();
    }

    public static String getTokenApps() {
        return preferences.getString("TokenApps", "");
    }


    // Profile
    public static void setProfile(ProfileModel profileModel) {
        editor = preferences.edit();
        editor.putString("profileModel", new Gson().toJson(profileModel));
        editor.apply();
    }

    public static ProfileModel getProfile() {
        return new Gson().fromJson(preferences.getString("profileModel", ""),
                ProfileModel.class);
    }

    public static String getStatus() {
        return preferences.getString("Status", null);
    }


    //Activation
    public static void setActivationTime() {
        editor = preferences.edit();
        editor.putLong("activationTime", System.currentTimeMillis() + + 180000L);
        editor.apply();
    }

    public static Long getActivationTime() {
        return preferences.getLong("activationTime", 0);
    }


    //Forgot
    public static void setForgot(boolean isEmail, String EmailPhone) {
        editor = preferences.edit();
        editor.putBoolean("isEmail", isEmail);
        editor.putString("EmailPhone", EmailPhone);
        editor.apply();
    }

    public static boolean getForgotIsEmail() {
        return preferences.getBoolean("isEmail", false);
    }

    public static String getForgotEmailPhone() {
        return preferences.getString("EmailPhone", null);
    }


    // Notification
    public static void setTokenFCM(String TokenFCM) {
        editor = preferences.edit();
        editor.putString("TokenFCM", TokenFCM);
        editor.apply();
    }

    public static String getTokenFCM() {
        return preferences.getString("TokenFCM", null);
    }

    // PriceList
    public static void setPriceList(PriceListModel model) {
        editor = preferences.edit();
        editor.putString("priceList", new Gson().toJson(model));
        editor.apply();
    }

    public static PriceListModel getPriceList() {
        return new Gson().fromJson(preferences.getString("priceList", ""),
                PriceListModel.class);
    }


    //Upload
    public static void setUploadStatus(int Status){
        editor = preferences.edit();
        editor.putInt("uploadStatus", Status);
        editor.apply();
    }

    public static void setUploadList(List<File> list) {
        Type listType = new TypeToken<List<File>>() {}.getType();
        editor = preferences.edit();
        editor.putString("uploadList", new Gson().toJson(list, listType));
        editor.apply();
    }

    public static boolean setUpload(File file) {
        List<File> fileList = getUploadList();
        if (fileList == null) {
            fileList = new ArrayList<>();
        }
        if (file.exists()) {
            fileList.add(file);
            Type listType = new TypeToken<List<File>>() {}.getType();
            editor = preferences.edit();
            editor.putString("uploadList", new Gson().toJson(fileList, listType));
            editor.apply();
            return true;
        }else {
            return false;
        }
    }

    public static List<File> getUploadList() {
        Type listType = new TypeToken<List<File>>() {}.getType();
        return new Gson().fromJson(preferences.getString("uploadList", ""), listType);
    }

    public static int getUploadStatus() {
        return preferences.getInt("uploadStatus", 0);
    }


    public static void clearToken() {
        editor = preferences.edit();
        editor.remove("TokenApps");
        editor.remove("loginStatus");
        editor.remove("profileModel");
        editor.apply();
    }
}
