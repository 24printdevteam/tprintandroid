package com.tprint.android.view.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.tprint.android.R;
import com.tprint.android.etc.DataUtils;
import com.tprint.android.etc.GilaPrint;
import com.tprint.android.presenter.Credential.CredentialPageImpl;
import com.tprint.android.presenter.Credential.CredentialPresenter;
import com.tprint.android.presenter.Credential.CredentialViewImpl;
import com.tprint.android.services.API;
import com.tprint.android.services.APIResponseData;
import com.tprint.android.view.BaseFragment;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by aboot on 7/4/17.
 */

public class SetPasswordFragment extends BaseFragment implements CredentialViewImpl {

    private CredentialPageImpl credentialPage;
    private CredentialPresenter credentialPresenter;

    @BindView(R.id.inputNewPassword) EditText inputNewPassword;
    @BindView(R.id.inputConfirmationPassword) EditText inputConfirmationPassword;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (!(context instanceof CredentialPageImpl)) {
            throw new ClassCastException("Activity must implement CredentialPageImpl");
        }
        this.credentialPage = (CredentialPageImpl) context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.set_password_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getToolbar().setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        setToolbarTitle(GilaPrint.SetPasswordPage);

        credentialPresenter = new CredentialPresenter(this);

    }

    @OnClick(R.id.btnSave) void onSave() {
        if (!DataUtils.isNotEmpty(inputNewPassword)) {
            Toast.makeText(getActivity(), R.string.validate_password_insert, Toast.LENGTH_SHORT).show();
            return;
        }else {
            if (inputNewPassword.getText().toString().length() < 6 &&
                    !DataUtils.isNumberAndAlphabet(inputNewPassword.getText().toString())) {
                Toast.makeText(getActivity(), R.string.validate_password_valid, Toast.LENGTH_SHORT).show();
                return;
            }
        }

        if (!DataUtils.isNotEmpty(inputConfirmationPassword)) {
            if (!inputNewPassword.getText().toString().equals(inputConfirmationPassword.getText().toString())) {
                Toast.makeText(getActivity(), R.string.validate_password_confirm, Toast.LENGTH_SHORT).show();
                return;
            }
        }

        credentialPresenter.MeResetPassword(inputNewPassword.getText().toString());
    }

    @Override
    public void showLoading(API api) {
        showLoadingDialog(api);
    }

    @Override
    public void hideLoading() {
        dismissLoadingDialog();
    }

    @Override
    public void LoginSuccess(APIResponseData response) {}

    @Override
    public void RegisterSuccess(APIResponseData response) {}

    @Override
    public void ProfileSuccess(String ChangePage) {
        credentialPage.ChangePage(ChangePage);
    }

    @Override
    public void VerificationSuccess() {}

    @Override
    public void ResendVerificationSuccess(String Message) {}

    @Override
    public void ForgotPasswordSuccess() {}

    @Override
    public void ResetPasswordCodeSuccess() {}

    @Override
    public void ResetPasswordSuccess() {
        credentialPresenter.MeProfile();
    }

    @Override
    public void ChangePhoneNumber() {}

    @Override
    public void RequestFailed(API api, String errorMessage) {
        Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT).show();
    }
}
