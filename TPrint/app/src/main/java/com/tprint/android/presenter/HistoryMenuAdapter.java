package com.tprint.android.presenter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.tprint.android.view.fragments.HistoryFragment;


/**
 * Created by aboot on 5/23/17.
 */

public class HistoryMenuAdapter extends FragmentStatePagerAdapter {


    public HistoryMenuAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return HistoryFragment.newInstance(false);
            default:
                return HistoryFragment.newInstance(true);
        }
    }

    @Override
    public int getCount() {
        return 2;
    }
}
