package com.tprint.android.etc;

import android.content.Context;
import android.graphics.Typeface;
import com.norbsoft.typefacehelper.TypefaceCollection;
import com.norbsoft.typefacehelper.TypefaceHelper;
import com.tprint.android.models.LocationModel;
import java.security.MessageDigest;
import java.util.Calendar;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;

/**
 * Created by aboot on 4/3/17.
 */
public class Tools {

    public static void initialize(Context context) {
        TypefaceCollection typeface = new TypefaceCollection.Builder()
                .set(Typeface.NORMAL, Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Regular.ttf"))
                .set(Typeface.BOLD, Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Bold.ttf"))
                .set(Typeface.BOLD_ITALIC, Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Medium.ttf"))
                .create();
        TypefaceHelper.init(typeface);

    }

    public static String getBaseUrl() {
//        return "https://printgila.com/";
        return "https://tprint.id/";
    }

    public static String getPreviewUrl() {
//        return "https://printgila.com/";
        return "https://tprint.id/mobilepreview/";
    }

    public static OkHttpClient.Builder getHttpClient() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.readTimeout(5, TimeUnit.MINUTES);
        httpClient.writeTimeout(5, TimeUnit.MINUTES);
        httpClient.connectTimeout(5, TimeUnit.MINUTES);
        httpClient.addInterceptor(logging);
        return httpClient;
    }

    public static String sha1(String s) {
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-1");
            messageDigest.update(s.getBytes("UTF-8"));
            byte[] bytes = messageDigest.digest();
            StringBuilder buffer = new StringBuilder();
            for (byte b : bytes) {
                buffer.append(Integer.toString((b & 0xff) + 0x100, 16).substring(1));
            }
            return buffer.toString();
        }
        catch (Exception ignored) {
            ignored.printStackTrace();
            return null;
        }
    }

    public static String createDocumentId() {
        String ts = String.valueOf(System.currentTimeMillis());
        String rand = UUID.randomUUID().toString();
        return sha1(ts + rand);
    }

    public static boolean isValidFile(String mimeType) {
        switch (mimeType) {
            case GilaPrint.mime_pdf:
            case GilaPrint.mime_msword:
            case GilaPrint.mime_msexcel:
            case GilaPrint.mime_mspowerpoint:
            case GilaPrint.mime_google_document:
            case GilaPrint.mime_google_spreadsheet:
            case GilaPrint.mime_google_presentation:
                return true;
            default:
                return false;
        }
    }

    public static boolean isValidExt(String fileName) {
        return fileName.endsWith(".pdf") ||
                fileName.endsWith(".doc") ||
                fileName.endsWith(".docx") ||
                fileName.endsWith(".ppt") ||
                fileName.endsWith(".pptx") ||
                fileName.endsWith(".xls") ||
                fileName.endsWith(".xlsx");
    }

    public static boolean isValidNumber(String Number) {
        return Number.startsWith("0811") || Number.startsWith("0812") || Number.startsWith("0813") ||
                Number.startsWith("0814") || Number.startsWith("0815") || Number.startsWith("0816") ||
                Number.startsWith("0817") || Number.startsWith("0818") || Number.startsWith("0819") ||
                Number.startsWith("0821") || Number.startsWith("0822") || Number.startsWith("0823") ||
                Number.startsWith("0831") || Number.startsWith("0832") || Number.startsWith("0838") ||
                Number.startsWith("0852") || Number.startsWith("0853") || Number.startsWith("0855") ||
                Number.startsWith("0856") || Number.startsWith("0857") || Number.startsWith("0858") ||
                Number.startsWith("0859") || Number.startsWith("0877") || Number.startsWith("0878") ||
                Number.startsWith("0895") || Number.startsWith("0896") || Number.startsWith("0897") ||
                Number.startsWith("0898") || Number.startsWith("0899") || Number.startsWith("0881") ||
                Number.startsWith("0882") || Number.startsWith("0887") || Number.startsWith("0888") ||
                Number.startsWith("0828");
    }

    public static String getOpenClose(LocationModel model) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(System.currentTimeMillis());
        cal.get(Calendar.HOUR_OF_DAY);
        int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
        switch (dayOfWeek) {
            case Calendar.SUNDAY:
                return checkOpenClose(model.getSunday().getOpen(), model.getSunday().getClose());
            case Calendar.MONDAY:
                return checkOpenClose(model.getMonday().getOpen(), model.getMonday().getClose());
            case Calendar.TUESDAY:
                return checkOpenClose(model.getTuesday().getOpen(), model.getTuesday().getClose());
            case Calendar.WEDNESDAY:
                return checkOpenClose(model.getWednesday().getOpen(), model.getWednesday().getClose());
            case Calendar.THURSDAY:
                return checkOpenClose(model.getThursday().getOpen(), model.getThursday().getClose());
            case Calendar.FRIDAY:
                return checkOpenClose(model.getFriday().getOpen(), model.getFriday().getClose());
            case Calendar.SATURDAY:
                return checkOpenClose(model.getSaturday().getOpen(), model.getSaturday().getClose());
            default:
                return "";
        }
    }

    private static String checkOpenClose(String Open, String Close) {
        String textHours;
        if (!DataUtils.isNotEmpty(Open) || !DataUtils.isNotEmpty(Close)) {
            return "Hari ini tutup";
        }
        String[] openTime = Open.split(":");
        Calendar openCal = Calendar.getInstance();
        openCal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(openTime[0]));
        openCal.set(Calendar.MINUTE, Integer.parseInt(openTime[1]));

        String[] closeTime = Open.split(":");
        Calendar closeCal = Calendar.getInstance();
        closeCal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(closeTime[0]));
        closeCal.set(Calendar.MINUTE, Integer.parseInt(closeTime[1]));

        Calendar currentCal = Calendar.getInstance();
        if (Open.equals(Close)) {
            textHours = "Buka 24 Jam";
        }else {
            if (currentCal.getTimeInMillis() > openCal.getTimeInMillis() &&
                    currentCal.getTimeInMillis() < closeCal.getTimeInMillis()) {
                textHours = "Buka "+Open + " sampai " + Close;
            }else {
                if (currentCal.getTimeInMillis() < openCal.getTimeInMillis()) {
                    textHours = "Buka jam "+Open;
                } else if (currentCal.getTimeInMillis() > openCal.getTimeInMillis()) {
                    textHours = "Tutup jam "+Close;
                } else {
                    textHours = "-";
                }
            }
        }
        return textHours;
    }

    public static String getDay(int now) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(System.currentTimeMillis());
        cal.add(Calendar.DAY_OF_WEEK, now);
        int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
        switch (dayOfWeek) {
            case Calendar.SUNDAY:
                return "Minggu";
            case Calendar.MONDAY:
                return "Senin";
            case Calendar.TUESDAY:
                return "Selasa";
            case Calendar.WEDNESDAY:
                return "Rabu";
            case Calendar.THURSDAY:
                return "Kamis";
            case Calendar.FRIDAY:
                return "Jumat";
            case Calendar.SATURDAY:
                return "Sabtu";
            default:
                return "";
        }
    }

    public static String getHours(LocationModel model, int now) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(System.currentTimeMillis());
        cal.add(Calendar.DAY_OF_WEEK, now);
        int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
        switch (dayOfWeek) {
            case Calendar.SUNDAY:
                if (DataUtils.isNotEmpty(model.getSunday().getOpen()) &&
                        DataUtils.isNotEmpty(model.getSunday().getOpen())) {
                    return model.getSunday().getOpen()+" - "+model.getSunday().getClose();
                }else {
                    return "Tutup";
                }
            case Calendar.MONDAY:
                if (DataUtils.isNotEmpty(model.getMonday().getOpen()) &&
                        DataUtils.isNotEmpty(model.getMonday().getOpen())) {
                    return model.getMonday().getOpen()+" - "+model.getMonday().getClose();
                }else {
                    return "Tutup";
                }
            case Calendar.TUESDAY:
                if (DataUtils.isNotEmpty(model.getTuesday().getOpen()) &&
                        DataUtils.isNotEmpty(model.getTuesday().getOpen())) {
                    return model.getTuesday().getOpen()+" - "+model.getTuesday().getClose();
                }else {
                    return "Tutup";
                }
            case Calendar.WEDNESDAY:
                if (DataUtils.isNotEmpty(model.getWednesday().getOpen()) &&
                        DataUtils.isNotEmpty(model.getWednesday().getOpen())) {
                    return model.getWednesday().getOpen()+" - "+model.getWednesday().getClose();
                }else {
                    return "Tutup";
                }
            case Calendar.THURSDAY:
                if (DataUtils.isNotEmpty(model.getThursday().getOpen()) &&
                        DataUtils.isNotEmpty(model.getThursday().getOpen())) {
                    return model.getThursday().getOpen()+" - "+model.getThursday().getClose();
                }else {
                    return "Tutup";
                }
            case Calendar.FRIDAY:
                if (DataUtils.isNotEmpty(model.getFriday().getOpen()) &&
                        DataUtils.isNotEmpty(model.getFriday().getOpen())) {
                    return model.getFriday().getOpen()+" - "+model.getFriday().getClose();
                }else {
                    return "Tutup";
                }
            case Calendar.SATURDAY:
                if (DataUtils.isNotEmpty(model.getSaturday().getOpen()) &&
                        DataUtils.isNotEmpty(model.getSaturday().getOpen())) {
                    return model.getSaturday().getOpen()+" - "+model.getSaturday().getClose();
                }else {
                    return "Tutup";
                }
            default:
                return "";
        }
    }
}
