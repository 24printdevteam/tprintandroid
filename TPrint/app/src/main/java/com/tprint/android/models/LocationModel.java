package com.tprint.android.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by aboot on 7/14/17.
 */

public class LocationModel implements Parcelable {

    private String Name;
    private String Details;
    private String Address;
    private double Latitude;
    private double Longitude;
    private DayModel Monday;
    private DayModel Tuesday;
    private DayModel Wednesday;
    private DayModel Thursday;
    private DayModel Friday;
    private DayModel Saturday;
    private DayModel Sunday;


    protected LocationModel(Parcel in) {
        Name = in.readString();
        Details = in.readString();
        Address = in.readString();
        Latitude = in.readDouble();
        Longitude = in.readDouble();
        Monday = in.readParcelable(DayModel.class.getClassLoader());
        Tuesday = in.readParcelable(DayModel.class.getClassLoader());
        Wednesday = in.readParcelable(DayModel.class.getClassLoader());
        Thursday = in.readParcelable(DayModel.class.getClassLoader());
        Friday = in.readParcelable(DayModel.class.getClassLoader());
        Saturday = in.readParcelable(DayModel.class.getClassLoader());
        Sunday = in.readParcelable(DayModel.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(Name);
        dest.writeString(Details);
        dest.writeString(Address);
        dest.writeDouble(Latitude);
        dest.writeDouble(Longitude);
        dest.writeParcelable(Monday, flags);
        dest.writeParcelable(Tuesday, flags);
        dest.writeParcelable(Wednesday, flags);
        dest.writeParcelable(Thursday, flags);
        dest.writeParcelable(Friday, flags);
        dest.writeParcelable(Saturday, flags);
        dest.writeParcelable(Sunday, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<LocationModel> CREATOR = new Creator<LocationModel>() {
        @Override
        public LocationModel createFromParcel(Parcel in) {
            return new LocationModel(in);
        }

        @Override
        public LocationModel[] newArray(int size) {
            return new LocationModel[size];
        }
    };

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getDetails() {
        return Details;
    }

    public void setDetails(String details) {
        Details = details;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public double getLatitude() {
        return Latitude;
    }

    public void setLatitude(double latitude) {
        Latitude = latitude;
    }

    public double getLongitude() {
        return Longitude;
    }

    public void setLongitude(double longitude) {
        Longitude = longitude;
    }

    public DayModel getMonday() {
        return Monday;
    }

    public void setMonday(DayModel monday) {
        Monday = monday;
    }

    public DayModel getTuesday() {
        return Tuesday;
    }

    public void setTuesday(DayModel tuesday) {
        Tuesday = tuesday;
    }

    public DayModel getWednesday() {
        return Wednesday;
    }

    public void setWednesday(DayModel wednesday) {
        Wednesday = wednesday;
    }

    public DayModel getThursday() {
        return Thursday;
    }

    public void setThursday(DayModel thursday) {
        Thursday = thursday;
    }

    public DayModel getFriday() {
        return Friday;
    }

    public void setFriday(DayModel friday) {
        Friday = friday;
    }

    public DayModel getSaturday() {
        return Saturday;
    }

    public void setSaturday(DayModel saturday) {
        Saturday = saturday;
    }

    public DayModel getSunday() {
        return Sunday;
    }

    public void setSunday(DayModel sunday) {
        Sunday = sunday;
    }
}
