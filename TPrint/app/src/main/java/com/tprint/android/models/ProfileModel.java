package com.tprint.android.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by aboot on 6/28/17.
 */

public class ProfileModel implements Parcelable {

    private String Username;
    private String Name;
    private String Email;
    private String Phone;
    private String Status;
    private String StorageUsed;
    private String StorageCapacity;
    private long Balance;
    private String Created;
    private String CreatedAt;

    public ProfileModel() {}

    public ProfileModel(String status) {
        Status = status;
    }

    protected ProfileModel(Parcel in) {
        Username = in.readString();
        Name = in.readString();
        Email = in.readString();
        Phone = in.readString();
        Status = in.readString();
        StorageUsed = in.readString();
        StorageCapacity = in.readString();
        Balance = in.readLong();
        Created = in.readString();
        CreatedAt = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(Username);
        dest.writeString(Name);
        dest.writeString(Email);
        dest.writeString(Phone);
        dest.writeString(Status);
        dest.writeString(StorageUsed);
        dest.writeString(StorageCapacity);
        dest.writeLong(Balance);
        dest.writeString(Created);
        dest.writeString(CreatedAt);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ProfileModel> CREATOR = new Creator<ProfileModel>() {
        @Override
        public ProfileModel createFromParcel(Parcel in) {
            return new ProfileModel(in);
        }

        @Override
        public ProfileModel[] newArray(int size) {
            return new ProfileModel[size];
        }
    };

    public String getUsername() {
        return Username;
    }

    public void setUsername(String username) {
        Username = username;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getStorageUsed() {
        return StorageUsed;
    }

    public void setStorageUsed(String storageUsed) {
        StorageUsed = storageUsed;
    }

    public String getStorageCapacity() {
        return StorageCapacity;
    }

    public void setStorageCapacity(String storageCapacity) {
        StorageCapacity = storageCapacity;
    }

    public long getBalance() {
        return Balance;
    }

    public void setBalance(long balance) {
        Balance = balance;
    }

    public String getCreated() {
        return Created;
    }

    public void setCreated(String created) {
        Created = created;
    }

    public String getCreatedAt() {
        return CreatedAt;
    }

    public void setCreatedAt(String createdAt) {
        CreatedAt = createdAt;
    }
}
