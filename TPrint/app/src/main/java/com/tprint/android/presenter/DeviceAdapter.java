package com.tprint.android.presenter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.tprint.android.R;
import com.tprint.android.etc.DataUtils;
import com.tprint.android.etc.Tools;
import com.tprint.android.view.BaseViewHolder;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by aboot on 6/7/17.
 */

public class DeviceAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener {

    List<File> list = new ArrayList<>();
    OnDeviceSelectedListener onDeviceSelectedListener;

    public void addNewList(List<File> list) {
        this.list.clear();
        if (list != null) {
            this.list.addAll(list);
        }
        notifyDataSetChanged();
    }

    public void setOnDeviceSelectedListener(OnDeviceSelectedListener onDeviceSelectedListener) {
        this.onDeviceSelectedListener = onDeviceSelectedListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == 0) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.device_file_viewholder, parent, false);
            return new FileHolder(view);
        }else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.device_folder_viewholder, parent, false);
            return new FolderHolder(view);
        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        File file = list.get(position);
        if (holder instanceof FolderHolder) {
            FolderHolder folderHolder = (FolderHolder) holder;
            folderHolder.textFolderName.setText(file.getName());
            folderHolder.layFolder.setTag(file);
            folderHolder.layFolder.setOnClickListener(this);
        }else if (holder instanceof FileHolder) {
            FileHolder fileHolder = (FileHolder) holder;
            fileHolder.textFileName.setText(file.getName());
            fileHolder.textProperties.setText(""+ DataUtils.getDateServer(file.lastModified())+"     "+
                    DataUtils.getFileSize(file.length()));
            fileHolder.imageFile.setAlpha(Tools.isValidExt(file.getName()) ? 1f : 0.5f);
            fileHolder.textFileName.setAlpha(Tools.isValidExt(file.getName()) ? 1f : 0.5f);
            fileHolder.textProperties.setAlpha(Tools.isValidExt(file.getName()) ? 1f : 0.5f);
            fileHolder.layFile.setTag(file);
            fileHolder.layFile.setOnClickListener(Tools.isValidExt(file.getName()) ? this : null);
        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public int getItemViewType(int position) {
        return list.get(position).isDirectory() ? 1 : 0;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.layFile:
                onDeviceSelectedListener.onFile((File) v.getTag());
                break;
            case R.id.layFolder:
                onDeviceSelectedListener.onFolder((File) v.getTag());
                break;
        }
    }

    class FolderHolder extends BaseViewHolder {

        @BindView(R.id.layFolder) FrameLayout layFolder;
        @BindView(R.id.textFolderName) TextView textFolderName;

        public FolderHolder(View itemView) {
            super(itemView);
        }
    }

    class FileHolder extends BaseViewHolder {

        @BindView(R.id.layFile) FrameLayout layFile;
        @BindView(R.id.imageFile) ImageView imageFile;
        @BindView(R.id.textFileName) TextView textFileName;
        @BindView(R.id.textProperties) TextView textProperties;

        public FileHolder(View itemView) {
            super(itemView);
        }
    }

    public interface OnDeviceSelectedListener {
        void onFile(File file);
        void onFolder(File path);
    }
}
