package com.tprint.android.services;


import com.tprint.android.models.BankModel;
import com.tprint.android.models.CheckOutModel;
import com.tprint.android.models.HistoryModel;
import com.tprint.android.models.LocationModel;
import com.tprint.android.models.PriceListModel;
import com.tprint.android.models.PrintModel;
import com.tprint.android.models.ProfileModel;
import com.tprint.android.models.TopUpModel;
import com.tprint.android.models.WalletModel;

import java.util.List;

/**
 * Created by aboot on 6/28/17.
 */
public class APIResponseData {

    private String Status;
    private int Code;
    private String Message;
    private String Token;
    private String loginStatus;
    private String previewUrl;

    private ProfileModel profileModel;
    private PriceListModel priceListModel;
    private CheckOutModel checkOutModel;
    private HistoryModel historyModel;

    private List<PrintModel> printList;
    private List<LocationModel> locationList;
    private List<HistoryModel> inProgressList;
    private List<HistoryModel> completedList;
    private List<WalletModel> walletList;
    private List<TopUpModel> topUpList;
    private List<BankModel> bankList;


    APIResponseData() {}

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public int getCode() {
        return Code;
    }

    public void setCode(int code) {
        Code = code;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getToken() {
        return Token;
    }

    public void setToken(String token) {
        Token = token;
    }

    public String getLoginStatus() {
        return loginStatus;
    }

    public void setLoginStatus(String loginStatus) {
        this.loginStatus = loginStatus;
    }

    public String getPreviewUrl() {
        return previewUrl;
    }

    public void setPreviewUrl(String previewUrl) {
        this.previewUrl = previewUrl;
    }

    public ProfileModel getProfileModel() {
        return profileModel;
    }

    void setProfileModel(ProfileModel profileModel) {
        this.profileModel = profileModel;
    }

    public PriceListModel getPriceListModel() {
        return priceListModel;
    }

    public void setPriceListModel(PriceListModel priceListModel) {
        this.priceListModel = priceListModel;
    }

    public CheckOutModel getCheckOutModel() {
        return checkOutModel;
    }

    public void setCheckOutModel(CheckOutModel checkOutModel) {
        this.checkOutModel = checkOutModel;
    }

    public HistoryModel getHistoryModel() {
        return historyModel;
    }

    public void setHistoryModel(HistoryModel historyModel) {
        this.historyModel = historyModel;
    }

    public List<PrintModel> getPrintList() {
        return printList;
    }

    public void setPrintList(List<PrintModel> printList) {
        this.printList = printList;
    }

    public List<LocationModel> getLocationList() {
        return locationList;
    }

    public void setLocationList(List<LocationModel> locationList) {
        this.locationList = locationList;
    }

    public List<HistoryModel> getInProgressList() {
        return inProgressList;
    }

    public void setInProgressList(List<HistoryModel> inProgressList) {
        this.inProgressList = inProgressList;
    }

    public List<HistoryModel> getCompletedList() {
        return completedList;
    }

    public void setCompletedList(List<HistoryModel> completedList) {
        this.completedList = completedList;
    }

    public List<WalletModel> getWalletList() {
        return walletList;
    }

    public void setWalletList(List<WalletModel> walletList) {
        this.walletList = walletList;
    }

    public List<TopUpModel> getTopUpList() {
        return topUpList;
    }

    public void setTopUpList(List<TopUpModel> topUpList) {
        this.topUpList = topUpList;
    }

    public List<BankModel> getBankList() {
        return bankList;
    }

    public void setBankList(List<BankModel> bankList) {
        this.bankList = bankList;
    }
}
