package com.tprint.android.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by aboot on 7/17/17.
 */

public class TopUpModel implements Parcelable {

    private String Type;
    private long Amount;
    private float AmountSystem;
    private long Expired;

    protected TopUpModel(Parcel in) {
        Type = in.readString();
        Amount = in.readLong();
        AmountSystem = in.readFloat();
        Expired = in.readLong();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(Type);
        dest.writeLong(Amount);
        dest.writeFloat(AmountSystem);
        dest.writeLong(Expired);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<TopUpModel> CREATOR = new Creator<TopUpModel>() {
        @Override
        public TopUpModel createFromParcel(Parcel in) {
            return new TopUpModel(in);
        }

        @Override
        public TopUpModel[] newArray(int size) {
            return new TopUpModel[size];
        }
    };

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public long getAmount() {
        return Amount;
    }

    public void setAmount(long amount) {
        Amount = amount;
    }

    public float getAmountSystem() {
        return AmountSystem;
    }

    public void setAmountSystem(float amountSystem) {
        AmountSystem = amountSystem;
    }

    public long getExpired() {
        return Expired;
    }

    public void setExpired(long expired) {
        Expired = expired;
    }
}
