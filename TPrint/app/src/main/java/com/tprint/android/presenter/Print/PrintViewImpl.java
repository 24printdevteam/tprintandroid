package com.tprint.android.presenter.Print;


import com.tprint.android.services.API;
import com.tprint.android.services.APIResponseData;

/**
 * Created by aboot on 7/5/17.
 */

public interface PrintViewImpl {

    void showLoading(API api);
    void hideLoading();
    void UploadSuccess();
    void ListSuccess(APIResponseData response);
    void SettingSuccess();
    void PreviewSuccess(String Url);
    void RemoveSuccess();
    void RefreshAllSuccess();
    void RemoveAllSuccess();
    void ConfirmationSuccess();
    void CheckOutSuccess(APIResponseData response);
    void RequestFailed(API api, String errorMessage);
}
