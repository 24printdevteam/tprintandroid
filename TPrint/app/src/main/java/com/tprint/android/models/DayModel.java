package com.tprint.android.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by aboot on 7/14/17.
 */

public class DayModel implements Parcelable {

    private String Open;
    private String Close;

    protected DayModel(Parcel in) {
        Open = in.readString();
        Close = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(Open);
        dest.writeString(Close);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<DayModel> CREATOR = new Creator<DayModel>() {
        @Override
        public DayModel createFromParcel(Parcel in) {
            return new DayModel(in);
        }

        @Override
        public DayModel[] newArray(int size) {
            return new DayModel[size];
        }
    };

    public String getOpen() {
        return Open;
    }

    public void setOpen(String open) {
        Open = open;
    }

    public String getClose() {
        return Close;
    }

    public void setClose(String close) {
        Close = close;
    }
}
