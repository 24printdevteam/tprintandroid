package com.tprint.android.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by aboot on 7/17/17.
 */

public class WalletModel implements Parcelable {

    private String ID;
    private String JobID;
    private long Value;
    private long Balance;
    private String Description;
    private long Timestamp;

    protected WalletModel(Parcel in) {
        ID = in.readString();
        JobID = in.readString();
        Value = in.readLong();
        Balance = in.readLong();
        Description = in.readString();
        Timestamp = in.readLong();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(ID);
        dest.writeString(JobID);
        dest.writeLong(Value);
        dest.writeLong(Balance);
        dest.writeString(Description);
        dest.writeLong(Timestamp);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<WalletModel> CREATOR = new Creator<WalletModel>() {
        @Override
        public WalletModel createFromParcel(Parcel in) {
            return new WalletModel(in);
        }

        @Override
        public WalletModel[] newArray(int size) {
            return new WalletModel[size];
        }
    };

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getJobID() {
        return JobID;
    }

    public void setJobID(String jobID) {
        JobID = jobID;
    }

    public long getValue() {
        return Value;
    }

    public void setValue(long value) {
        Value = value;
    }

    public long getBalance() {
        return Balance;
    }

    public void setBalance(long balance) {
        Balance = balance;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public long getTimestamp() {
        return Timestamp;
    }

    public void setTimestamp(long timestamp) {
        Timestamp = timestamp;
    }
}
