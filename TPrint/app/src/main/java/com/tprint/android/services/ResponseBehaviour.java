package com.tprint.android.services;

import android.support.annotation.NonNull;

import com.tprint.android.etc.DataUtils;
import com.tprint.android.etc.DeviceUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by aboot on 10/30/16.
 */
class ResponseBehaviour implements Callback<APIResponseData> {

    private API api;
    private APIListener apiListener;

    ResponseBehaviour(API api, APIListener apiListener) {
        this.api = api;
        this.apiListener = apiListener;
    }

    @Override
    public void onResponse(@NonNull Call<APIResponseData> call, @NonNull Response<APIResponseData> response) {
        if (response.code() == 200) {
            APIResponseData body = response.body();
            if (body != null) {
                switch (api) {
                    case Me_Register:
                    case Me_Login:
                    case Me_Profile:
                    case Me_ResetPasswordCode:
                    case Server_PriceList:
                    case Server_LocationList:
                    case History_List:
                    case History_Details:
                    case Wallet_List:
                    case Wallet_TopUp:
                    case Server_BankList:
                    case Print_Preview:
                        if (body.getCode() == 101) {
                            apiListener.onSuccess(body);
                        }else {
                            apiListener.onFailure(api, body.getMessage());
                        }
                        break;
                    case Me_ResendVerification:
                    case Me_Verification:
                    case Me_ChangePhoneNumber:
                    case Me_ForgotPassword:
                    case Me_ResetPassword:
                    case Me_AddPushID:
                    case Print_Upload:
                    case Print_Setting:
                    case Print_Remove:
                    case Print_RefreshAll:
                    case Print_RemoveAll:
                    case Print_Confirmation:
                    case Print_CheckOut:
                    case Wallet_TopUpCancel:
                    case Storage_Upload:
                    case Storage_Print:
                    case Storage_Remove:
                        if (body.getCode() == 100) {
                            apiListener.onSuccess(body);
                        }else {
                            apiListener.onFailure(api, body.getMessage());
                        }
                        break;
                    case Print_List:
                    case Storage_List:
                        if (body.getCode() == 101 || body.getCode() == 200 || body.getCode() == 300) {
                            apiListener.onSuccess(body);
                        }else {
                            apiListener.onFailure(api, body.getMessage());
                        }
                        break;
                    case Wallet_TopUpList:
                        if (body.getCode() == 101 || body.getCode() == 200) {
                            apiListener.onSuccess(body);
                        }else {
                            apiListener.onFailure(api, body.getMessage());
                        }
                        break;
                    default:
                        sendApiResponse(api, "Response Not Handling");


                }
            }else {
                sendApiResponse(api, "No Data");
            }
        }else {
            sendApiResponse(api, "Response failure");
        }
    }

    @Override
    public void onFailure(@NonNull Call<APIResponseData> call, @NonNull Throwable t) {
        if (DeviceUtils.haveNetworkConnection()) {
            if (DataUtils.isNotEmpty(t.getMessage()) && t.getMessage().contains("time")) {
                sendApiResponse(api, "Your connection seems to be slow");
            } else if (DataUtils.isNotEmpty(t.getMessage()) && t.getMessage().contains("cumi.id")) {
                sendApiResponse(api, "Your connection seems off");
            }else {
                sendApiResponse(api, t.getMessage());
            }
        }else {
            sendApiResponse(api, "You are currently offline");
        }
    }

    private void sendApiResponse(API api, String message) {
        apiListener.onFailure(api, message);
//        Answers.getInstance().logCustom(
//                new CustomEvent("API Response")
//                .putCustomAttribute("API", api.toString())
//                .putCustomAttribute("message", message)
//        );
    }
}
