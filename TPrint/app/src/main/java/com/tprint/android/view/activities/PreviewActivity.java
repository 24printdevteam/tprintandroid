package com.tprint.android.view.activities;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.tprint.android.R;
import com.tprint.android.view.BaseActivity;
import butterknife.BindView;

/**
 * Created by aboot on 7/19/17.
 */

public class PreviewActivity extends BaseActivity {

    @BindView(R.id.webView) WebView webView;

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.preview_activity);
        getToolbar().setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        setToolbarTitle("Preview");

        String pdfPreview = getIntent().getStringExtra("pdfPreview");

        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.setWebViewClient(new WebViewClient());
        webView.loadUrl("http://docs.google.com/gview?embedded=true&url="
        +""+pdfPreview);
    }
}
