package com.tprint.android.presenter.Wallet;

/**
 * Created by aboot on 7/17/17.
 */

public interface WalletPresenterImpl {

    void WalletList();
    void WalletTopUpList();
    void WalletTopUp(long amount, String Type);
    void WalletTopUpCancel(String Type);
}
