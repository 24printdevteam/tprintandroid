package com.tprint.android.presenter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tprint.android.R;
import com.tprint.android.view.BaseViewHolder;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by aboot on 6/8/17.
 */

public class DevicePathAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<File> list = new ArrayList<>();

    public void addNewList(List<File> list) {
        this.list.clear();
        if (list != null) {
            this.list.addAll(list);
        }
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.device_path_viewholder, parent, false);
        return new PathHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof PathHolder) {
            PathHolder pathHolder = (PathHolder) holder;
            pathHolder.textPath.setText(list.get(position).getName());
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class PathHolder extends BaseViewHolder {

        @BindView(R.id.textPath) TextView textPath;

        public PathHolder(View itemView) {
            super(itemView);
        }
    }
}
