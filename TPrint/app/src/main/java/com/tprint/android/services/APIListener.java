package com.tprint.android.services;

/**
 * Created by aboot on 10/9/16.
 */
public interface APIListener {

    void onSuccess(APIResponseData response);
    void onFailure(API api, String errorMessage);

}
