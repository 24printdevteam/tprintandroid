package com.tprint.android.presenter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.tprint.android.R;
import com.tprint.android.database.SessionApps;
import com.tprint.android.etc.DataUtils;
import com.tprint.android.etc.GilaPrint;
import com.tprint.android.models.PageDetails;
import com.tprint.android.models.PriceListModel;
import com.tprint.android.models.PrintModel;
import com.tprint.android.view.BaseViewHolder;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by aboot on 5/19/17.
 */

public class PrintAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private PriceListModel priceList;
    private List<PrintModel> list = new ArrayList<>();
    private OnClickListener onClickListener;

    public PrintAdapter(Context context) {
        this.context = context;
        priceList = SessionApps.getPriceList();
    }

    public void addNewList(List<PrintModel> list) {
        if (list != null) {
            this.list.clear();
            this.list.addAll(list);
            notifyDataSetChanged();
        }
    }

    public void setOnClickListener(OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == 1) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.print_add_viewholder, parent, false);
            return new FooterHolder(view);
        }else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.print_item_viewholder, parent, false);
            return new ItemHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof FooterHolder) {
            FooterHolder footerHolder = (FooterHolder) holder;
            footerHolder.btnAddNew.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onClickListener != null)onClickListener.onAddNewItem();
                }
            });
        }else if (holder instanceof ItemHolder) {
            final ItemHolder itemHolder = (ItemHolder) holder;
            final PrintModel model = list.get(position);
            itemHolder.textNumberList.setText("File "+(position+1)+"/"+list.size());
            itemHolder.textFileName.setText(model.getFilename()+"."+model.getExtension());
            itemHolder.textFileFormat.setText(model.getSize()+", "+model.getPages()+" Halaman");

            String Color = context.getString(model.getOColor() == 0 ?
                    R.string.black_amp_white : R.string.color);
            String Side = context.getString(model.getODoubleSided() == 0 ?
                    R.string.satu_muka : R.string.bolak_balik);
            String Page = "Pages: " + model.getOPageFrom()+" s/d "+model.getOPageTo();
            String Copies = model.getOCopies() +" "+ context.getString(R.string.copies);
            itemHolder.textSetting.setText(Color+", "+Side+"\n"+Page);

            itemHolder.textTotalCopies.setText("x"+Copies+"");

            List<PageDetails> detailList = getPageDetails(model);

            int lowPages = getLowPages(detailList);
            int mediumPages = getMediumPages(detailList);
            int highPages = getHighPages(detailList);
            long lowPrices = getLowPrice(model.getSize(), model.getOColor());
            long mediumPrices = getMediumPrice(model.getSize(), model.getOColor());
            long highPrices = getHighPrice(model.getSize(), model.getOColor());

            itemHolder.textLowPages.setText(""+lowPages+" Halaman");
            itemHolder.textLowPrice.setText(DataUtils.getCurrency(lowPages * lowPrices));

            itemHolder.textMediumPages.setText(""+mediumPages+" Halaman");
            itemHolder.textMediumPrice.setText(DataUtils.getCurrency(mediumPages * mediumPrices));

            itemHolder.textHighPages.setText(""+highPages+" Halaman");
            itemHolder.textHighPrice.setText(DataUtils.getCurrency(highPages * highPrices));

            long totalPrice = model.getOCopies() * ((lowPages * lowPrices) + (mediumPages * mediumPrices) + (highPages * highPrices));

            model.setTotalPerFile(totalPrice);
            itemHolder.textPricePerFile.setText(DataUtils.getCurrency(model.getTotalPerFile()));

            list.set(position, model);

            if (position == (list.size() - 1)) {
                if (onClickListener != null) onClickListener.onLoaded();
            }

            //Action
            itemHolder.btnPreview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onClickListener != null) onClickListener.onPreview(model);
                }
            });

            itemHolder.btnSetting.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onClickListener != null) onClickListener.onSetting(model);
                }
            });

            itemHolder.btnArrowDown.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (itemHolder.layFileCustom.getVisibility() == View.VISIBLE) {
                        itemHolder.layFileCustom.setVisibility(View.GONE);
                        itemHolder.btnArrowDown.setImageResource(R.drawable.ic_keyboard_arrow_down_black_24dp);
                    }else {
                        itemHolder.layFileCustom.setVisibility(View.VISIBLE);
                        itemHolder.btnArrowDown.setImageResource(R.drawable.ic_keyboard_arrow_up_black_24dp);
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return list.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == list.size()) {
            return 1;
        }else {
            return 0;
        }
    }

    public List<PrintModel> getList() {
        return list;
    }

    class ItemHolder extends BaseViewHolder {

        @BindView(R.id.textNumberList) TextView textNumberList;
        @BindView(R.id.textFileName) TextView textFileName;
        @BindView(R.id.textFileFormat) TextView textFileFormat;
        @BindView(R.id.textSetting) TextView textSetting;

        @BindView(R.id.textLowPages) TextView textLowPages;
        @BindView(R.id.textLowPrice) TextView textLowPrice;
        @BindView(R.id.textMediumPages) TextView textMediumPages;
        @BindView(R.id.textMediumPrice) TextView textMediumPrice;
        @BindView(R.id.textHighPages) TextView textHighPages;
        @BindView(R.id.textHighPrice) TextView textHighPrice;

        @BindView(R.id.textTotalCopies) TextView textTotalCopies;
        @BindView(R.id.textPricePerFile) TextView textPricePerFile;

        @BindView(R.id.btnPreview) TextView btnPreview;
        @BindView(R.id.btnSetting) TextView btnSetting;
        @BindView(R.id.btnArrowDown) ImageButton btnArrowDown;

        @BindView(R.id.layFileCustom) View layFileCustom;

        private ItemHolder(View itemView) {
            super(itemView);
        }
    }

    class FooterHolder extends BaseViewHolder {

        @BindView(R.id.btnAddNew) Button btnAddNew;

        private FooterHolder(View itemView) {
            super(itemView);
        }
    }

    public interface OnClickListener {
        void onLoaded();
        void onPreview(PrintModel model);
        void onSetting(PrintModel model);
        void onAddNewItem();
    }

    private List<PageDetails> getPageDetails(PrintModel printModel) {
        List<PageDetails> list = new ArrayList<>();
        for (int i = 0; i < printModel.getPageDetails().size(); i++) {
            if (i >= (printModel.getOPageFrom() - 1) && i <= (printModel.getOPageTo() - 1)) {
                list.add(printModel.getPageDetails().get(i));
            }
        }
        return list;
    }

    private int getLowPages(List<PageDetails> details) {
        int pages = 0;
        for (int i = 0; i < details.size(); i++) {
            PageDetails detail = details.get(i);
            if (detail.getSize().equals(GilaPrint.CoverageQuarter)) {
                ++pages;
            }
        }
        return pages;
    }

    private int getMediumPages(List<PageDetails> details) {
        int pages = 0;
        for (int i = 0; i < details.size(); i++) {
            PageDetails detail = details.get(i);
            if (detail.getSize().equals(GilaPrint.CoverageHalf)) {
                ++pages;
            }
        }
        return pages;
    }

    private int getHighPages(List<PageDetails> details) {
        int pages = 0;
        for (int i = 0; i < details.size(); i++) {
            PageDetails detail = details.get(i);
            if (detail.getSize().equals(GilaPrint.CoverageFull)) {
                ++pages;
            }
        }
        return pages;
    }

    private long getLowPrice(String PaperSize, int Color) {
        switch (PaperSize) {
            case GilaPrint.PaperA4:
                return Color == 0 ? priceList.getA4MonoLow() : priceList.getA4ColorLow();
            case GilaPrint.PaperA3:
                return Color == 0 ? priceList.getA3MonoLow() : priceList.getA3ColorLow();
            default:
                return 0;
        }
    }

    private long getMediumPrice(String PaperSize, int Color) {
        switch (PaperSize) {
            case GilaPrint.PaperA4:
                return Color == 0 ? priceList.getA4MonoMedium() : priceList.getA4ColorMedium();
            case GilaPrint.PaperA3:
                return Color == 0 ? priceList.getA3MonoMedium() : priceList.getA3ColorMedium();
            default:
                return 0;
        }
    }

    private long getHighPrice(String PaperSize, int Color) {
        switch (PaperSize) {
            case GilaPrint.PaperA4:
                return Color == 0 ? priceList.getA4MonoHigh() : priceList.getA4ColorHigh();
            case GilaPrint.PaperA3:
                return Color == 0 ? priceList.getA3MonoHigh() : priceList.getA3ColorHigh();
            default:
                return 0;
        }
    }
}
