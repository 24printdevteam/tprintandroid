package com.tprint.android.view.fragments;

import android.annotation.SuppressLint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.tprint.android.R;
import com.tprint.android.etc.GilaPrint;
import com.tprint.android.presenter.HistoryMenuAdapter;
import com.tprint.android.view.BaseFragment;
import butterknife.BindView;
import static com.norbsoft.typefacehelper.TypefaceHelper.typeface;

/**
 * Created by aboot on 5/12/17.
 */

public class HistoryMenuFragment extends BaseFragment implements ViewPager.OnPageChangeListener, TabLayout.OnTabSelectedListener {

    HistoryMenuAdapter historyMenuAdapter;

    @BindView(R.id.viewPager) ViewPager viewPager;
    @BindView(R.id.tabLayout) TabLayout tabLayout;

    public static HistoryMenuFragment newInstance() {
        Bundle args = new Bundle();
        HistoryMenuFragment fragment = new HistoryMenuFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.history_menu_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getToolbar();
        setToolbarTitle(GilaPrint.HistoryPage);

        historyMenuAdapter = new HistoryMenuAdapter(getChildFragmentManager());
        viewPager.setAdapter(historyMenuAdapter);
        viewPager.setOffscreenPageLimit(2);
        viewPager.addOnPageChangeListener(this);
        tabLayout.addTab(tabLayout.newTab());
        tabLayout.addTab(tabLayout.newTab());
        tabLayout.addOnTabSelectedListener(this);

        setUpTab(0);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        setUpTab(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewPager.setCurrentItem(tab.getPosition());
        setUpTab(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    private void setUpTab(int position) {
        TabLayout.Tab tabPrev = tabLayout.getTabAt(position);
        if (tabPrev != null) {
            tabPrev.select();
        }

        tabLayout.invalidate();
        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            assert tab != null;
            tab.setCustomView(null);
            tab.setCustomView(getTabView(tab.getPosition()));
        }
    }

    private View getTabView(int position) {
        @SuppressLint("InflateParams")
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.history_tab_viewholder, null);
        typeface(view);
        TextView tabTitle = (TextView) view.findViewById(R.id.tabTitle);
        String[] title = {"In Progress", "Completed"};
        tabTitle.setText(title[position]);
        if (tabLayout.getSelectedTabPosition() == position) {
            tabTitle.setAlpha(1f);
            tabTitle.setTypeface(null, Typeface.BOLD);
        }else {
            tabTitle.setAlpha(0.5f);
            tabTitle.setTypeface(null, Typeface.NORMAL);
        }
        typeface(tabTitle);
        return view;
    }
}
