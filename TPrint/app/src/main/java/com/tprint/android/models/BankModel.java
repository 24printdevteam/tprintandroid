package com.tprint.android.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by aboot on 7/17/17.
 */

public class BankModel implements Parcelable {

    private String Bank;
    private String BankName;
    private String BankNumber;
    private String BankBranch;

    protected BankModel(Parcel in) {
        Bank = in.readString();
        BankName = in.readString();
        BankNumber = in.readString();
        BankBranch = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(Bank);
        dest.writeString(BankName);
        dest.writeString(BankNumber);
        dest.writeString(BankBranch);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BankModel> CREATOR = new Creator<BankModel>() {
        @Override
        public BankModel createFromParcel(Parcel in) {
            return new BankModel(in);
        }

        @Override
        public BankModel[] newArray(int size) {
            return new BankModel[size];
        }
    };

    public String getBank() {
        return Bank;
    }

    public void setBank(String bank) {
        Bank = bank;
    }

    public String getBankName() {
        return BankName;
    }

    public void setBankName(String bankName) {
        BankName = bankName;
    }

    public String getBankNumber() {
        return BankNumber;
    }

    public void setBankNumber(String bankNumber) {
        BankNumber = bankNumber;
    }

    public String getBankBranch() {
        return BankBranch;
    }

    public void setBankBranch(String bankBranch) {
        BankBranch = bankBranch;
    }
}
