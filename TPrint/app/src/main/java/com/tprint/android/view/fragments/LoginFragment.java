package com.tprint.android.view.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.tprint.android.R;
import com.tprint.android.etc.DataUtils;
import com.tprint.android.etc.DeviceUtils;
import com.tprint.android.etc.GilaPrint;
import com.tprint.android.presenter.Credential.CredentialPageImpl;
import com.tprint.android.presenter.Credential.CredentialPresenter;
import com.tprint.android.presenter.Credential.CredentialViewImpl;
import com.tprint.android.services.API;
import com.tprint.android.services.APIResponseData;
import com.tprint.android.view.BaseFragment;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by aboot on 7/4/17.
 */

public class LoginFragment extends BaseFragment implements CredentialViewImpl {

    private CredentialPageImpl credentialPage;
    private CredentialPresenter credentialPresenter;

    @BindView(R.id.inputNumber) EditText inputNumber;
    @BindView(R.id.inputPassword) EditText inputPassword;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (!(context instanceof CredentialPageImpl)) {
            throw new ClassCastException("Activity must implement CredentialPageImpl");
        }
        this.credentialPage = (CredentialPageImpl) context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.login_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getToolbar().setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        setToolbarTitle(GilaPrint.LoginPage);

        if (DeviceUtils.getUniquePsuedoID().equals("00000000-0828-c81b-0000-000048a33f14")) {
            inputNumber.setText("085921332426");
            inputPassword.setText("Android1");
        }

        credentialPresenter = new CredentialPresenter(this);
    }

    @OnClick(R.id.btnLogin) void onLogin() {
        if (DataUtils.isNotEmpty(inputNumber) && DataUtils.isNotEmpty(inputPassword)) {
            credentialPresenter.MeLogin(inputNumber.getText().toString(),
                    inputPassword.getText().toString());
        }else {
            Toast.makeText(getActivity(), R.string.validate_login, Toast.LENGTH_LONG).show();
        }
    }

    @OnClick(R.id.btnRegister) void onRegister() {
        credentialPage.ChangePage(GilaPrint.RegisterPage);
    }

    @OnClick(R.id.btnForgot) void onReset() {
        credentialPage.ChangePage(GilaPrint.ForgotPasswordPage);
    }

    @Override
    public void showLoading(API api) {
        showLoadingDialog(api);
    }

    @Override
    public void hideLoading() {
        dismissLoadingDialog();
    }

    @Override
    public void LoginSuccess(APIResponseData response) {
        credentialPresenter.MeProfile();
    }

    @Override
    public void RegisterSuccess(APIResponseData response) {}

    @Override
    public void ProfileSuccess(String ChangePage) {
        credentialPage.ChangePage(ChangePage);
    }

    @Override
    public void VerificationSuccess() {}

    @Override
    public void ResendVerificationSuccess(String Message) {}

    @Override
    public void ForgotPasswordSuccess() {}

    @Override
    public void ResetPasswordCodeSuccess() {}

    @Override
    public void ResetPasswordSuccess() {}

    @Override
    public void ChangePhoneNumber() {}

    @Override
    public void RequestFailed(API api, String errorMessage) {
        Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT).show();
    }
}
