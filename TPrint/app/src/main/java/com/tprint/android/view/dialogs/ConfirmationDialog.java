package com.tprint.android.view.dialogs;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tprint.android.R;
import com.tprint.android.view.BaseDialog;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by aboot on 7/27/17.
 */

public class ConfirmationDialog extends BaseDialog {

    private OnClickListener onClickListener;

    @BindView(R.id.textTitle) TextView textTitle;
    @BindView(R.id.textMessage) TextView textMessage;
    @BindView(R.id.btnNegative) TextView btnNegative;
    @BindView(R.id.btnPositive) TextView btnPositive;


    public static ConfirmationDialog newInstance(String title, String Message,
                                                 String Negative, String Positive,
                                                 FragmentManager transaction,
                                                 OnClickListener onClickListener) {
        Bundle args = new Bundle();
        args.putString("Title", title);
        args.putString("Message", Message);
        args.putString("Negative", Negative);
        args.putString("Positive", Positive);
        ConfirmationDialog fragment = new ConfirmationDialog();
        fragment.setArguments(args);
        fragment.onClickListener = onClickListener;
        fragment.show(transaction, "Confirmation");
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.confirmation_dialog, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        textTitle.setText(getTitle());
        textMessage.setText(getMessage());
        btnNegative.setText(getNegative());
        btnPositive.setText(getPositive());
    }

    private String getTitle() {
        return getArguments().getString("Title");
    }

    private String getMessage() {
        return getArguments().getString("Message");
    }

    private String getNegative() {
        return getArguments().getString("Negative");
    }

    private String getPositive() {
        return getArguments().getString("Positive");
    }

    @OnClick(R.id.btnNegative) void onNegative() {
        onClickListener.onClick(false);
        dismissDialog();
    }

    @OnClick(R.id.btnPositive) void onPositive() {
        onClickListener.onClick(true);
        dismissDialog();
    }

    public interface OnClickListener {
        void onClick(boolean isPositive);
    }
}
