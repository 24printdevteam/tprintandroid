package com.tprint.android.presenter;

import android.annotation.SuppressLint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.tprint.android.R;
import com.tprint.android.database.SessionApps;
import com.tprint.android.etc.DataUtils;
import com.tprint.android.view.BaseViewHolder;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by aboot on 7/7/17.
 */

public class UploadAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private int uploadStatus = 0;
    private List<File> list = new ArrayList<>();
    private OnUploadListener onUploadListener;

    public void addNewList(List<File> list) {
        this.list.clear();
        if (list != null) {
            this.uploadStatus = SessionApps.getUploadStatus();
            this.list.addAll(list);
        }
        notifyDataSetChanged();
    }

    public void addList(File file) {
        if (file != null) {
            list.add(file);
            notifyDataSetChanged();
        }
    }

    public void removeFile(int Position) {
        list.remove(Position);
        SessionApps.setUploadList(list);
        notifyDataSetChanged();
    }

    public void uploadStatus(int Status) {
        this.uploadStatus = Status;
        notifyDataSetChanged();
    }

    public void setOnUploadListener(OnUploadListener onUploadListener) {
        this.onUploadListener = onUploadListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == 0) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.upload_item_viewholder, parent, false);
            return new FileHolder(view);
        }else {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.upload_add_viewholder, parent, false);
            return new AddHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder,
                                 @SuppressLint("RecyclerView") final int position) {
        if (holder instanceof FileHolder) {
            FileHolder fileHolder = (FileHolder) holder;

            final File file = list.get(position);
            fileHolder.textFileName.setText(file.getName());
            fileHolder.textFileSize.setText(DataUtils.getFileSize(file.length()));
            fileHolder.btnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onUploadListener != null) onUploadListener.onRemove(position);
                }
            });
            fileHolder.uploadLoading.setVisibility(uploadStatus == 0 ? View.GONE : View.VISIBLE);
            fileHolder.btnDelete.setVisibility(uploadStatus == 0 ? View.VISIBLE : View.GONE);
            fileHolder.textStatus.setVisibility(uploadStatus == 0 ? View.GONE : View.VISIBLE);
            if (uploadStatus == 1) {
                fileHolder.textStatus.setText("Uploading...");
            }else if (uploadStatus == 2) {
                fileHolder.textStatus.setText("memproses...");
            }

            fileHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onUploadListener != null) onUploadListener.onOpenFile(file);
                }
            });
        }else if (holder instanceof AddHolder) {
            AddHolder addHolder = (AddHolder) holder;
            addHolder.btnAddFile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onUploadListener != null) onUploadListener.onAddFile();
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return list.size() + (uploadStatus == 0 ? 1 : 0);
    }

    @Override
    public int getItemViewType(int position) {
        if (position == list.size()) {
            return 1;
        }else {
            return 0;
        }
    }

    public List<File> getList() {
        return list;
    }

    class FileHolder extends BaseViewHolder {

        @BindView(R.id.textFileName) TextView textFileName;
        @BindView(R.id.textFileSize) TextView textFileSize;
        @BindView(R.id.btnDelete) ImageButton btnDelete;
        @BindView(R.id.uploadLoading) ProgressBar uploadLoading;
        @BindView(R.id.textStatus) TextView textStatus;

        public FileHolder(View itemView) {
            super(itemView);
        }
    }

    class AddHolder extends BaseViewHolder {

        @BindView(R.id.btnAddFile) Button btnAddFile;

        public AddHolder(View itemView) {
            super(itemView);
        }
    }

    public interface OnUploadListener {
        void onAddFile();
        void onOpenFile(File file);
        void onRemove(int Position);
    }
}
