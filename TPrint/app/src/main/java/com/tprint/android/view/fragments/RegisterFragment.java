package com.tprint.android.view.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.tprint.android.R;
import com.tprint.android.etc.DataUtils;
import com.tprint.android.etc.GilaPrint;
import com.tprint.android.etc.Tools;
import com.tprint.android.presenter.Credential.CredentialPageImpl;
import com.tprint.android.presenter.Credential.CredentialPresenter;
import com.tprint.android.presenter.Credential.CredentialViewImpl;
import com.tprint.android.services.API;
import com.tprint.android.services.APIResponseData;
import com.tprint.android.view.BaseFragment;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by aboot on 7/4/17.
 */

public class RegisterFragment extends BaseFragment implements CredentialViewImpl {

    private CredentialPageImpl credentialPage;
    private CredentialPresenter credentialPresenter;

    @BindView(R.id.inputNumber) EditText inputNumber;
    @BindView(R.id.inputEmail) EditText inputEmail;
    @BindView(R.id.inputName) EditText inputName;
    @BindView(R.id.inputPassword) EditText inputPassword;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (!(context instanceof CredentialPageImpl)) {
            throw new ClassCastException("Activity must implement CredentialPageImpl");
        }
        this.credentialPage = (CredentialPageImpl) context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.register_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getToolbar().setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        setToolbarTitle(GilaPrint.RegisterPage);

        credentialPresenter = new CredentialPresenter(this);
    }

    @OnClick(R.id.btnRegister) void onRegister() {
        if (!DataUtils.isNotEmpty(inputNumber)) {
            Toast.makeText(getActivity(), R.string.validate_number_insert, Toast.LENGTH_SHORT).show();
            return;
        }else {
            if (!Tools.isValidNumber(inputNumber.getText().toString())) {
                Toast.makeText(getActivity(), R.string.validate_number_valid, Toast.LENGTH_SHORT).show();
                return;
            }
        }

        if (!DataUtils.isNotEmpty(inputEmail)) {
            Toast.makeText(getActivity(), R.string.validate_email_insert, Toast.LENGTH_SHORT).show();
            return;
        }else {
            if (!DataUtils.isValidEmail(inputEmail)) {
                Toast.makeText(getActivity(), R.string.validate_number_valid, Toast.LENGTH_SHORT).show();
                return;
            }
        }

        if (!DataUtils.isNotEmpty(inputName)) {
            Toast.makeText(getActivity(), R.string.validate_name_insert, Toast.LENGTH_SHORT).show();
            return;
        }

        if (!DataUtils.isNotEmpty(inputPassword)) {
            Toast.makeText(getActivity(), R.string.validate_password_insert, Toast.LENGTH_SHORT).show();
            return;
        }else {
            if (inputPassword.getText().toString().length() < 6 &&
                    !DataUtils.isNumberAndAlphabet(inputPassword.getText().toString())) {
                Toast.makeText(getActivity(), R.string.validate_password_valid, Toast.LENGTH_SHORT).show();
                return;
            }
        }

        credentialPresenter.MeRegister(inputNumber.getText().toString(), inputEmail.getText().toString(),
                inputName.getText().toString(), inputPassword.getText().toString());
    }

    @Override
    public void showLoading(API api) {
        showLoadingDialog(api);
    }

    @Override
    public void hideLoading() {
        dismissLoadingDialog();
    }

    @Override
    public void LoginSuccess(APIResponseData response) {}

    @Override
    public void RegisterSuccess(APIResponseData response) {
        credentialPresenter.MeProfile();
    }

    @Override
    public void ProfileSuccess(String ChangePage) {
        credentialPage.ChangePage(ChangePage);
    }

    @Override
    public void VerificationSuccess() {}

    @Override
    public void ResendVerificationSuccess(String Message) {}

    @Override
    public void ForgotPasswordSuccess() {}

    @Override
    public void ResetPasswordCodeSuccess() {}

    @Override
    public void ResetPasswordSuccess() {}

    @Override
    public void ChangePhoneNumber() {}

    @Override
    public void RequestFailed(API api, String errorMessage) {
        Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT).show();
    }
}
