package com.tprint.android.presenter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.tprint.android.R;
import com.tprint.android.etc.DataUtils;
import com.tprint.android.etc.GilaPrint;
import com.tprint.android.models.HistoryModel;
import com.tprint.android.view.BaseViewHolder;
import java.util.ArrayList;
import java.util.List;
import butterknife.BindView;

/**
 * Created by aboot on 5/29/17.
 */

public class HistoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<HistoryModel> list = new ArrayList<>();
    private OnClickListener onClickListener;

    public HistoryAdapter(Context context) {
        this.context = context;
    }

    public void addNewList(List<HistoryModel> list) {
        this.list.clear();
        if (list != null) {
            this.list.addAll(list);
        }
        notifyDataSetChanged();
    }

    public void setOnClickListener(OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.history_item_viewholder, parent, false);
        return new ItemHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ItemHolder) {
            ItemHolder itemHolder = (ItemHolder) holder;
            final HistoryModel model = list.get(position);

            if (model.getStatus().equals(GilaPrint.HistoryPending)) {
                itemHolder.imageStatus.setImageResource(R.drawable.ic_order_status_pending);
                itemHolder.textStatus.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
            }else if (model.getStatus().equals(GilaPrint.HistoryWaiting)) {
                itemHolder.imageStatus.setImageResource(R.drawable.ic_order_status_paid);
                itemHolder.textStatus.setTextColor(ContextCompat.getColor(context, R.color.waiting));
            } else if (model.getStatus().equals(GilaPrint.HistoryExpired)) {
                itemHolder.imageStatus.setImageResource(R.drawable.ic_order_status_expired);
                itemHolder.textStatus.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
            }else if (model.getStatus().equals(GilaPrint.HistoryCompleted)) {
                itemHolder.imageStatus.setImageResource(R.drawable.ic_order_status_completed);
                itemHolder.textStatus.setTextColor(ContextCompat.getColor(context, android.R.color.black));
            }

            itemHolder.textStatus.setText(model.getStatus());
            itemHolder.textDate.setText(" - "+ DataUtils.getDateServer(model.getTCreated())+"");
            itemHolder.textFileFormat.setText("Total: "+model.getTotalPages()+" Pages");
            itemHolder.textCodePrint.setText("Kode: "+model.getID()+"");

            itemHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onClickListener != null) onClickListener.onClick(model.getID());
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ItemHolder extends BaseViewHolder {

        @BindView(R.id.imageStatus) ImageView imageStatus;
        @BindView(R.id.textStatus) TextView textStatus;
        @BindView(R.id.textDate) TextView textDate;
        @BindView(R.id.textFileFormat) TextView textFileFormat;
        @BindView(R.id.textCodePrint) TextView textCodePrint;


        private ItemHolder(View itemView) {
            super(itemView);
        }
    }

    public interface OnClickListener {
        void onClick(String orderID);

    }
}
