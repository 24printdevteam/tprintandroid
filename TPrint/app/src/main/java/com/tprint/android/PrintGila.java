package com.tprint.android;

import android.app.Application;

import com.crashlytics.android.Crashlytics;
import com.tprint.android.database.SessionApps;
import com.tprint.android.etc.DeviceUtils;
import com.tprint.android.etc.ScreenUtils;
import com.tprint.android.etc.Tools;
import io.fabric.sdk.android.Fabric;


/**
 * Created by aboot on 4/3/17.
 */

public class PrintGila extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        Tools.initialize(this);
        DeviceUtils.initialize(this);
        ScreenUtils.initialize(this);
        SessionApps.initialize(this);
    }
}
