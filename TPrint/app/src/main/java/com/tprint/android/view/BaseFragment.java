package com.tprint.android.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.norbsoft.typefacehelper.TypefaceHelper;
import com.tprint.android.R;
import com.tprint.android.services.API;
import com.tprint.android.view.dialogs.LoadingDialog;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by aboot on 9/8/16.
 */
public class BaseFragment extends Fragment {

    private View view;
    private LoadingDialog loadingDialog;


    @Nullable @BindView(R.id.toolbarTitle) TextView toolbarTitle;
    @Nullable @BindView(R.id.toolbarImage) ImageView toolbarImage;

    @Nullable @BindView(R.id.layEmpty) LinearLayout layEmpty;
    @Nullable @BindView(R.id.textEmpty) TextView textEmpty;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.view = view;
        ButterKnife.bind(this, view);
        TypefaceHelper.typeface(view);
    }

    protected Toolbar getToolbar(){
        if (view != null) {
            Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
            if (toolbar != null) {
                AppCompatActivity activity = (AppCompatActivity) getActivity();
                activity.setSupportActionBar(toolbar);
                toolbar.setTitle(null);
                activity.setTitle(null);
                return toolbar;
            }
        }
        return null;
    }

    protected void setToolbarTitle(@Nullable String title) {
        if (toolbarTitle != null) {
            getActivity().setTitle(null);
            toolbarTitle.setVisibility(View.VISIBLE);
            toolbarTitle.setText(title);
            if (toolbarImage != null) toolbarImage.setVisibility(View.GONE);
        }
    }

    protected void showToolbarImage() {
        if (toolbarImage != null) {
            toolbarImage.setVisibility(View.VISIBLE);
            if (toolbarTitle != null) toolbarTitle.setVisibility(View.GONE);
        }
    }

    protected void showEmpty(String emptyText) {
        if (layEmpty != null) layEmpty.setVisibility(View.VISIBLE);
        if (textEmpty != null) textEmpty.setText(emptyText);
    }

    protected void hideAllState() {
        if (layEmpty != null) layEmpty.setVisibility(View.GONE);
    }

    protected void showLoadingDialog(API api) {
        String Message;
        if (api == null) {
            Message = null;
        }else if (api == API.Me_Login) {
            Message = getString(R.string.loadingText);
        }else if (api == API.Me_Register) {
            Message = getString(R.string.loadingText);
        }else if (api == API.Me_Profile) {
            Message = getString(R.string.loading_profile);
        }else if (api == API.Me_ResendVerification) {
            Message = getString(R.string.loading_resend_verification);
        }else if (api == API.Me_Verification) {
            Message = getString(R.string.loading_verification);
        }else if (api == API.Me_ChangePhoneNumber) {
            Message = getString(R.string.loading_change_phone);
        }else if (api == API.Me_ForgotPassword) {
            Message = getString(R.string.loadingText);
        }else if (api == API.Me_ResetPasswordCode) {
            Message = getString(R.string.loading_verification);
        }else if (api == API.Me_ResetPassword) {
            Message = getString(R.string.loading_update_password);
        }else if (api == API.Print_List) {
            Message = getString(R.string.loading_print_list);
        }else if (api == API.Storage_Upload) {
            Message = getString(R.string.loading_upload);
        }

        else {
            Message = getString(R.string.loadingText);
        }
        dismissLoadingDialog();
        loadingDialog = LoadingDialog.newInstance(Message);
        loadingDialog.show(getChildFragmentManager(), Message);
    }

    protected void showLoadingDialog(String Message) {
        dismissLoadingDialog();
        loadingDialog = LoadingDialog.newInstance(Message);
        loadingDialog.show(getChildFragmentManager(), Message);
    }

    protected void dismissLoadingDialog() {
        if (loadingDialog != null) {
            loadingDialog.dismissDialog();
            loadingDialog = null;
        }
    }
}