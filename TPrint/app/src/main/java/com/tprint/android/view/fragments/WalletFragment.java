package com.tprint.android.view.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.tprint.android.R;
import com.tprint.android.database.SessionApps;
import com.tprint.android.etc.DataUtils;
import com.tprint.android.etc.GilaPrint;
import com.tprint.android.presenter.Wallet.WalletPresenter;
import com.tprint.android.presenter.Wallet.WalletViewImpl;
import com.tprint.android.presenter.WalletAdapter;
import com.tprint.android.services.API;
import com.tprint.android.services.APIResponseData;
import com.tprint.android.view.BaseFragment;
import com.tprint.android.view.activities.TopUpActivity;
import com.tprint.android.view.activities.TopUpDetailActivity;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by aboot on 5/12/17.
 */

public class WalletFragment extends BaseFragment implements
        SwipeRefreshLayout.OnRefreshListener, WalletViewImpl {

    private WalletAdapter walletAdapter;
    private WalletPresenter walletPresenter;

    @BindView(R.id.textBalance) TextView textBalance;
    @BindView(R.id.refreshLayout) SwipeRefreshLayout refreshLayout;
    @BindView(R.id.recyclerView) RecyclerView recyclerView;

    public static WalletFragment newInstance() {
        Bundle args = new Bundle();
        WalletFragment fragment = new WalletFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.wallet_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getToolbar();
        setToolbarTitle(GilaPrint.WalletPage);

        walletPresenter = new WalletPresenter(this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        walletAdapter = new WalletAdapter();
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(walletAdapter);
        refreshLayout.setOnRefreshListener(this);

        walletPresenter.WalletList();
    }

    @Override
    public void onResume() {
        super.onResume();
        textBalance.setText(DataUtils.getCurrency(SessionApps.getProfile().getBalance()));
    }

    @Override
    public void onRefresh() {
        refreshLayout.setRefreshing(false);
        walletPresenter.WalletList();
    }

    @OnClick(R.id.btnTopUp) void onTopUp() {
        walletPresenter.WalletTopUpList();
    }

    @Override
    public void showLoading(API api) {
        if (api == API.Wallet_List) {
            refreshLayout.setRefreshing(true);
        }else {
            showLoadingDialog(api);
        }
    }

    @Override
    public void hideLoading() {
        dismissLoadingDialog();
    }

    @Override
    public void ListSuccess(APIResponseData response) {
        refreshLayout.setRefreshing(false);
        walletAdapter.addNewList(response.getWalletList());
    }

    @Override
    public void TopUpListSuccess(APIResponseData response) {
        if (response.getCode() == 101) {
            Intent intent = new Intent(getActivity(), TopUpDetailActivity.class);
            intent.putExtra("topUpModel", response.getTopUpList().get(0));
            startActivity(intent);
        }else if (response.getCode() == 200) {
            Intent intent = new Intent(getActivity(), TopUpActivity.class);
            startActivity(intent);
        }
    }

    @Override
    public void TopUpSuccess() {}

    @Override
    public void TopUpCancelSuccess() {}

    @Override
    public void RequestFailed(API api, String errorMessage) {
        Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT).show();
    }
}
