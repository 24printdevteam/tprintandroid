package com.tprint.android.view.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tprint.android.R;
import com.tprint.android.database.SessionApps;
import com.tprint.android.etc.GilaPrint;
import com.tprint.android.models.ProfileModel;
import com.tprint.android.view.BaseFragment;
import com.tprint.android.view.activities.SplashActivity;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by aboot on 5/12/17.
 */

public class ProfileFragment extends BaseFragment {

    @BindView(R.id.textNumber) TextView textNumber;
    @BindView(R.id.textName) TextView textName;
    @BindView(R.id.textEmail) TextView textEmail;

    public static ProfileFragment newInstance() {
        Bundle args = new Bundle();
        ProfileFragment fragment = new ProfileFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.profile_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getToolbar();
        setToolbarTitle(GilaPrint.ProfilePage);

        ProfileModel profile = SessionApps.getProfile();
        if (profile != null) {
            textNumber.setText(profile.getPhone());
            textName.setText(profile.getName());
            textEmail.setText(profile.getEmail());
        }
    }

    @OnClick(R.id.btnLogOut) void onLogOut() {
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                SessionApps.clearToken();
                Intent intent = new Intent(getActivity(), SplashActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                getActivity().finish();
            }
        });
    }

}
