package com.tprint.android.presenter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tprint.android.R;

/**
 * Created by aboot on 5/12/17.
 */

public class SlideAdapter extends PagerAdapter {

    Context context;
    LayoutInflater layoutInflater;

    public SlideAdapter(Context context) {
        this.context = context;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view = layoutInflater.inflate(R.layout.slide_item_viewholder, container, false);
        ImageView imageSlide = (ImageView) view.findViewById(R.id.imageSlide);
        TextView textTitleSlide = (TextView) view.findViewById(R.id.textTitleSlide);
        TextView textDescSlide = (TextView) view.findViewById(R.id.textDescSlide);

        switch (position) {
            case 0:
                imageSlide.setImageResource(R.drawable.ic_slide_0);
                textTitleSlide.setText(R.string.slide_0);
                textDescSlide.setText(R.string.slide_0_desc);
                break;
            case 1:
                imageSlide.setImageResource(R.drawable.ic_slide_1);
                textTitleSlide.setText(R.string.slide_1);
                textDescSlide.setText(R.string.slide_1_desc);
                break;
            case 2:
                imageSlide.setImageResource(R.drawable.ic_slide_2);
                textTitleSlide.setText(R.string.slide_2);
                textDescSlide.setText(R.string.slide_2_desc);
                break;
            case 3:
                imageSlide.setImageResource(R.drawable.ic_slide_3);
                textTitleSlide.setText(R.string.slide_3);
                textDescSlide.setText(R.string.slide_3_desc);
                break;

        }


        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout)object);
    }
}
