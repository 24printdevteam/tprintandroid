package com.tprint.android.presenter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import com.tprint.android.R;

/**
 * Created by aboot on 5/12/17.
 */

public class BannerAdapter extends PagerAdapter {

    private OnClickListener onClickListener;
    private LayoutInflater layoutInflater;
    private int[] banner = {
            R.drawable.slide1,
            R.drawable.slide2,
            R.drawable.slide3,
            R.drawable.slide4,
            R.drawable.slide5,
            R.drawable.slide6,
            R.drawable.slide7,
            R.drawable.slide8,
    };

    public BannerAdapter(Context context, OnClickListener onClickListener) {
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.onClickListener = onClickListener;
    }

    @Override
    public int getCount() {
        return banner.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View view = layoutInflater.inflate(R.layout.banner_item_viewholder, container, false);
        ImageView imageBanner = (ImageView) view.findViewById(R.id.imageBanner);
        View more = view.findViewById(R.id.btnMore);
        imageBanner.setImageResource(banner[position]);
        more.setVisibility(position == 3 ? View.VISIBLE : View.GONE);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onClickListener != null) onClickListener.onClick(position);
            }
        });
        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((FrameLayout)object);
    }

    public interface OnClickListener {
        void onClick(int Position);
    }
}
