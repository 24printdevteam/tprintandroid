package com.tprint.android.services;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tprint.android.database.SessionApps;
import com.tprint.android.etc.DeviceUtils;
import com.tprint.android.etc.GilaPrint;
import com.tprint.android.etc.Tools;
import com.tprint.android.models.PrintModel;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by aboot on 6/28/17.
 */

public class APIRequest {

    private APIServices getServices(API api) {
        Gson gson = new GsonBuilder().registerTypeAdapter(APIResponseData.class, new APIDeserializer(api)).create();
        Retrofit retrofit = new Retrofit.Builder().addConverterFactory(GsonConverterFactory.create(gson)).baseUrl(Tools.getBaseUrl())
                .client(Tools.getHttpClient().build()).build();
        return retrofit.create(APIServices.class);
    }

    public void MeRegister(String Number, String Email, String Name, String Password,
                           APIListener apiListener) {
        API api = API.Me_Register;
        getServices(api).meRegister(
                DeviceUtils.getUniquePsuedoID(),
                GilaPrint.Type,
                Number,
                Email,
                Name,
                Password
        ).enqueue(new ResponseBehaviour(api, apiListener));
    }

    public void MeLogin(String Username, String Password,
                        APIListener apiListener) {
        API api = API.Me_Login;
        getServices(api).meLogin(
                DeviceUtils.getUniquePsuedoID(),
                GilaPrint.Type,
                Username,
                Password
        ).enqueue(new ResponseBehaviour(api, apiListener));
    }

    public void MeProfile(APIListener apiListener) {
        API api = API.Me_Profile;
        getServices(api).meProfile(
                SessionApps.getTokenApps()
        ).enqueue(new ResponseBehaviour(api, apiListener));
    }

    public void MeResendVerification(APIListener apiListener) {
        API api = API.Me_ResendVerification;
        getServices(api).meResendVerification(
                SessionApps.getTokenApps()
        ).enqueue(new ResponseBehaviour(api, apiListener));
    }

    public void MeVerification(String Code, APIListener apiListener) {
        API api = API.Me_Verification;
        getServices(api).meVerification(
                SessionApps.getTokenApps(),
                Code
        ).enqueue(new ResponseBehaviour(api, apiListener));
    }

    public void MeChangePhoneNumber(String Phone, APIListener apiListener) {
        API api = API.Me_ChangePhoneNumber;
        getServices(api).meChangePhoneNumber(
                SessionApps.getTokenApps(),
                Phone
        ).enqueue(new ResponseBehaviour(api, apiListener));
    }

    public void MeForgotPassword(boolean isEmail, String EmailPhone, APIListener apiListener) {
        API api = API.Me_ForgotPassword;
        Map<String, RequestBody> map = new HashMap<>();
        map.put(isEmail ? "Email" : "Phone", RequestBody.create(MediaType.parse("text/plain"), EmailPhone));
        getServices(api).meForgotPassword(
                map
        ).enqueue(new ResponseBehaviour(api, apiListener));
    }

    public void MeResetPasswordCode(boolean isEmail, String EmailPhone, String Code, APIListener apiListener) {
        API api = API.Me_ResetPasswordCode;
        Map<String, RequestBody> map = new HashMap<>();
        map.put("DeviceID", RequestBody.create(MediaType.parse("text/plain"), DeviceUtils.getUniquePsuedoID()));
        map.put("Type", RequestBody.create(MediaType.parse("text/plain"), GilaPrint.Type));
        map.put(isEmail ? "Email" : "Phone", RequestBody.create(MediaType.parse("text/plain"), EmailPhone));
        map.put("Code", RequestBody.create(MediaType.parse("text/plain"), Code));
        getServices(api).meResetPasswordCode(
                map
        ).enqueue(new ResponseBehaviour(api, apiListener));
    }

    public void MeResetPassword(String Password, APIListener apiListener) {
        API api = API.Me_ResetPassword;
        getServices(api).meResetPassword(
                SessionApps.getTokenApps(),
                Password
        ).enqueue(new ResponseBehaviour(api, apiListener));
    }

    public void MeAddPushID(String PushID, APIListener apiListener) {
        API api = API.Me_AddPushID;
        getServices(api).meAddPushID(
                DeviceUtils.getUniquePsuedoID(),
                PushID
        ).enqueue(new ResponseBehaviour(api, apiListener));
    }

    public void PrintUpload(List<File> list, APIListener apiListener) {
        Map<String, RequestBody> map = new HashMap<>();
        map.put("Token", RequestBody.create(MediaType.parse("text/plain"), SessionApps.getTokenApps()));
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).exists()) {
                map.put("file[]\"; filename=\" " + list.get(i).getName(),
                        RequestBody.create(MediaType.parse("*/*"), list.get(i)));
            }
        }
        API api = API.Print_Upload;
        getServices(api).printUpload(
                map
        ).enqueue(new ResponseBehaviour(api, apiListener));
    }

    public void PrintList(APIListener apiListener) {
        API api = API.Print_List;
        getServices(api).printList(
                SessionApps.getTokenApps()
        ).enqueue(new ResponseBehaviour(api, apiListener));
    }

    public void PrintSetting(PrintModel model, int PrintReceipt, APIListener apiListener) {
        Map<String, RequestBody> map = new HashMap<>();
        map.put("Token", RequestBody.create(MediaType.parse("text/plain"), SessionApps.getTokenApps()));
        map.put("OPageFrom["+model.getID()+"]", RequestBody.create(MediaType.parse("text/plain"),
                String.valueOf(model.getOPageFrom())));
        map.put("OPageTo["+model.getID()+"]", RequestBody.create(MediaType.parse("text/plain"),
                String.valueOf(model.getOPageTo())));
        map.put("OCopies["+model.getID()+"]", RequestBody.create(MediaType.parse("text/plain"),
                String.valueOf(model.getOCopies())));
        map.put("OColor["+model.getID()+"]", RequestBody.create(MediaType.parse("text/plain"),
                String.valueOf(model.getOColor())));
        map.put("ODoubleSided["+model.getID()+"]", RequestBody.create(MediaType.parse("text/plain"),
                String.valueOf(model.getODoubleSided())));
        map.put("PrintReceipt", RequestBody.create(MediaType.parse("text/plain"),
                String.valueOf(PrintReceipt)));
        API api = API.Print_Setting;
        getServices(api).printSetting(
                map
        ).enqueue(new ResponseBehaviour(api, apiListener));
    }

    public void PrintPreview(String ID, int Color, int PageFrom, int PageTo, APIListener apiListener) {
        API api = API.Print_Preview;
        getServices(api).printPreview(
                SessionApps.getTokenApps(),
                ID,
                Color,
                PageFrom,
                PageTo
        ).enqueue(new ResponseBehaviour(api, apiListener));
    }

    public void PrintRemove(String ID, APIListener apiListener) {
        API api = API.Print_Remove;
        getServices(api).printRemove(
                SessionApps.getTokenApps(),
                ID
        ).enqueue(new ResponseBehaviour(api, apiListener));
    }

    public void PrintRefreshAll(APIListener apiListener) {
        API api = API.Print_RefreshAll;
        getServices(api).printRefreshAll(
                SessionApps.getTokenApps()
        ).enqueue(new ResponseBehaviour(api, apiListener));
    }

    public void PrintRemoveAll(APIListener apiListener) {
        API api = API.Print_RemoveAll;
        getServices(api).printRemoveAll(
                SessionApps.getTokenApps()
        ).enqueue(new ResponseBehaviour(api, apiListener));
    }

    public void PrintConfirmation(APIListener apiListener) {
        API api = API.Print_Confirmation;
        getServices(api).printConfirmation(
                SessionApps.getTokenApps()
        ).enqueue(new ResponseBehaviour(api, apiListener));
    }

    public void PrintCheckOut(String Type, APIListener apiListener) {
        API api = API.Print_CheckOut;
        getServices(api).printCheckOut(
                SessionApps.getTokenApps(),
                Type
        ).enqueue(new ResponseBehaviour(api, apiListener));
    }

    public void HistoryList(APIListener apiListener) {
        API api = API.History_List;
        getServices(api).historyList(
                SessionApps.getTokenApps()
        ).enqueue(new ResponseBehaviour(api, apiListener));
    }

    public void HistoryDetails(String ID, APIListener apiListener) {
        API api = API.History_Details;
        getServices(api).historyDetails(
                SessionApps.getTokenApps(),
                ID
        ).enqueue(new ResponseBehaviour(api, apiListener));
    }

    public void WalletList(APIListener apiListener) {
        API api = API.Wallet_List;
        getServices(api).walletList(
                SessionApps.getTokenApps()
        ).enqueue(new ResponseBehaviour(api, apiListener));
    }

    public void WalletTopUpList(APIListener apiListener) {
        API api = API.Wallet_TopUpList;
        getServices(api).walletTopUpList(
                SessionApps.getTokenApps()
        ).enqueue(new ResponseBehaviour(api, apiListener));
    }

    public void WalletTopUp(long Amount, String Type, APIListener apiListener) {
        API api = API.Wallet_TopUp;
        getServices(api).walletTopUp(
                SessionApps.getTokenApps(),
                Amount,
                Type
        ).enqueue(new ResponseBehaviour(api, apiListener));
    }

    public void WalletTopUpCancel(String Type, APIListener apiListener) {
        API api = API.Wallet_TopUpCancel;
        getServices(api).walletTopUpCancel(
                SessionApps.getTokenApps(),
                Type
        ).enqueue(new ResponseBehaviour(api, apiListener));
    }

    public void StorageUpload(File file, APIListener apiListener) {
        Map<String, RequestBody> map = new HashMap<>();
        map.put("Token", RequestBody.create(MediaType.parse("text/plain"), SessionApps.getTokenApps()));
        map.put("file[]\"; filename=\" " + file.getName(),
                RequestBody.create(MediaType.parse("*/*"), file));
        API api = API.Storage_Upload;
        getServices(api).storageUpload(
                map
        ).enqueue(new ResponseBehaviour(api, apiListener));
    }

    public void StorageList(APIListener apiListener) {
        API api = API.Storage_List;
        getServices(api).storageList(
                SessionApps.getTokenApps()
        ).enqueue(new ResponseBehaviour(api, apiListener));
    }

    public void StoragePrint(String IDs, APIListener apiListener) {
        API api = API.Storage_Print;
        getServices(api).storagePrint(
                SessionApps.getTokenApps(),
                IDs
        ).enqueue(new ResponseBehaviour(api, apiListener));
    }

    public void StorageRemove(String ID, APIListener apiListener) {
        API api = API.Storage_Remove;
        getServices(api).storageRemove(
                SessionApps.getTokenApps(),
                ID
        ).enqueue(new ResponseBehaviour(api, apiListener));
    }

    public void ServerPriceList(APIListener apiListener) {
        API api = API.Server_PriceList;
        getServices(api).serverPriceList(
                SessionApps.getTokenApps()
        ).enqueue(new ResponseBehaviour(api, apiListener));
    }

    public void ServerLocationList(APIListener apiListener) {
        API api = API.Server_LocationList;
        getServices(api).serverLocationList(
                SessionApps.getTokenApps()
        ).enqueue(new ResponseBehaviour(api, apiListener));
    }

    public void ServerBankList(APIListener apiListener) {
        API api = API.Server_BankList;
        getServices(api).serverBankList(
                SessionApps.getTokenApps()
        ).enqueue(new ResponseBehaviour(api, apiListener));
    }
}

