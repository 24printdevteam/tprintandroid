package com.tprint.android.view.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.widget.Toast;

import com.tprint.android.R;
import com.tprint.android.etc.GilaPrint;
import com.tprint.android.models.PrintModel;
import com.tprint.android.presenter.Print.PrintPageImpl;
import com.tprint.android.presenter.Print.PrintPresenter;
import com.tprint.android.presenter.Print.PrintViewImpl;
import com.tprint.android.services.API;
import com.tprint.android.services.APIResponseData;
import com.tprint.android.view.BaseActivity;
import com.tprint.android.view.fragments.PaymentFragment;
import com.tprint.android.view.fragments.PrintListFragment;
import com.tprint.android.view.fragments.UploadFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by aboot on 7/5/17.
 */

public class PrintActivity extends BaseActivity implements PrintViewImpl, PrintPageImpl {

    PrintPresenter printPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.print_activity);

        printPresenter = new PrintPresenter(this);

        printPresenter.PrintList();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.content);
        if (fragment instanceof UploadFragment) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
            getSupportFragmentManager().popBackStack();
        }else {
            finish();
        }

    }

    @Override
    public void showLoading(API api) {
        showLoadingDialog(api);
    }

    @Override
    public void hideLoading() {
        dismissLoadingDialog();
    }

    @Override
    public void UploadSuccess() {}

    @Override
    public void ListSuccess(APIResponseData response) {
        if (response.getCode() == 101) {
            PrintListPage(response.getPrintList());
        }else if (response.getCode() == 200 || response.getCode() == 300) {
            PrintUploadPage();
        }
    }

    @Override
    public void SettingSuccess() {}

    @Override
    public void PreviewSuccess(String Url) {}

    @Override
    public void RemoveSuccess() {}

    @Override
    public void RefreshAllSuccess() {}

    @Override
    public void RemoveAllSuccess() {}

    @Override
    public void ConfirmationSuccess() {}

    @Override
    public void CheckOutSuccess(APIResponseData response) {}

    @Override
    public void RequestFailed(API api, String errorMessage) {
        Toast.makeText(getApplicationContext(), errorMessage, Toast.LENGTH_SHORT).show();
    }

    private void PrintListPage(List<PrintModel> list) {
        PrintListFragment fragment = new PrintListFragment();
        Bundle bundle = new Bundle();
        if (list != null) {
            bundle.putParcelableArrayList("printList", (ArrayList<? extends Parcelable>) list);
        }
        fragment.setArguments(bundle);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.content, fragment, GilaPrint.PrintList);
        fragmentTransaction.addToBackStack(GilaPrint.PrintList);
        fragmentTransaction.commit();
    }

    private void PrintUploadPage() {
        UploadFragment fragment = new UploadFragment();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.content, fragment, GilaPrint.PrintUpload);
        fragmentTransaction.addToBackStack(GilaPrint.PrintUpload);
        fragmentTransaction.commit();
    }

    private void PrintPaymentPage(long GrandTotal) {
        PaymentFragment fragment = new PaymentFragment();
        Bundle bundle = new Bundle();
        bundle.putLong("GrandTotal", GrandTotal);
        fragment.setArguments(bundle);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.content, fragment, GilaPrint.PrintPayment);
        fragmentTransaction.addToBackStack(GilaPrint.PrintPayment);
        fragmentTransaction.commit();
    }

    @Override
    public void ChangePage(String Page, APIResponseData response) {
        if (Page.equals(GilaPrint.PrintUpload)) {
            PrintUploadPage();
        }else if (Page.equals(GilaPrint.PrintList)) {
            PrintListPage(response.getPrintList());
        }
    }

    @Override
    public void ChangePage(long GrandTotal) {
        PrintPaymentPage(GrandTotal);
    }
}
