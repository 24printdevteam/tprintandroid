package com.tprint.android.view.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;
import com.tprint.android.R;
import com.tprint.android.presenter.Storage.StoragePresenter;
import com.tprint.android.presenter.Storage.StorageViewImpl;
import com.tprint.android.presenter.StorageAdapter;
import com.tprint.android.services.API;
import com.tprint.android.services.APIResponseData;
import com.tprint.android.view.BaseActivity;
import com.tprint.android.view.dialogs.UploadDialog;
import java.io.File;
import butterknife.BindView;
import butterknife.OnClick;


/**
 * Created by aboot on 6/7/17.
 */

public class StorageActivity extends BaseActivity implements
        StorageViewImpl,
        SwipeRefreshLayout.OnRefreshListener,
        UploadDialog.OnUploadListener,
        StorageAdapter.OnClickListener {

    private boolean isChooser;
    private UploadDialog uploadDialog;
    private StorageAdapter storageAdapter;
    private StoragePresenter storagePresenter;

    @BindView(R.id.refreshLayout) SwipeRefreshLayout refreshLayout;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.btnAddNewItem) FloatingActionButton btnAddNewItem;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.storage_activity);

        isChooser = getIntent().getBooleanExtra("isChooser", false);

        getToolbar().setNavigationIcon(isChooser ? R.drawable.ic_close_white_24dp : R.drawable.ic_arrow_back_white_24dp);
        setToolbarTitle("Storage");
//        btnAddNewItem.setVisibility(isChooser ? View.GONE : View.VISIBLE);
        btnAddNewItem.setVisibility(View.GONE);


        storagePresenter = new StoragePresenter(this);
        uploadDialog = new UploadDialog();
        uploadDialog.hideStorage(true);
        uploadDialog.setOnUploadListener(this);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        storageAdapter = new StorageAdapter();
        recyclerView.setAdapter(storageAdapter);
        storageAdapter.setOnClickListener(this);
        refreshLayout.setOnRefreshListener(this);

        storagePresenter.StorageList();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (uploadDialog != null) {
            uploadDialog.onActivityResult(requestCode, resultCode, data);
        }
    }

    @OnClick(R.id.btnAddNewItem) void onAddNewItem() {
        uploadDialog.show(getSupportFragmentManager(), "Upload");
    }

    @Override
    public void showLoading(API api) {
        showLoadingDialog(api);
    }

    @Override
    public void hideLoading() {
        dismissLoadingDialog();
    }

    @Override
    public void UploadSuccess() {
        storagePresenter.StorageList();
    }

    @Override
    public void ListSuccess(APIResponseData response) {
        hideAllState();
        refreshLayout.setRefreshing(false);
        if (response.getCode() == 101) {
            if (!isFinishing()) {
                storageAdapter.addNewList(response.getPrintList());
            }
        } else if (response.getCode() == 200) {
            if (!isFinishing()) {
                showEmpty("Storage is empty");
            }
        }
    }

    @Override
    public void PrintSuccess() {
        Toast.makeText(getApplicationContext(), "Berhasil menambahkan ke printing", Toast.LENGTH_SHORT).show();
        if (isChooser) {
            Intent intent = new Intent();
            setResult(RESULT_OK, intent);
            finish();
        }
    }

    @Override
    public void RemoveSuccess() {
        storagePresenter.StorageList();
    }

    @Override
    public void RequestFailed(API api, String errorMessage) {
        Toast.makeText(getApplicationContext(), errorMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRefresh() {
        storagePresenter.StorageList();
    }

    @Override
    public void onSelected(String fileString) {
        File file = new File(fileString);
        if (file.exists()) {
            storagePresenter.StorageUpload(file);
        }
    }

    @Override
    public void onStorage() {}

    @Override
    public void onScanned(String fileString) {}

    @Override
    public void onPrint(String ID) {
        storagePresenter.StoragePrint(ID);
    }

    @Override
    public void onRemove(String ID) {
        ShowDialogRemove(ID);
    }

    private void ShowDialogRemove(final String ID) {
        AlertDialog.Builder builder = new AlertDialog.Builder(StorageActivity.this);
        builder.setTitle("Remove?")
                .setMessage("Apakah Anda yakin?")
                .setNegativeButton("Batal", null)
                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        storagePresenter.StorageRemove(ID);
                    }
                })
                .show();
    }

}