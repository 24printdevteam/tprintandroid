package com.tprint.android.view.fragments;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import com.tprint.android.R;
import com.tprint.android.database.SessionApps;
import com.tprint.android.etc.DataUtils;
import com.tprint.android.etc.GilaPrint;
import com.tprint.android.presenter.Print.PrintPresenter;
import com.tprint.android.presenter.Print.PrintViewImpl;
import com.tprint.android.presenter.Wallet.WalletPresenter;
import com.tprint.android.presenter.Wallet.WalletViewImpl;
import com.tprint.android.services.API;
import com.tprint.android.services.APIResponseData;
import com.tprint.android.view.BaseFragment;
import com.tprint.android.view.activities.HistoryDetailActivity;
import com.tprint.android.view.activities.TopUpActivity;
import com.tprint.android.view.activities.TopUpDetailActivity;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by aboot on 7/15/17.
 */

public class PaymentFragment extends BaseFragment implements PrintViewImpl, WalletViewImpl {

    private long Balance = 0;
    private long GrandTotal;
    private String PaymentMethod;
    private PrintPresenter printPresenter;
    private WalletPresenter walletPresenter;

    @BindView(R.id.btnWallet) TextView btnWallet;
    @BindView(R.id.textBalance) TextView textBalance;
    @BindView(R.id.btnTransfer) TextView btnTransfer;
    @BindView(R.id.btnVending) TextView btnVending;

    @BindView(R.id.textGrandTotal) TextView textGrandTotal;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.payment_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getToolbar().setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        setToolbarTitle(GilaPrint.PrintPayment);

        printPresenter = new PrintPresenter(this);
        walletPresenter = new WalletPresenter(this);

        Balance = SessionApps.getProfile().getBalance();

        textBalance.setText(DataUtils.getNumber(Balance));

        GrandTotal = getArguments().getLong("GrandTotal", -1);
        textGrandTotal.setText(DataUtils.getCurrency(GrandTotal));

        if (Balance < GrandTotal) {
            btnWallet.setText(R.string.top_up);
        }else {
            btnWallet.setText(R.string.bayar_via_wallet);
        }
        PaymentMethod = GilaPrint.PaymentBalance;
        setButton(1);
    }

    @OnClick(R.id.btnWallet) void onWallet() {
        if (Balance < GrandTotal) {
            walletPresenter.WalletTopUpList();
        }else {
            PaymentMethod = GilaPrint.PaymentBalance;
            setButton(1);
        }
    }

    @OnClick(R.id.btnTransfer) void onTransfer() {
        PaymentMethod = GilaPrint.PaymentTransfer;
        setButton(2);
    }

    @OnClick(R.id.btnVending) void onVending() {
        PaymentMethod = GilaPrint.PaymentOnSite;
        setButton(3);
    }

    @OnClick(R.id.btnPayment) void onPayment() {
        if (DataUtils.isNotEmpty(PaymentMethod)) {
            if (PaymentMethod.equals(GilaPrint.PaymentBalance)) {
                if (Balance < GrandTotal) {
                    Toast.makeText(getActivity(), "Saldo Anda tidak mencukupi.", Toast.LENGTH_SHORT).show();
                }else {
                    printPresenter.PrintCheckOut(PaymentMethod);
                }
            }else {
                printPresenter.PrintCheckOut(PaymentMethod);
            }
        }else {
            Toast.makeText(getActivity(), "Silahkan pilih metode pembayaran.", Toast.LENGTH_SHORT).show();
        }
    }

    private void setButton(int method) {
        int unSelectText = ContextCompat.getColor(getActivity(), R.color.colorPrimary);
        Drawable select = ContextCompat.getDrawable(getActivity(), R.drawable.button_gradient_rounded);
        Drawable unSelect = ContextCompat.getDrawable(getActivity(), R.drawable.button_outline_blue_rounded);
        if (method == 1) {
            btnWallet.setTextColor(Color.WHITE);
            btnWallet.setBackground(select);
            btnVending.setTextColor(unSelectText);
            btnVending.setBackground(unSelect);
            btnTransfer.setTextColor(unSelectText);
            btnTransfer.setBackground(unSelect);
        }else if (method == 2) {
            btnWallet.setTextColor(unSelectText);
            btnWallet.setBackground(unSelect);
            btnVending.setTextColor(unSelectText);
            btnVending.setBackground(unSelect);
            btnTransfer.setTextColor(Color.WHITE);
            btnTransfer.setBackground(select);
        }else if (method == 3) {
            btnWallet.setTextColor(unSelectText);
            btnWallet.setBackground(unSelect);
            btnVending.setTextColor(Color.WHITE);
            btnVending.setBackground(select);
            btnTransfer.setTextColor(unSelectText);
            btnTransfer.setBackground(unSelect);
        }
    }

    @Override
    public void showLoading(API api) {
        showLoadingDialog(api);
    }

    @Override
    public void hideLoading() {
        dismissLoadingDialog();
    }

    @Override
    public void UploadSuccess() {}

    @Override
    public void ListSuccess(APIResponseData response) {}

    @Override
    public void TopUpListSuccess(APIResponseData response) {
        if (response.getCode() == 101) {
            Intent intent = new Intent(getActivity(), TopUpDetailActivity.class);
            intent.putExtra("topUpModel", response.getTopUpList().get(0));
            startActivity(intent);
        }else if (response.getCode() == 200) {
            Intent intent = new Intent(getActivity(), TopUpActivity.class);
            startActivity(intent);
        }
    }

    @Override
    public void TopUpSuccess() {}

    @Override
    public void TopUpCancelSuccess() {}

    @Override
    public void SettingSuccess() {}

    @Override
    public void PreviewSuccess(String Url) {}

    @Override
    public void RemoveSuccess() {}

    @Override
    public void RefreshAllSuccess() {}

    @Override
    public void RemoveAllSuccess() {}

    @Override
    public void ConfirmationSuccess() {}

    @Override
    public void CheckOutSuccess(APIResponseData response) {
        Intent intent = new Intent(getActivity(), HistoryDetailActivity.class);
        intent.putExtra("ID", response.getCheckOutModel().getID());
        startActivity(intent);
        getActivity().finish();
    }

    @Override
    public void RequestFailed(API api, String errorMessage) {
        Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT).show();
    }
}
