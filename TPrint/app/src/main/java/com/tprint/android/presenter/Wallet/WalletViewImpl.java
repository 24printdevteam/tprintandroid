package com.tprint.android.presenter.Wallet;

import com.tprint.android.services.API;
import com.tprint.android.services.APIResponseData;

/**
 * Created by aboot on 7/17/17.
 */

public interface WalletViewImpl {

    void showLoading(API api);
    void hideLoading();
    void ListSuccess(APIResponseData response);
    void TopUpListSuccess(APIResponseData response);
    void TopUpSuccess();
    void TopUpCancelSuccess();
    void RequestFailed(API api, String errorMessage);
}
