package com.tprint.android.presenter.Credential;


import com.tprint.android.database.SessionApps;
import com.tprint.android.etc.GilaPrint;
import com.tprint.android.etc.Tools;
import com.tprint.android.models.ProfileModel;
import com.tprint.android.services.API;
import com.tprint.android.services.APIListener;
import com.tprint.android.services.APIRequest;
import com.tprint.android.services.APIResponseData;

/**
 * Created by aboot on 7/4/17.
 */

public class CredentialPresenter implements CredentialPresenterImpl {

    private CredentialViewImpl credentialView;

    public CredentialPresenter(CredentialViewImpl credentialView) {
        this.credentialView = credentialView;
    }

    @Override
    public void MeLogin(String Username, String Password) {
        credentialView.showLoading(API.Me_Login);
        new APIRequest().MeLogin(Username, Tools.sha1(Password), new APIListener() {
            @Override
            public void onSuccess(APIResponseData response) {
                SessionApps.setLogin(response.getToken(), response.getLoginStatus());
                credentialView.hideLoading();
                credentialView.LoginSuccess(response);
            }

            @Override
            public void onFailure(API api, String errorMessage) {
                credentialView.hideLoading();
                credentialView.RequestFailed(api, errorMessage);
            }
        });
    }

    @Override
    public void MeRegister(String Phone, String Email, String Name, String Password) {
        credentialView.showLoading(API.Me_Register);
        new APIRequest().MeRegister(Phone, Email, Name, Tools.sha1(Password), new APIListener() {
            @Override
            public void onSuccess(APIResponseData response) {
                SessionApps.setLogin(response.getToken(), response.getLoginStatus());
                SessionApps.setActivationTime();
                credentialView.hideLoading();
                credentialView.RegisterSuccess(response);
            }

            @Override
            public void onFailure(API api, String errorMessage) {
                credentialView.hideLoading();
                credentialView.RequestFailed(api, errorMessage);
            }
        });
    }

    @Override
    public void MeProfile() {
        credentialView.showLoading(API.Me_Profile);
        new APIRequest().MeProfile(new APIListener() {
            @Override
            public void onSuccess(APIResponseData response) {
                credentialView.hideLoading();
                ProfileModel profileModel = response.getProfileModel();
                if (profileModel != null) {
                    SessionApps.setProfile(profileModel);
                    switch (profileModel.getStatus()) {
                        case GilaPrint.Unverified:
                            credentialView.ProfileSuccess(GilaPrint.ActivationPage);
                            break;
                        case GilaPrint.Reset:
                            credentialView.ProfileSuccess(GilaPrint.SetPasswordPage);
                            break;
                        default:
                            credentialView.ProfileSuccess(GilaPrint.MainMenuPage);
                            break;
                    }
                } else {
                    credentialView.RequestFailed(API.Me_Profile, "Akun Anda bermasalah, Silahkan menghubungi Admin kami.");
                }
            }

            @Override
            public void onFailure(API api, String errorMessage) {
                credentialView.hideLoading();
                credentialView.RequestFailed(api, errorMessage);
            }
        });
    }

    @Override
    public void MeVerification(String Code) {
        credentialView.showLoading(API.Me_Verification);
        new APIRequest().MeVerification(Code, new APIListener() {
            @Override
            public void onSuccess(APIResponseData response) {
                credentialView.hideLoading();
                credentialView.VerificationSuccess();
            }

            @Override
            public void onFailure(API api, String errorMessage) {
                credentialView.hideLoading();
                credentialView.RequestFailed(api, errorMessage);
            }
        });
    }

    @Override
    public void MeResendVerification() {
        credentialView.showLoading(API.Me_ResendVerification);
        new APIRequest().MeResendVerification(new APIListener() {
            @Override
            public void onSuccess(APIResponseData response) {
                credentialView.hideLoading();
                credentialView.ResendVerificationSuccess(response.getMessage());
            }

            @Override
            public void onFailure(API api, String errorMessage) {
                credentialView.hideLoading();
                credentialView.RequestFailed(api, errorMessage);
            }
        });
    }

    @Override
    public void MeForgotPassword(final boolean isEmail, final String EmailPhone) {
        credentialView.showLoading(API.Me_ForgotPassword);
        new APIRequest().MeForgotPassword(isEmail, EmailPhone, new APIListener() {
            @Override
            public void onSuccess(APIResponseData response) {
                SessionApps.setForgot(isEmail, EmailPhone);
                credentialView.hideLoading();
                credentialView.ForgotPasswordSuccess();
            }

            @Override
            public void onFailure(API api, String errorMessage) {
                credentialView.hideLoading();
                credentialView.RequestFailed(api, errorMessage);
            }
        });
    }

    @Override
    public void MeResetPasswordCode(boolean isEmail, String EmailPhone, String Code) {
        credentialView.showLoading(API.Me_ResetPasswordCode);
        new APIRequest().MeResetPasswordCode(isEmail, EmailPhone, Code, new APIListener() {
            @Override
            public void onSuccess(APIResponseData response) {
                SessionApps.setLogin(response.getToken(), response.getLoginStatus());
                credentialView.hideLoading();
                credentialView.ResetPasswordCodeSuccess();
            }

            @Override
            public void onFailure(API api, String errorMessage) {
                credentialView.hideLoading();
                credentialView.RequestFailed(api, errorMessage);
            }
        });
    }

    @Override
    public void MeResetPassword(String Password) {
        credentialView.showLoading(API.Me_ResetPassword);
        new APIRequest().MeResetPassword(Tools.sha1(Password), new APIListener() {
            @Override
            public void onSuccess(APIResponseData response) {
                credentialView.hideLoading();
                credentialView.ResetPasswordSuccess();
            }

            @Override
            public void onFailure(API api, String errorMessage) {
                credentialView.hideLoading();
                credentialView.RequestFailed(api, errorMessage);
            }
        });
    }

    @Override
    public void MeChangePhoneNumber(String Phone) {
        credentialView.showLoading(API.Me_ChangePhoneNumber);
        new APIRequest().MeChangePhoneNumber(Phone, new APIListener() {
            @Override
            public void onSuccess(APIResponseData response) {
                credentialView.hideLoading();
                credentialView.ChangePhoneNumber();
            }

            @Override
            public void onFailure(API api, String errorMessage) {
                credentialView.hideLoading();
                credentialView.RequestFailed(api, errorMessage);
            }
        });
    }
}
