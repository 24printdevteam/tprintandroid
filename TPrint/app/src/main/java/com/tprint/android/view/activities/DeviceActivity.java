package com.tprint.android.view.activities;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.widget.Toast;

import com.tprint.android.R;
import com.tprint.android.etc.GilaPrint;
import com.tprint.android.presenter.DevicePathAdapter;
import com.tprint.android.view.BaseActivity;
import com.tprint.android.view.fragments.DeviceFragment;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by aboot on 6/7/17.
 */

public class DeviceActivity extends BaseActivity implements DeviceFragment.OnFolderSelectedListener {

    private DevicePathAdapter devicePathAdapter;
    private List<File> directoryList = new ArrayList<>();

    @BindView(R.id.recyclerView) RecyclerView recyclerView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.device_activity);

        getToolbar().setNavigationIcon(R.drawable.ic_close_white_24dp);
        setToolbarTitle("Choose a File");

        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        devicePathAdapter = new DevicePathAdapter();
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(devicePathAdapter);

        if (checkPermissionReadAndWrite()) {
            directoryList.clear();
            directoryList.add(0, Environment.getExternalStorageDirectory());
            setFragment();
        }else {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_EXTERNAL_STORAGE},
                    GilaPrint.REQUEST_PERMISSION);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            try {
                getSupportFragmentManager().popBackStack();
                directoryList.remove(directoryList.size() - 1);
                devicePathAdapter.addNewList(directoryList);
            }catch (Exception e) {
                e.printStackTrace();
            }
        }else {
            super.onBackPressed();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case GilaPrint.REQUEST_PERMISSION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    directoryList.clear();
                    directoryList.add(0, Environment.getExternalStorageDirectory());
                    setFragment();
                } else if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    boolean showRationale = ActivityCompat.shouldShowRequestPermissionRationale(this,
                            Manifest.permission.READ_EXTERNAL_STORAGE) && ActivityCompat.shouldShowRequestPermissionRationale(this,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE);
                    if (showRationale) {
                        Toast.makeText(this, "Permission Gallery Denied", Toast.LENGTH_SHORT).show();
                    } else {
                        showDialogToSettings(false);
                    }
                }
                break;
        }
    }

    @Override
    public void onFolder(File directory) {
        directoryList.add(directory);
        setFragment();
    }

    @Override
    public void onFile(File file) {
        Intent intent = new Intent();
        intent.setData(Uri.fromFile(file));
        setResult(RESULT_OK, intent);
        finish();
    }

    private void setFragment() {
        FragmentManager manager = getSupportFragmentManager();
        if (directoryList.size() <= 1) {
            manager.beginTransaction()
                    .replace(R.id.layContent, DeviceFragment.newInstance(directoryList.get(0).getPath()))
                    .commitAllowingStateLoss();
        }else {
            manager.beginTransaction()
                    .add(R.id.layContent, DeviceFragment.newInstance(directoryList.get(directoryList.size() - 1).getPath()))
                    .addToBackStack(null)
                    .commitAllowingStateLoss();
        }
        devicePathAdapter.addNewList(directoryList);
        recyclerView.scrollToPosition(directoryList.size() - 1);
    }

    private boolean checkPermissionReadAndWrite() {
        return ContextCompat.checkSelfPermission(this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this,
                        Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }

    private void showDialogToSettings(boolean isCamera) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(DeviceActivity.this);
        if (isCamera) {
            builder.setTitle("Camera Permission");
            builder.setMessage("Open Settings, then tap Permission and turn on Camera.");
        }else {
            builder.setTitle("Gallery Permission");
            builder.setMessage("Open Settings, then tap Permission and turn on Storage.");
        }
        builder.setNegativeButton("Cancel", null);
        builder.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent i = new Intent();
                i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                i.addCategory(Intent.CATEGORY_DEFAULT);
                i.setData(Uri.parse("package:" + getPackageName()));
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                startActivityForResult(i, GilaPrint.REQUEST_APPLICATION_SETTINGS);
            }
        });
        builder.show();
    }
}
