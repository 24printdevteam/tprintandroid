package com.tprint.android.view.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import com.tprint.android.R;
import com.tprint.android.etc.GilaPrint;
import com.tprint.android.presenter.Credential.CredentialPageImpl;
import com.tprint.android.view.BaseActivity;
import com.tprint.android.view.fragments.ActivationFragment;
import com.tprint.android.view.fragments.ChangePhoneFragment;
import com.tprint.android.view.fragments.ForgotCodeFragment;
import com.tprint.android.view.fragments.ForgotFragment;
import com.tprint.android.view.fragments.LoginFragment;
import com.tprint.android.view.fragments.RegisterFragment;
import com.tprint.android.view.fragments.SetPasswordFragment;

/**
 * Created by aboot on 7/4/17.
 */

public class CredentialActivity extends BaseActivity implements CredentialPageImpl {

    private boolean isActivation = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.credential_activity);

        ShowPage(getCredentialPage());
    }

    private String getCredentialPage() {
        return getIntent().getStringExtra(GilaPrint.CredentialPage);
    }

    @Override
    public void onBackPressed() {
        if (isActivation) {
            Intent intent = new Intent();
            setResult(RESULT_OK, intent);
            finish();
        }else {
            if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
                getSupportFragmentManager().popBackStack();
            }else {
                finish();
            }
        }
    }



    @Override
    public void ChangePage(String CredentialPage) {
        ShowPage(CredentialPage);
    }

    private void ShowPage(String Page) {
        switch (Page) {
            case GilaPrint.LoginPage:
                LoginPage();
                break;
            case GilaPrint.RegisterPage:
                RegisterPage();
                break;
            case GilaPrint.ActivationPage:
                isActivation = true;
                ActivationPage();
                break;
            case GilaPrint.ChangePhonePage:
                ChangePhoneNumber();
                break;
            case GilaPrint.ForgotPasswordPage:
                ForgotPassword();
                break;
            case GilaPrint.ForgotPasswordCodePage:
                isActivation = true;
                ForgotCodePassword();
                break;
            case GilaPrint.SetPasswordPage:
                isActivation = true;
                SetPasswordPage();
                break;
            case GilaPrint.MainMenuPage:
                MainMenuPage();
                break;
        }
    }

    private void LoginPage() {
        LoginFragment loginFragment = new LoginFragment();
        Bundle bundle = new Bundle();
        loginFragment.setArguments(bundle);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.content, loginFragment);
        fragmentTransaction.addToBackStack(GilaPrint.LoginPage);
        fragmentTransaction.commit();
    }

    private void RegisterPage() {
        RegisterFragment registerFragment = new RegisterFragment();
        Bundle bundle = new Bundle();
        registerFragment.setArguments(bundle);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.content, registerFragment);
        fragmentTransaction.addToBackStack(GilaPrint.RegisterPage);
        fragmentTransaction.commit();
    }

    private void ActivationPage() {
        ActivationFragment activationFragment = new ActivationFragment();
        Bundle bundle = new Bundle();
        activationFragment.setArguments(bundle);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.content, activationFragment);
        fragmentTransaction.addToBackStack(GilaPrint.ActivationPage);
        fragmentTransaction.commit();
    }

    private void ChangePhoneNumber() {
        ChangePhoneFragment changePhoneFragment = new ChangePhoneFragment();
        Bundle bundle = new Bundle();
        changePhoneFragment.setArguments(bundle);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.content, changePhoneFragment);
        fragmentTransaction.addToBackStack(GilaPrint.ChangePhonePage);
        fragmentTransaction.commit();
    }

    private void ForgotPassword() {
        ForgotFragment forgotFragment = new ForgotFragment();
        Bundle bundle = new Bundle();
        forgotFragment.setArguments(bundle);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.content, forgotFragment);
        fragmentTransaction.addToBackStack(GilaPrint.ForgotPasswordPage);
        fragmentTransaction.commit();
    }

    private void ForgotCodePassword() {
        ForgotCodeFragment forgotCodeFragment = new ForgotCodeFragment();
        Bundle bundle = new Bundle();
        forgotCodeFragment.setArguments(bundle);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.content, forgotCodeFragment);
        fragmentTransaction.addToBackStack(GilaPrint.ForgotPasswordCodePage);
        fragmentTransaction.commit();
    }

    private void SetPasswordPage() {
        SetPasswordFragment setPasswordFragment = new SetPasswordFragment();
        Bundle bundle = new Bundle();
        setPasswordFragment.setArguments(bundle);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.content, setPasswordFragment);
        fragmentTransaction.addToBackStack(GilaPrint.SetPasswordPage);
        fragmentTransaction.commit();
    }

    private void MainMenuPage() {
        Intent intent = new Intent(getApplicationContext(), MainMenuActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }
}
