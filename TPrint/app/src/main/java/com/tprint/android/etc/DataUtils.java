package com.tprint.android.etc;

import android.text.format.DateFormat;
import android.widget.EditText;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by aboot on 4/3/17.
 */
public class DataUtils {

    public static boolean isNotEmpty(String text) {
        return text != null && !text.equals("") && !text.trim().isEmpty();
    }

    public static boolean isNotEmpty(Object text) {
        return text != null && text instanceof Long && (Long)text != 0;
    }

    public static boolean isNotEmpty(EditText text) {
        return text != null && !text.getText().toString().equals("") && !text.getText().toString().trim().isEmpty();
    }

    public static boolean isNotEmpty(TextView text) {
        return text != null && !text.getText().toString().equals("") && !text.getText().toString().trim().isEmpty();
    }

    public static boolean isValidEmail(EditText text) {
        return text.getText() != null && android.util.Patterns.EMAIL_ADDRESS.matcher(text.getText()).matches();
    }

    public static boolean isNumberAndAlphabet(String s) {
        String n = ".*[0-9].*";
        String a = ".*[A-Z].*";
        return s.matches(n) && s.matches(a);
    }

    public static String getCurrency(long money) {
        Locale locale = new Locale("id", "ID");
        NumberFormat currencyFormatter = NumberFormat.getIntegerInstance(locale);
        return "Rp "+currencyFormatter.format(money);
    }

    public static String getNumber(long money) {
        Locale locale = new Locale("id", "ID");
        NumberFormat currencyFormatter = NumberFormat.getIntegerInstance(locale);
        return ""+currencyFormatter.format(money);
    }

    public static String getDateServer(long time) {
        Calendar cal = Calendar.getInstance(Locale.getDefault());
        cal.setTimeInMillis(time*1000);
        return DateFormat.format("dd MMMM yyyy", cal).toString();
    }

    public static String getDateMillis(long time) {
        Calendar cal = Calendar.getInstance(Locale.getDefault());
        cal.setTimeInMillis(time);
        return DateFormat.format("dd MMMM yyyy", cal).toString();
    }

    public static String getFileSize(long size) {
        DecimalFormat df = new DecimalFormat("0.00");
        float sizeKb = 1024.0f;
        float sizeMo = sizeKb * sizeKb;
        float sizeGb = sizeMo * sizeKb;
        float sizeTerra = sizeGb * sizeKb;
        if (size < sizeKb) {
            return df.format(size)+ " B";
        }else if(size < sizeMo)
            return df.format(size / sizeKb)+ " KB";
        else if(size < sizeGb)
            return df.format(size / sizeMo) + " MB";
        else if(size < sizeTerra)
            return df.format(size / sizeGb) + " GB";

        return "";
    }

}
