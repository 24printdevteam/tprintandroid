package com.tprint.android.view.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;
import com.tprint.android.R;
import com.tprint.android.etc.DataUtils;
import com.tprint.android.etc.GilaPrint;
import com.tprint.android.etc.Tools;
import com.tprint.android.presenter.Credential.CredentialPageImpl;
import com.tprint.android.presenter.Credential.CredentialPresenter;
import com.tprint.android.presenter.Credential.CredentialViewImpl;
import com.tprint.android.services.API;
import com.tprint.android.services.APIResponseData;
import com.tprint.android.view.BaseFragment;
import butterknife.BindView;
import butterknife.OnClick;


/**
 * Created by aboot on 7/4/17.
 */

public class ForgotFragment extends BaseFragment implements CredentialViewImpl {

    private CredentialPageImpl credentialPage;
    private CredentialPresenter credentialPresenter;

    @BindView(R.id.inputForgot) EditText inputForgot;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (!(context instanceof CredentialPageImpl)) {
            throw new ClassCastException("Activity must implement CredentialPageImpl");
        }
        this.credentialPage = (CredentialPageImpl) context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.forgot_fragment, container, false);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getToolbar().setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        setToolbarTitle(GilaPrint.ForgotPasswordPage);

        credentialPresenter = new CredentialPresenter(this);
    }

    @OnClick(R.id.btnSend) void onSend() {
        if (DataUtils.isNotEmpty(inputForgot)) {
            if (DataUtils.isValidEmail(inputForgot)) {
                credentialPresenter.MeForgotPassword(true, inputForgot.getText().toString());
            }else {
                if (isPhone(inputForgot.getText().toString())) {
                    if (Tools.isValidNumber(inputForgot.getText().toString())) {
                        credentialPresenter.MeForgotPassword(false, inputForgot.getText().toString());
                    }else {
                        Toast.makeText(getActivity(),
                                R.string.validate_number_valid, Toast.LENGTH_SHORT)
                                .show();
                    }
                }else {
                    Toast.makeText(getActivity(),
                            R.string.validate_email_insert, Toast.LENGTH_SHORT).show();
                }
            }
        }else {
            Toast.makeText(getActivity(),
                    R.string.validate_forgot, Toast.LENGTH_SHORT).show();
        }
    }

    public static boolean isPhone(String str) {
        try {
            Log.i("isNumber", ""+Long.parseLong(str));
        } catch(NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    @Override
    public void showLoading(API api) {
        showLoadingDialog(api);
    }

    @Override
    public void hideLoading() {
        dismissLoadingDialog();
    }

    @Override
    public void LoginSuccess(APIResponseData response) {}

    @Override
    public void RegisterSuccess(APIResponseData response) {}

    @Override
    public void ProfileSuccess(String ChangePage) {}

    @Override
    public void VerificationSuccess() {}

    @Override
    public void ResendVerificationSuccess(String Message) {}

    @Override
    public void ForgotPasswordSuccess() {
        credentialPage.ChangePage(GilaPrint.ForgotPasswordCodePage);
    }

    @Override
    public void ResetPasswordCodeSuccess() {}

    @Override
    public void ResetPasswordSuccess() {}

    @Override
    public void ChangePhoneNumber() {}

    @Override
    public void RequestFailed(API api, String errorMessage) {
        Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT).show();
    }
}
