package com.tprint.android.view.dialogs;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import com.norbsoft.typefacehelper.TypefaceHelper;
import com.tprint.android.R;
import com.tprint.android.etc.GilaPrint;
import com.tprint.android.view.activities.DeviceActivity;
import com.tprint.android.view.activities.StorageActivity;

import butterknife.ButterKnife;
import static android.app.Activity.RESULT_OK;

/**
 * Created by aboot on 12/18/16.
 */

public class UploadDialog extends BottomSheetDialogFragment
        implements View.OnClickListener {

    private boolean hideStorage;
    private OnUploadListener onUploadListener;

    public void hideStorage(boolean storage) {
        this.hideStorage = storage;
    }

    public void setOnUploadListener(OnUploadListener onUploadListener) {
        this.onUploadListener = onUploadListener;
    }

    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        View contentView = View.inflate(getContext(), R.layout.upload_dialog, null);
        dialog.setContentView(contentView);
        ButterKnife.bind(contentView);
        TypefaceHelper.typeface(contentView);

        FrameLayout layUploadFile = contentView.findViewById(R.id.layUploadFile);
        FrameLayout layUploadStorage = contentView.findViewById(R.id.layUploadStorage);
        FrameLayout layUploadGoogleDrive = contentView.findViewById(R.id.layUploadGoogleDrive);
        FrameLayout layUploadDropbox = contentView.findViewById(R.id.layUploadDropbox);
        FrameLayout layUploadScanDocument = contentView.findViewById(R.id.layUploadScanDocument);

        layUploadGoogleDrive.setVisibility(View.GONE);
        layUploadDropbox.setVisibility(View.GONE);
        layUploadStorage.setVisibility(hideStorage ? View.GONE : View.VISIBLE);
        layUploadScanDocument.setVisibility(View.GONE);

        layUploadFile.setOnClickListener(this);
        layUploadStorage.setOnClickListener(this);
        layUploadGoogleDrive.setOnClickListener(this);
        layUploadDropbox.setOnClickListener(this);
        layUploadScanDocument.setOnClickListener(this);

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams)
                ((View) contentView.getParent()).getLayoutParams();
        CoordinatorLayout.Behavior behavior = params.getBehavior();

        if( behavior != null && behavior instanceof BottomSheetBehavior ) {
            ((BottomSheetBehavior) behavior).setBottomSheetCallback(bottomSheetCallback);
        }
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        try {
            FragmentTransaction ft = manager.beginTransaction();
            ft.add(this, tag);
            ft.commitAllowingStateLoss();
        } catch (IllegalStateException e) {
            Log.e("DialogShow", "Exception", e);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == GilaPrint.REQUEST_UPLOADFILE && resultCode == RESULT_OK) {
            Uri uri = data.getData();
            if (onUploadListener != null) onUploadListener.onSelected(uri.getPath());
            dismiss();
        }else if (requestCode == GilaPrint.REQUEST_STORAGE && resultCode == RESULT_OK) {
            if (onUploadListener != null) onUploadListener.onStorage();
            dismiss();
        }else if (requestCode == GilaPrint.REQUEST_SCAN && resultCode == RESULT_OK) {
            Uri uri = data.getData();
            if (onUploadListener != null) onUploadListener.onScanned(uri.getPath());
            dismiss();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private BottomSheetBehavior.BottomSheetCallback bottomSheetCallback = new BottomSheetBehavior.BottomSheetCallback() {

        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                dismiss();
            }
        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {}
    };

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.layUploadFile:
                Intent intent = new Intent(getActivity(), DeviceActivity.class);
                startActivityForResult(intent, GilaPrint.REQUEST_UPLOADFILE);
                break;
            case R.id.layUploadStorage:
                Intent storageIntent = new Intent(getActivity(), StorageActivity.class);
                storageIntent.putExtra("isChooser", true);
                startActivityForResult(storageIntent, GilaPrint.REQUEST_STORAGE);
                break;
        }
    }

    public interface OnUploadListener {
        void onSelected(String fileString);
        void onStorage();
        void onScanned(String fileString);
    }

}
