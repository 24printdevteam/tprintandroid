package com.tprint.android.presenter.Print;

import com.tprint.android.models.PrintModel;

import java.io.File;
import java.util.List;

/**
 * Created by aboot on 7/5/17.
 */

public interface PrintPresenterImpl {

    void PrintUpload(List<File> list);
    void PrintList();
    void PrintSetting(PrintModel printModel, int PrintReceipt);
    void PrintPreview(String ID, int Color, int PageFrom, int PageTo);
    void PrintRemove(String ID);
    void PrintRefreshAll();
    void PrintRemoveAll();
    void PrintConfirmation();
    void PrintCheckOut(String Method);
}
