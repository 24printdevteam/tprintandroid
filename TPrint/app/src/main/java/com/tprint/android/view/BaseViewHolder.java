package com.tprint.android.view;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.norbsoft.typefacehelper.TypefaceHelper;

import butterknife.ButterKnife;

/**
 * Created by aboot on 9/14/16.
 */
public class BaseViewHolder extends RecyclerView.ViewHolder {

    public BaseViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        TypefaceHelper.typeface(itemView);
    }
}
