package com.tprint.android.presenter.Storage;

import java.io.File;

/**
 * Created by aboot on 7/13/17.
 */

public interface StoragePresenterImpl {

    void StorageUpload(File file);
    void StorageList();
    void StoragePrint(String IDs);
    void StorageRemove(String ID);

}
