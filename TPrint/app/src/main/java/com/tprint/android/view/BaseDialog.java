package com.tprint.android.view;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import com.norbsoft.typefacehelper.TypefaceHelper;
import com.tprint.android.etc.ScreenUtils;

import butterknife.ButterKnife;

/**
 * Created by aboot on 9/17/16.
 */
public class BaseDialog extends DialogFragment {

    View view;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.view = view;
        ButterKnife.bind(this, view);
        TypefaceHelper.typeface(view);

        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isAdded()) {
            ViewGroup.LayoutParams params = getDialog().getWindow().getAttributes();
            params.width = ScreenUtils.getScreenWidth() - (ScreenUtils.getScreenWidth() / 5);
            params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
            getDialog().getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        try {
            FragmentTransaction ft = manager.beginTransaction();
            ft.add(this, tag);
            ft.commitAllowingStateLoss();
        } catch (IllegalStateException e) {
            Log.e("DialogShow", "Exception", e);
        }
    }

    public void dismissDialog() {
        try {
            dismissAllowingStateLoss();
        } catch (Exception e) {
            Log.e("DialogDismis", "Exception", e);
        }

    }
}
