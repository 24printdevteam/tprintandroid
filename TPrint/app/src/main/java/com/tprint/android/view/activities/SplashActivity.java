package com.tprint.android.view.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.widget.Toast;
import com.tprint.android.R;
import com.tprint.android.database.SessionApps;
import com.tprint.android.etc.DataUtils;
import com.tprint.android.etc.GilaPrint;
import com.tprint.android.presenter.Credential.CredentialPresenter;
import com.tprint.android.presenter.Credential.CredentialViewImpl;
import com.tprint.android.services.API;
import com.tprint.android.services.APIResponseData;
import com.tprint.android.view.BaseActivity;


/**
 * Created by aboot on 4/3/17.
 */

public class SplashActivity extends BaseActivity implements CredentialViewImpl {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_activity);

        final CredentialPresenter credentialPresenter = new CredentialPresenter(this);


        if (DataUtils.isNotEmpty(SessionApps.getTokenApps())) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    credentialPresenter.MeProfile();
                }
            }, 1500);
        }else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent intent = new Intent(getApplicationContext(), SlideActivity.class);
                    startActivity(intent);
                    finish();
                }
            }, 5000);

        }
    }

    @Override
    public void showLoading(API api) {
        showLoadingDialog(api);
    }

    @Override
    public void hideLoading() {
        dismissLoadingDialog();
    }

    @Override
    public void LoginSuccess(APIResponseData response) {}

    @Override
    public void RegisterSuccess(APIResponseData response) {}

    @Override
    public void ProfileSuccess(String ChangePage) {
        Intent intent;
        switch (ChangePage) {
            case GilaPrint.ActivationPage:
                intent = new Intent(getApplicationContext(), CredentialActivity.class);
                intent.putExtra(GilaPrint.CredentialPage, GilaPrint.ActivationPage);
                startActivityForResult(intent, GilaPrint.REQUEST_CREDENTIAL);
                finish();
                break;
            case GilaPrint.SetPasswordPage:
                intent = new Intent(getApplicationContext(), CredentialActivity.class);
                intent.putExtra(GilaPrint.CredentialPage, GilaPrint.SetPasswordPage);
                startActivityForResult(intent, GilaPrint.REQUEST_CREDENTIAL);
                finish();
                break;
            default:
                intent = new Intent(getApplicationContext(), MainMenuActivity.class);
                startActivity(intent);
                finish();
                break;
        }
    }

    @Override
    public void VerificationSuccess() {}

    @Override
    public void ResendVerificationSuccess(String Message) {}

    @Override
    public void ForgotPasswordSuccess() {}

    @Override
    public void ResetPasswordCodeSuccess() {}

    @Override
    public void ResetPasswordSuccess() {}

    @Override
    public void ChangePhoneNumber() {}

    @Override
    public void RequestFailed(API api, String errorMessage) {
        Toast.makeText(getApplicationContext(), errorMessage, Toast.LENGTH_SHORT).show();
    }
}