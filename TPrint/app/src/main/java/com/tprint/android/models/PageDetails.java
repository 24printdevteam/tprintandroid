package com.tprint.android.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by aboot on 7/5/17.
 */

public class PageDetails implements Parcelable {

    private int Page;
    private String Coverage;
    private String Size;


    protected PageDetails(Parcel in) {
        Page = in.readInt();
        Coverage = in.readString();
        Size = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(Page);
        dest.writeString(Coverage);
        dest.writeString(Size);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<PageDetails> CREATOR = new Creator<PageDetails>() {
        @Override
        public PageDetails createFromParcel(Parcel in) {
            return new PageDetails(in);
        }

        @Override
        public PageDetails[] newArray(int size) {
            return new PageDetails[size];
        }
    };

    public int getPage() {
        return Page;
    }

    public void setPage(int page) {
        Page = page;
    }

    public String getCoverage() {
        return Coverage;
    }

    public void setCoverage(String coverage) {
        Coverage = coverage;
    }

    public String getSize() {
        return Size;
    }

    public void setSize(String size) {
        Size = size;
    }
}
