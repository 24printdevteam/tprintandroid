package com.tprint.android.view.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.tprint.android.R;
import com.tprint.android.etc.ScreenUtils;
import com.tprint.android.presenter.BannerAdapter;
import com.tprint.android.view.BaseFragment;
import com.tprint.android.view.activities.LocationActivity;
import com.tprint.android.view.activities.PrintActivity;
import com.tprint.android.view.activities.StorageActivity;
import com.tprint.android.view.dialogs.UploadDialog;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by aboot on 5/12/17.
 */

public class HomeFragment extends BaseFragment implements BannerAdapter.OnClickListener {

    private int currentPage = 0;
    private int NUM_PAGES = 8;

    private UploadDialog uploadDialog;

    @BindView(R.id.layBanner) RelativeLayout layBanner;
    @BindView(R.id.viewPager) ViewPager viewPager;
    @BindView(R.id.circePageIndicator) CirclePageIndicator circlePageIndicator;
    @BindView(R.id.layContinue) LinearLayout layContinue;

    public static HomeFragment newInstance() {
        Bundle args = new Bundle();
        HomeFragment fragment = new HomeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.home_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getToolbar();
        showToolbarImage();
        viewPager.setAdapter(new BannerAdapter(getActivity(), this));
        layBanner.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                (int) ScreenUtils.getBanner()));
        circlePageIndicator.setViewPager(viewPager);
        uploadDialog = new UploadDialog();
        autoScroll();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (uploadDialog != null && uploadDialog.isVisible()) uploadDialog.onActivityResult(requestCode, resultCode, data);
    }

    @OnClick(R.id.layContinue) void onContinue() {
        Intent intent = new Intent(getActivity(), PrintActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.layPrintings) void onPrintings() {
        Intent intent = new Intent(getActivity(), PrintActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.layStorage) void onStorage() {
        Intent intent = new Intent(getActivity(), StorageActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.layLocation) void onLocation() {
        Intent intent = new Intent(getActivity(), LocationActivity.class);
        startActivity(intent);
    }

    @Override
    public void onClick(int Position) {
        if (Position == 3) {
            Intent intent = new Intent(getActivity(), LocationActivity.class);
            startActivity(intent);
        }
    }

    private void autoScroll() {
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES-1) {
                    currentPage = 0;
                }
                viewPager.setCurrentItem(currentPage++, true);
            }
        };

        Timer timer = new Timer();
        timer.schedule(new TimerTask() {

            @Override
            public void run() {
                handler.post(Update);
            }
        }, 500, 3000);
    }
}
