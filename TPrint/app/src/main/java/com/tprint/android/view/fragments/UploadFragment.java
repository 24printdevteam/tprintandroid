package com.tprint.android.view.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.tprint.android.R;
import com.tprint.android.database.SessionApps;
import com.tprint.android.etc.GilaPrint;
import com.tprint.android.presenter.Print.PrintPageImpl;
import com.tprint.android.presenter.Print.PrintPresenter;
import com.tprint.android.presenter.Print.PrintViewImpl;
import com.tprint.android.presenter.UploadAdapter;
import com.tprint.android.services.API;
import com.tprint.android.services.APIResponseData;
import com.tprint.android.view.BaseFragment;
import com.tprint.android.view.dialogs.UploadDialog;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by aboot on 7/7/17.
 */

public class UploadFragment extends BaseFragment implements
        UploadDialog.OnUploadListener,
        UploadAdapter.OnUploadListener, PrintViewImpl {

    private UploadDialog uploadDialog;
    private UploadAdapter uploadAdapter;
    private PrintPresenter printPresenter;
    private PrintPageImpl printPage;

    @BindView(R.id.recyclerView) RecyclerView recyclerView;
    @BindView(R.id.layUpload) View layUpload;
    @BindView(R.id.btnUpload) TextView btnUpload;
    @BindView(R.id.layEmptyUpload) LinearLayout layEmptyUpload;
    @BindView(R.id.layVerification) LinearLayout layVerification;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (!(context instanceof PrintPageImpl)) {
            throw new ClassCastException("Activity must implement PrintPageImpl");
        }
        this.printPage = (PrintPageImpl) context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.upload_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getToolbar().setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        setToolbarTitle(GilaPrint.PrintUpload);

        printPresenter = new PrintPresenter(this);

        uploadDialog = new UploadDialog();
        uploadDialog.setOnUploadListener(this);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        uploadAdapter = new UploadAdapter();
        uploadAdapter.setOnUploadListener(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(uploadAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (uploadDialog != null && !uploadDialog.isAdded()) {
            uploadDialog.show(getFragmentManager(), "Upload");
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (uploadDialog != null) {
            uploadDialog.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onAddFile() {
        uploadDialog.show(getFragmentManager(), "Upload");
    }

    @Override
    public void onOpenFile(File file) {}

    @Override
    public void onRemove(int Position) {
        uploadAdapter.removeFile(Position);
        validateFile();
    }

    @OnClick(R.id.layEmptyUpload) void onAddItem() {
        uploadDialog.show(getFragmentManager(), "Upload");
    }

    @OnClick(R.id.btnUpload) void onUpload() {
        SessionApps.setUploadStatus(1);
        printPresenter.PrintUpload(SessionApps.getUploadList());
        validateFile();
    }

    @Override
    public void onSelected(String fileString) {
        File file = new File(fileString);
        if (file.exists()) {
            if (SessionApps.setUpload(file)) {
                uploadAdapter.addList(file);
            }
            validateFile();
        }
    }

    @Override
    public void onStorage() {
        printPresenter.PrintList();
    }

    @Override
    public void onScanned(String fileString) {}

    private void validateFile() {
        List<File> list = SessionApps.getUploadList();
        if (list == null) list = new ArrayList<>();
        List<File> newList = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            File file = list.get(i);
            if (file.exists()) {
                newList.add(list.get(i));
            }
        }
        SessionApps.setUploadList(newList);
        uploadAdapter.addNewList(newList);
        if (list.size() == 0) {
            uploadDialog.show(getFragmentManager(), "Upload");
        }
        btnUpload.setText("Upload File ("+newList.size()+")");
        layUpload.setVisibility(SessionApps.getUploadStatus() == 0 ?
                (newList.size() > 0 ? View.VISIBLE : View.GONE) : View.GONE);
        layEmptyUpload.setVisibility(newList.size() > 0 ? View.GONE : View.VISIBLE);
        layVerification.setVisibility(View.GONE);
    }

    @Override
    public void showLoading(API api) {}

    @Override
    public void hideLoading() {}

    @Override
    public void UploadSuccess() {
        SessionApps.setUploadStatus(2);
        validateFile();
        printPresenter.PrintList();
    }

    @Override
    public void ListSuccess(APIResponseData response) {
        if (response.getCode() == 101) {
            SessionApps.setUploadStatus(0);
            SessionApps.setUploadList(null);
            if (isAdded()) {
                printPage.ChangePage(GilaPrint.PrintList, response);
            }
        }else if (response.getCode() == 300) {
            SessionApps.setUploadStatus(2);
            validateFile();
            List<File> list = SessionApps.getUploadList();
            if (list == null) {
                layVerification.setVisibility(View.VISIBLE);
            }else {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        printPresenter.PrintList();
                    }
                }, 3000);
            }
        }else if (response.getCode() == 310) {
            ShodDialog310();
        }
    }

    @Override
    public void SettingSuccess() {}

    @Override
    public void PreviewSuccess(String Url) {}

    @Override
    public void RemoveSuccess() {}

    @Override
    public void RefreshAllSuccess() {
        printPresenter.PrintList();
    }

    @Override
    public void RemoveAllSuccess() {
        printPresenter.PrintList();
    }

    @Override
    public void ConfirmationSuccess() {}

    @Override
    public void CheckOutSuccess(APIResponseData response) {}

    @Override
    public void RequestFailed(API api, String errorMessage) {
        Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT).show();
    }

    private void ShodDialog310() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage("Are you sure?")
                .setPositiveButton("Proses ulang", dialogClickListener)
                .setNegativeButton("Hapus semua berkas", dialogClickListener)
                .show();
    }

    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which){
                case DialogInterface.BUTTON_POSITIVE:
                    printPresenter.PrintRefreshAll();
                    break;

                case DialogInterface.BUTTON_NEGATIVE:
                    printPresenter.PrintRemoveAll();
                    break;
            }
        }
    };
}
