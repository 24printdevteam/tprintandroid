package com.tprint.android.notification;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.tprint.android.R;
import com.tprint.android.etc.GilaPrint;
import com.tprint.android.view.activities.MainMenuActivity;

/**
 * Created by aboot on 7/5/17.
 */

public class PrintGilaMessaging extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        setupNotification();
    }

    private void setupNotification() {
        final NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this);
        notificationBuilder.setSmallIcon(R.drawable.ico_home_prints);
        notificationBuilder.setContentText("24Print Notification");
        notificationBuilder.setAutoCancel(true);
        notificationBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText("24Print Notification Big Text"));
        long[] vibrate = {500, 500};
        notificationBuilder.setVibrate(vibrate);
        notificationBuilder.setColor(ContextCompat.getColor(this, R.color.colorPrimary));
        Uri defaultSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        notificationBuilder.setSound(defaultSound);

        Intent intent = new Intent(this, MainMenuActivity.class);
        PendingIntent openPending = PendingIntent.getService(this, GilaPrint.REQUEST_NOTIFICATION, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        notificationBuilder.setContentIntent(openPending);
        notificationBuilder.setPriority(NotificationCompat.PRIORITY_HIGH);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notificationBuilder.build());
    }
}
