package com.tprint.android.presenter.Credential;


import com.tprint.android.services.API;
import com.tprint.android.services.APIResponseData;

/**
 * Created by aboot on 7/4/17.
 */

public interface CredentialViewImpl {

    void showLoading(API api);
    void hideLoading();
    void LoginSuccess(APIResponseData response);
    void RegisterSuccess(APIResponseData response);
    void ProfileSuccess(String ChangePage);
    void VerificationSuccess();
    void ResendVerificationSuccess(String Message);
    void ForgotPasswordSuccess();
    void ResetPasswordCodeSuccess();
    void ResetPasswordSuccess();
    void ChangePhoneNumber();
    void RequestFailed(API api, String errorMessage);
}
