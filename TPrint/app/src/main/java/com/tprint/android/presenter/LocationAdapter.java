package com.tprint.android.presenter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tprint.android.R;
import com.tprint.android.etc.Tools;
import com.tprint.android.models.LocationModel;
import com.tprint.android.view.BaseViewHolder;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by aboot on 5/29/17.
 */

public class LocationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<LocationModel> list = new ArrayList<>();
    private OnMapsListener onMapsListener;

    public void addNewList(List<LocationModel> list) {
        this.list.clear();
        if (list != null) {
            this.list.addAll(list);
        }
        notifyDataSetChanged();
    }

    public void setOnMapsListener(OnMapsListener onMapsListener) {
        this.onMapsListener = onMapsListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.location_item_viewholder, parent, false);
        return new ItemHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ItemHolder) {
            final ItemHolder itemHolder = (ItemHolder) holder;
            final LocationModel model = list.get(position);

            itemHolder.textLocationName.setText(model.getName());
            itemHolder.textLocationDetail.setText(model.getDetails());
            itemHolder.textLocationAddress.setText(model.getAddress());

            itemHolder.textOpenClose.setText(Tools.getOpenClose(model));

            itemHolder.textDay0.setText(Tools.getDay(0));
            itemHolder.textHours0.setText(Tools.getHours(model, 0));
            itemHolder.textDay1.setText(Tools.getDay(1));
            itemHolder.textHours1.setText(Tools.getHours(model, 1));
            itemHolder.textDay2.setText(Tools.getDay(2));
            itemHolder.textHours2.setText(Tools.getHours(model, 2));
            itemHolder.textDay3.setText(Tools.getDay(3));
            itemHolder.textHours3.setText(Tools.getHours(model, 3));
            itemHolder.textDay4.setText(Tools.getDay(4));
            itemHolder.textHours4.setText(Tools.getHours(model, 4));
            itemHolder.textDay5.setText(Tools.getDay(5));
            itemHolder.textHours5.setText(Tools.getHours(model, 5));
            itemHolder.textDay6.setText(Tools.getDay(6));
            itemHolder.textHours6.setText(Tools.getHours(model, 6));





            itemHolder.layDayHeader.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemHolder.layDayHeader.setVisibility(View.GONE);
                    itemHolder.layDayDetail.setVisibility(View.VISIBLE);
                }
            });
            itemHolder.layDayDetail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemHolder.layDayHeader.setVisibility(View.VISIBLE);
                    itemHolder.layDayDetail.setVisibility(View.GONE);
                }
            });
            itemHolder.btnDirections.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onMapsListener != null) onMapsListener.onDirection(model.getLatitude(),
                            model.getLongitude(), model.getName());
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ItemHolder extends BaseViewHolder {

        @BindView(R.id.textLocationName) TextView textLocationName;
        @BindView(R.id.layDayHeader) LinearLayout layDayHeader;
        @BindView(R.id.textOpenClose) TextView textOpenClose;

        @BindView(R.id.layDayDetail) LinearLayout layDayDetail;
        @BindView(R.id.textDay0) TextView textDay0;
        @BindView(R.id.textHours0) TextView textHours0;
        @BindView(R.id.textDay1) TextView textDay1;
        @BindView(R.id.textHours1) TextView textHours1;
        @BindView(R.id.textDay2) TextView textDay2;
        @BindView(R.id.textHours2) TextView textHours2;
        @BindView(R.id.textDay3) TextView textDay3;
        @BindView(R.id.textHours3) TextView textHours3;
        @BindView(R.id.textDay4) TextView textDay4;
        @BindView(R.id.textHours4) TextView textHours4;
        @BindView(R.id.textDay5) TextView textDay5;
        @BindView(R.id.textHours5) TextView textHours5;
        @BindView(R.id.textDay6) TextView textDay6;
        @BindView(R.id.textHours6) TextView textHours6;

        @BindView(R.id.textLocationDetail) TextView textLocationDetail;
        @BindView(R.id.textLocationAddress) TextView textLocationAddress;
        @BindView(R.id.btnDirections) TextView btnDirections;

        public ItemHolder(View itemView) {
            super(itemView);
        }
    }

    public interface OnMapsListener {
        void onDirection(double latitude, double longitude, String label);
    }
}
