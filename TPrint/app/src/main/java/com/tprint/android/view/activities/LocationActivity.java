package com.tprint.android.view.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.tprint.android.R;
import com.tprint.android.presenter.LocationAdapter;
import com.tprint.android.services.API;
import com.tprint.android.services.APIListener;
import com.tprint.android.services.APIRequest;
import com.tprint.android.services.APIResponseData;
import com.tprint.android.view.BaseActivity;

import butterknife.BindView;

/**
 * Created by aboot on 5/29/17.
 */

public class LocationActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener, LocationAdapter.OnMapsListener {

    private LocationAdapter locationAdapter;

    @BindView(R.id.refreshLayout) SwipeRefreshLayout refreshLayout;
    @BindView(R.id.recyclerView) RecyclerView recyclerView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.location_acitivity);
        getToolbar().setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        setToolbarTitle("Location");

        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        locationAdapter = new LocationAdapter();
        locationAdapter.setOnMapsListener(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(locationAdapter);
        refreshLayout.setOnRefreshListener(this);
        loadList();
    }

    @Override
    public void onRefresh() {
        loadList();
    }

    @Override
    public void onDirection(double latitude, double longitude, String label) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:"+latitude+","+longitude+"?q="+
                latitude+","+longitude+"("+label+")"));
        startActivity(intent);
    }

    private void loadList() {
        new APIRequest().ServerLocationList(new APIListener() {
            @Override
            public void onSuccess(APIResponseData response) {
                refreshLayout.setRefreshing(false);
                hideAllState();
                if (response.getCode() == 101) {
                    if (response.getLocationList() != null) {
                        locationAdapter.addNewList(response.getLocationList());
                    }else {
                        showEmpty("Location list empty");
                    }
                }
            }

            @Override
            public void onFailure(API api, String errorMessage) {
                Toast.makeText(getApplicationContext(), errorMessage, Toast.LENGTH_SHORT).show();
            }
        });
    }

}
