package com.tprint.android.view.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.tprint.android.R;
import com.tprint.android.database.SessionApps;
import com.tprint.android.etc.DataUtils;
import com.tprint.android.etc.GilaPrint;
import com.tprint.android.presenter.Wallet.WalletPresenter;
import com.tprint.android.presenter.Wallet.WalletViewImpl;
import com.tprint.android.services.API;
import com.tprint.android.services.APIResponseData;
import com.tprint.android.view.BaseActivity;
import com.tprint.android.view.customs.MoneyTextWatcher;
import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by aboot on 5/23/17.
 */

public class TopUpActivity extends BaseActivity implements WalletViewImpl {

    private WalletPresenter walletPresenter;

    @BindView(R.id.textBalance) TextView textBalance;
    @BindView(R.id.inputAmount) EditText inputAmount;
    @BindView(R.id.spinnerMethod) Spinner spinnerMethod;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.topup_activity);
        getToolbar().setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        setToolbarTitle(GilaPrint.TopUpPage);

        walletPresenter = new WalletPresenter(this);


        inputAmount.addTextChangedListener(new MoneyTextWatcher(inputAmount));
    }

    @Override
    protected void onResume() {
        super.onResume();
        textBalance.setText(DataUtils.getNumber(SessionApps.getProfile().getBalance()));
    }

    @OnClick(R.id.btnTopUp) void onTopUp() {
        if (DataUtils.isNotEmpty(inputAmount)) {
            walletPresenter.WalletTopUp((Long) inputAmount.getTag(), spinnerMethod.getSelectedItem().toString());
        }
    }

    @Override
    public void showLoading(API api) {
        showLoadingDialog(api);
    }

    @Override
    public void hideLoading() {
        dismissLoadingDialog();
    }

    @Override
    public void ListSuccess(APIResponseData response) {}

    @Override
    public void TopUpListSuccess(APIResponseData response) {
        if (response.getCode() == 101) {
            Intent intent = new Intent(getApplicationContext(), TopUpDetailActivity.class);
            intent.putExtra("topUpModel", response.getTopUpList().get(0));
            startActivity(intent);
            finish();
        }else {
            finish();
        }
    }

    @Override
    public void TopUpSuccess() {
        walletPresenter.WalletTopUpList();
    }

    @Override
    public void TopUpCancelSuccess() {}

    @Override
    public void RequestFailed(API api, String errorMessage) {
        Toast.makeText(getApplicationContext(), errorMessage, Toast.LENGTH_SHORT).show();
    }
}
