package com.tprint.android.view.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;
import com.tprint.android.R;
import com.tprint.android.database.SessionApps;
import com.tprint.android.etc.DataUtils;
import com.tprint.android.etc.GilaPrint;
import com.tprint.android.presenter.Credential.CredentialPageImpl;
import com.tprint.android.presenter.Credential.CredentialPresenter;
import com.tprint.android.presenter.Credential.CredentialViewImpl;
import com.tprint.android.services.API;
import com.tprint.android.services.APIResponseData;
import com.tprint.android.view.BaseFragment;
import butterknife.BindView;

/**
 * Created by aboot on 7/4/17.
 */

public class ForgotCodeFragment extends BaseFragment implements CredentialViewImpl {

    private boolean isEmail;
    private String EmailPhone;
    private CredentialPageImpl credentialPage;
    private CredentialPresenter credentialPresenter;

    @BindView(R.id.inputCode1) EditText inputCode1;
    @BindView(R.id.inputCode2) EditText inputCode2;
    @BindView(R.id.inputCode3) EditText inputCode3;
    @BindView(R.id.inputCode4) EditText inputCode4;
    @BindView(R.id.inputCode5) EditText inputCode5;
    @BindView(R.id.inputCode6) EditText inputCode6;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (!(context instanceof CredentialPageImpl)) {
            throw new ClassCastException("Activity must implement CredentialPageImpl");
        }
        this.credentialPage = (CredentialPageImpl) context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.forgot_code_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getToolbar().setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        setToolbarTitle(GilaPrint.ForgotPasswordCodePage);

        credentialPresenter = new CredentialPresenter(this);

        isEmail = SessionApps.getForgotIsEmail();
        EmailPhone = SessionApps.getForgotEmailPhone();

        CodeEngine();
    }

    private void CodeEngine() {
        inputCode1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    inputCode2.requestFocus();
                    checkVerificationCode();
                }
            }
        });

        inputCode2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    inputCode3.requestFocus();
                    checkVerificationCode();
                }
            }
        });

        inputCode3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    inputCode4.requestFocus();
                    checkVerificationCode();
                }
            }
        });

        inputCode4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    inputCode5.requestFocus();
                    checkVerificationCode();
                }
            }
        });

        inputCode5.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    inputCode6.requestFocus();
                    checkVerificationCode();
                }
            }
        });

        inputCode6.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    checkVerificationCode();
                }
            }
        });

        inputCode6.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_DEL) {
                    if (inputCode6.length() == 0) {
                        inputCode5.requestFocus();
                        inputCode5.setText(null);
                        return true;
                    }else {
                        return false;
                    }
                }else {
                    return false;
                }
            }
        });

        inputCode5.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_DEL) {
                    if (inputCode5.length() == 0) {
                        inputCode4.requestFocus();
                        inputCode4.setText(null);
                        return true;
                    }else {
                        return false;
                    }
                }else {
                    return false;
                }
            }
        });

        inputCode4.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_DEL) {
                    if (inputCode4.length() == 0) {
                        inputCode3.requestFocus();
                        inputCode3.setText(null);
                        return true;
                    }else {
                        return false;
                    }
                }else {
                    return false;
                }
            }
        });

        inputCode3.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_DEL) {
                    if (inputCode3.length() == 0) {
                        inputCode2.requestFocus();
                        inputCode2.setText(null);
                        return true;
                    }else {
                        return false;
                    }
                }else {
                    return false;
                }
            }
        });

        inputCode2.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_DEL) {
                    if (inputCode2.length() == 0) {
                        inputCode1.requestFocus();
                        inputCode1.setText(null);
                        return true;
                    }else {
                        return false;
                    }
                }else {
                    return false;
                }
            }
        });
    }

    private void checkVerificationCode() {
        if (DataUtils.isNotEmpty(inputCode1) && DataUtils.isNotEmpty(inputCode2) &&
                DataUtils.isNotEmpty(inputCode3) && DataUtils.isNotEmpty(inputCode4) &&
                DataUtils.isNotEmpty(inputCode5) && DataUtils.isNotEmpty(inputCode6)) {
            String Code = inputCode1.getText().toString()+inputCode2.getText().toString()+
                    inputCode3.getText().toString()+inputCode4.getText().toString()+
                    inputCode5.getText().toString()+inputCode6.getText().toString();
            credentialPresenter.MeResetPasswordCode(isEmail, EmailPhone, Code);
        }
    }

    @Override
    public void showLoading(API api) {
        showLoadingDialog(api);
    }

    @Override
    public void hideLoading() {
        dismissLoadingDialog();
    }

    @Override
    public void LoginSuccess(APIResponseData response) {}

    @Override
    public void RegisterSuccess(APIResponseData response) {}

    @Override
    public void ProfileSuccess(String ChangePage) {
        credentialPage.ChangePage(ChangePage);
    }

    @Override
    public void VerificationSuccess() {}

    @Override
    public void ResendVerificationSuccess(String Message) {}

    @Override
    public void ForgotPasswordSuccess() {}

    @Override
    public void ResetPasswordCodeSuccess() {
        credentialPresenter.MeProfile();
    }

    @Override
    public void ResetPasswordSuccess() {}

    @Override
    public void ChangePhoneNumber() {}

    @Override
    public void RequestFailed(API api, String errorMessage) {
        Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT).show();
    }
}
