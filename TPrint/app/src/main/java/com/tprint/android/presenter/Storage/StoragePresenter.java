package com.tprint.android.presenter.Storage;

import com.tprint.android.services.API;
import com.tprint.android.services.APIListener;
import com.tprint.android.services.APIRequest;
import com.tprint.android.services.APIResponseData;

import java.io.File;

/**
 * Created by aboot on 7/13/17.
 */

public class StoragePresenter implements StoragePresenterImpl {

    private StorageViewImpl storageView;

    public StoragePresenter(StorageViewImpl storageView) {
        this.storageView = storageView;
    }

    @Override
    public void StorageUpload(File file) {
        storageView.showLoading(API.Storage_Upload);
        new APIRequest().StorageUpload(file, new APIListener() {
            @Override
            public void onSuccess(APIResponseData response) {
                storageView.hideLoading();
                storageView.UploadSuccess();
            }

            @Override
            public void onFailure(API api, String errorMessage) {
                storageView.hideLoading();
                storageView.RequestFailed(api, errorMessage);
            }
        });
    }

    @Override
    public void StorageList() {
        storageView.showLoading(API.Storage_List);
        new APIRequest().StorageList(new APIListener() {
            @Override
            public void onSuccess(APIResponseData response) {
                storageView.hideLoading();
                storageView.ListSuccess(response);
            }

            @Override
            public void onFailure(API api, String errorMessage) {
                storageView.hideLoading();
                storageView.RequestFailed(api, errorMessage);
            }
        });
    }

    @Override
    public void StoragePrint(String IDs) {
        storageView.showLoading(API.Storage_Print);
        new APIRequest().StoragePrint(IDs, new APIListener() {
            @Override
            public void onSuccess(APIResponseData response) {
                storageView.hideLoading();
                storageView.PrintSuccess();
            }

            @Override
            public void onFailure(API api, String errorMessage) {
                storageView.hideLoading();
                storageView.RequestFailed(api, errorMessage);
            }
        });
    }

    @Override
    public void StorageRemove(String ID) {
        storageView.showLoading(API.Storage_Remove);
        new APIRequest().StorageRemove(ID, new APIListener() {
            @Override
            public void onSuccess(APIResponseData response) {
                storageView.hideLoading();
                storageView.RemoveSuccess();
            }

            @Override
            public void onFailure(API api, String errorMessage) {
                storageView.hideLoading();
                storageView.RequestFailed(api, errorMessage);
            }
        });
    }
}
