package com.tprint.android.view.dialogs;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tprint.android.R;
import com.tprint.android.etc.DataUtils;
import com.tprint.android.view.BaseDialog;

import butterknife.BindView;

/**
 * Created by aboot on 7/4/17.
 */

public class LoadingDialog extends BaseDialog {

    @BindView(R.id.textMessage) TextView textMessage;

    public static LoadingDialog newInstance(String Message) {
        Bundle args = new Bundle();
        args.putString("Message", Message);
        LoadingDialog fragment = new LoadingDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.loading_dialog, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        String Message = getArguments().getString("Message");

        if (DataUtils.isNotEmpty(Message)) {
            textMessage.setText(getArguments().getString("Message"));
            textMessage.setVisibility(View.VISIBLE);
        }else {
            textMessage.setVisibility(View.GONE);
        }

        setCancelable(false);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isAdded()) {
            ViewGroup.LayoutParams params = getDialog().getWindow().getAttributes();
            params.width = ViewGroup.LayoutParams.WRAP_CONTENT;
            params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
            getDialog().getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }
}
