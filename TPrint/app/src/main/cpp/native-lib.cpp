#include <jni.h>
#include <string>

JNIEXPORT jstring JNICALL
Java_com_tprint_android_view_BaseActivity_getPrintGila(JNIEnv *env, jobject instance) {

    // TODO


    std::string hello = "Hi, tprint";
    return env->NewStringUTF(hello.c_str());
}

extern "C"
JNIEXPORT jstring JNICALL
Java_com_tprint_android_MainActivity_stringFromJNI(
        JNIEnv *env,
        jobject /* this */) {
    std::string hello = "Hello from C++";
    return env->NewStringUTF(hello.c_str());
}
